from componentes.models import Pedido, HistorialEstadoPedido
from custom_admin.views.auth_view import staff_required
from django.shortcuts import render
from django.views.generic import ListView, FormView
from django.views.generic.detail import DetailView
from componentes.pedido_services import PedidoServices
from custom_admin.forms import BusquedaPedidoForm


class PromocionListView(ListView):
    template_name = 'administracion_total/componentes/promocion_list.html'
    model = Pedido


def pedidos_view(request):
    return render(request, 'administracion_sucursal/componentes/pedidos.html')


def pedidos_management_view(request):
    pedido_service = PedidoServices()
    pedidos_sin_asignar = pedido_service.pedidos_por_estado(
        HistorialEstadoPedido.PENDIENTE)
    pedidos_en_produccion = pedido_service.pedidos_por_estado(
        HistorialEstadoPedido.EN_PRODUCCION)
    pedidos_listos_para_entregar = pedido_service.pedidos_por_estado(
        HistorialEstadoPedido.LISTO_PARA_ENTREGAR)
    pedidos_en_envio = pedido_service.pedidos_por_estado(
        HistorialEstadoPedido.EN_ENVIO)
    pedidos_entregados = pedido_service.pedidos_por_estado(
        HistorialEstadoPedido.ENTREGADO)

    ctx = {
        'pedidos_sin_asignar': pedidos_sin_asignar,
        'pedidos_en_produccion': pedidos_en_produccion,
        'pedidos_listos_para_entregar': pedidos_listos_para_entregar,
        'pedidos_en_envio': pedidos_en_envio,
        'pedidos_entregados': pedidos_entregados,
        'id_pedido_pendiente': HistorialEstadoPedido.PENDIENTE,
        'id_pedido_en_produccion': HistorialEstadoPedido.EN_PRODUCCION,
        'id_pedido_entregado': HistorialEstadoPedido.ENTREGADO,
        'id_pedido_cancelado': HistorialEstadoPedido.CANCELADO,
        'id_pedido_listo_para_entregar':
            HistorialEstadoPedido.LISTO_PARA_ENTREGAR,
        'id_pedido_en_envio': HistorialEstadoPedido.EN_ENVIO
    }
    return render(request, 'administracion_sucursal/componentes/partial/gestor_pedidos.html', ctx)


class BusquedaPedidoFormView(FormView):
    form_class = BusquedaPedidoForm
    template_name = 'administracion_sucursal/componentes/pedidos_busqueda.html'

    def get(self, request, *args, **kwargs):
        pedido_service = PedidoServices()
        pedidos = pedido_service.obtener_pedidos_exclude_definicion()
        listado_de_pedido = pedidos.order_by('-fechaPedido')
        return self.render_to_response(self.get_context_data(
            listado_de_pedido=listado_de_pedido))

    def form_valid(self, form):
        pedido_service = PedidoServices()
        filtro = form.cleaned_data.get('buscar')
        try:
            pedidos = pedido_service.obtener_pedidos_exclude_definicion(
                Pedido.objects.pedidos_by_filtro(filtro))
            listado_de_pedido = pedidos
        except Pedido.DoesNotExist:
            pedidos = pedido_service.obtener_pedidos_exclude_definicion()
            listado_de_pedido = pedidos.order_by('-fechaPedido')
            return self.render_to_response(self.get_context_data(
                form=form, listado_de_pedido=listado_de_pedido))

        if listado_de_pedido:
            return self.render_to_response(self.get_context_data(
                form=form, listado_de_pedido=listado_de_pedido))
        else:
            pedidos = pedido_service.obtener_pedidos_exclude_definicion()
            listado_de_pedido = pedidos.order_by('-fechaPedido')
            return self.render_to_response(self.get_context_data(
                form=form, listado_de_pedido=listado_de_pedido))


class DetallePedidoView(DetailView):
    """
    Muestra el detalle de un pedido.
    """
    template_name = 'administracion_sucursal/componentes/pedido_detalle.html'
    context_object_name = 'pedido'
    model = Pedido

    def get_object(self, queryset=None):
        return Pedido.objects.get(pk=self.kwargs['pk_pedido'])