# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.contrib import messages
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.views.generic import DeleteView
from django.views.generic import ListView, CreateView, UpdateView
from django.core.urlresolvers import reverse
from componentes.models import (
    TiempoPromedioCola,
    TiempoPromedioEntrega, TiempoPromedioProducto)
from custom_admin.forms import (
    TiempoPromedioProductoForm, TiempoPromedioColaForm,
    TiempoPromedioEntregaForm)


class TiempoPromedioColaCreateView(CreateView):
    model = TiempoPromedioCola
    template_name = 'administracion_total/componentes/tiempo_promedio_cola.html'
    context_object_name = 'tiempo_promedio_cola'
    form_class = TiempoPromedioColaForm

    def form_valid(self, form):
        self.object = form.save(commit=False)
        if self.object.valida_nuevo_tiempo_promedio_cola(self.object):
            message = """¡Cuidado!
            La franja horaria elegida para este día coinciden con alguna ya
            cargada"""
            messages.add_message(
                self.request,
                messages.WARNING,
                message,
            )
            return self.form_invalid(form)

        return super(TiempoPromedioColaCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('promedio_cola_list')


class TiempoPromedioColaListView(ListView):
    template_name = 'administracion_total/componentes/tiempo_promedio_cola_list.html'
    model = TiempoPromedioCola

    def get_queryset(self):
        return TiempoPromedioCola.objects.all().order_by('dia_semanal',
                                                         'hora_desde')


class TiempoPromedioColaDeleteView(DeleteView):
    """
    Esta vista se encarga de la eliminación del
    objeto producto para armar.
    """
    model = TiempoPromedioCola
    template_name = 'administracion_total/componentes/tiempo_promedio_cola_elimina.html'

    def get_success_url(self):
        return reverse('promedio_cola_list')


class TiempoPromedioColaUpdateView(UpdateView):
    model = TiempoPromedioCola
    template_name = 'administracion_total/componentes/tiempo_promedio_cola.html'
    context_object_name = 'tiempo_promedio_cola'
    form_class = TiempoPromedioColaForm

    def form_valid(self, form):
        self.object = form.save(commit=False)
        if self.object.valida_update_tiempo_promedio_cola(self.object):
            message = """¡Cuidado!
            La franja horaria elegida para este día coinciden con alguna ya
            cargada"""
            messages.add_message(
                self.request,
                messages.WARNING,
                message,
            )
            return self.form_invalid(form)

        return super(TiempoPromedioColaUpdateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('promedio_cola_list')


class TiempoPromedioEntregaCreateView(CreateView):
    model = TiempoPromedioEntrega
    template_name = 'administracion_total/componentes/tiempo_promedio_entrega.html'
    context_object_name = 'tiempo_promedio_entrega'
    form_class = TiempoPromedioEntregaForm

    def form_valid(self, form):
        self.object = form.save(commit=False)
        if self.object.valida_nuevo_tiempo_promedio_entrega(self.object):
            message = """¡Cuidado!
            La franja horaria elegida para este día coinciden con alguna ya
            cargada"""
            messages.add_message(
                self.request,
                messages.WARNING,
                message,
            )
            return self.form_invalid(form)

        return super(TiempoPromedioEntregaCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('promedio_entrega_list')


class TiempoPromedioEntregaListView(ListView):
    template_name = 'administracion_total/componentes/tiempo_promedio_entrega_list.html'
    model = TiempoPromedioEntrega

    def get_queryset(self):
        return TiempoPromedioEntrega.objects.all().order_by('dia_semanal',
                                                            'hora_desde')


class TiempoPromedioEntregaDeleteView(DeleteView):
    """
    Esta vista se encarga de la eliminación del
    objeto producto para armar.
    """
    model = TiempoPromedioEntrega
    template_name = 'administracion_total/componentes/tiempo_promedio_entrega_elimina.html'

    def get_success_url(self):
        return reverse('promedio_entrega_list')


class TiempoPromedioEntregaUpdateView(UpdateView):
    model = TiempoPromedioEntrega
    template_name = 'administracion_total/componentes/tiempo_promedio_entrega.html'
    context_object_name = 'tiempo_promedio_entrega'
    form_class = TiempoPromedioEntregaForm

    def form_valid(self, form):
        self.object = form.save(commit=False)
        if self.object.valida_update_tiempo_promedio_entrega(self.object):
            message = """¡Cuidado!
            La franja horaria elegida para este día coinciden con alguna ya
            cargada"""
            messages.add_message(
                self.request,
                messages.WARNING,
                message,
            )
            return self.form_invalid(form)

        return super(TiempoPromedioEntregaUpdateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('promedio_entrega_list')


class TiempoPromedioProductoCreateView(CreateView):
    model = TiempoPromedioProducto
    template_name = 'administracion_total/componentes/tiempo_promedio_producto.html'
    context_object_name = 'tiempo_promedio_producto'
    form_class = TiempoPromedioProductoForm

    def form_valid(self, form):
        self.object = form.save(commit=False)
        if self.object.valida_nuevo_tiempo_promedio_producto(self.object):
            message = """¡Cuidado!
            La franja de cantidad elegida para este día coinciden con alguna ya
            cargada"""
            messages.add_message(
                self.request,
                messages.WARNING,
                message,
            )
            return self.form_invalid(form)

        return super(TiempoPromedioProductoCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('promedio_producto_list')


class TiempoPromedioProductoListView(ListView):
    template_name = 'administracion_total/componentes/tiempo_promedio_producto_list.html'
    model = TiempoPromedioProducto

    def get_queryset(self):
        return TiempoPromedioProducto.objects.all().order_by('producto',
                                                             'cantidad_minima')


class TiempoPromedioProductoDeleteView(DeleteView):
    """
    Esta vista se encarga de la eliminación del
    objeto producto para armar.
    """
    model = TiempoPromedioProducto
    template_name = 'administracion_total/componentes/tiempo_promedio_producto_elimina.html'

    def get_success_url(self):
        return reverse('promedio_producto_list')


class TiempoPromedioProductoUpdateView(UpdateView):
    model = TiempoPromedioProducto
    template_name = 'administracion_total/componentes/tiempo_promedio_producto.html'
    context_object_name = 'tiempo_promedio_producto'
    form_class = TiempoPromedioProductoForm

    def form_valid(self, form):
        self.object = form.save(commit=False)
        if self.object.valida_update_tiempo_promedio_producto(self.object):
            message = """¡Cuidado!
            La franja de cantidad elegida para este día coinciden con alguna ya
            cargada"""""
            messages.add_message(
                self.request,
                messages.WARNING,
                message,
            )
            return self.form_invalid(form)

        return super(TiempoPromedioProductoUpdateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('promedio_producto_list')
