from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.views.generic import UpdateView
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from libs.bdparams_utils import get_param
from libs.util.deco_utils import jsonResponse
from filer.models import Image

