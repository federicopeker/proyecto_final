from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.forms import PasswordChangeForm
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect
from django.shortcuts import resolve_url
from django.template.response import TemplateResponse
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django.contrib import messages


@sensitive_post_parameters()
@csrf_protect
@login_required
def cms_password_change(request,
                    template_name='registration/password_change_form.html',
                    post_change_redirect=None,
                    password_change_form=PasswordChangeForm,
                    current_app=None, extra_context=None):
    if post_change_redirect is None:
        post_change_redirect = reverse('password_change_done')
    else:
        post_change_redirect = resolve_url(post_change_redirect)
    if request.method == "POST":
        form = password_change_form(user=request.user, data=request.POST)
        if form.is_valid():
            form.save()
            # Updating the password logs out all other sessions for the user
            # except the current one if
            # django.contrib.auth.middleware.SessionAuthenticationMiddleware
            # is enabled.
            update_session_auth_hash(request, form.user)
            messages.success(request, 'Password was changed!')
            return HttpResponseRedirect(post_change_redirect)
    else:
        form = password_change_form(user=request.user)
    context = {
        'form': form,
        'title': 'Password change',
    }
    if extra_context is not None:
        context.update(extra_context)

    if current_app is not None:
        request.current_app = current_app

    return TemplateResponse(request, template_name, context)


def admin_required(view_func):
    """
    decorador para verificar que un usuario es administrador
    :param view_func:
    :return:
    """
    deco = user_passes_test(lambda u: u.is_superuser, login_url=reverse_lazy('custom_admin_login'))
    return deco(view_func)


def staff_required(view_func):
    """
    decorador para verificar que un usuario es staff(en este caso encargado)
    :param view_func:
    :return:
    """
    deco = user_passes_test(lambda u: u.is_staff, login_url=reverse_lazy('custom_admin_login'))
    return deco(view_func)

