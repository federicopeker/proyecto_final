# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from custom_admin.views.auth_view import admin_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.views.generic import DeleteView
from django.views.generic import ListView, CreateView, UpdateView


from recursos_de_empresa.models import Encargado
from custom_admin.forms import EncargadoUserCreateForm, EncargadoUserUpdateForm


class EncargadoCreateView(CreateView):

    template_name = 'administracion_total/recursos/encargado_create_update.html'
    model = Encargado
    context_object_name = 'encargado'
    form_class = EncargadoUserCreateForm

    def form_valid(self, form):
        ret = super(EncargadoCreateView, self).form_valid(form)

        # Set the password
        self.object = form.save(commit=False)
        self.object.set_password(form['password1'].value())
        self.object.is_staff = True
        self.object.save()

        messages.success(self.request, 'El encargado fue creado correctamente')
        return ret

    def get_success_url(self):
        return reverse('encargado_list')


class EncargadoUpdateView(UpdateView):

    template_name = 'administracion_total/recursos/encargado_create_update.html'
    model = Encargado
    context_object_name = 'encargado'
    form_class = EncargadoUserUpdateForm

    def form_valid(self, form):
        ret = super(EncargadoUpdateView, self).form_valid(form)

        # Set the password
        self.object = form.save(commit=False)
        self.object.set_password(form['password1'].value())
        self.object.save()

        messages.success(self.request, 'El encargado fue actualizado '
                                       'correctamente')
        return ret

    def get_success_url(self):
        return reverse('encargado_list')


class EncargadoListView(ListView):
    template_name = 'administracion_total/recursos/encargado_list.html'
    model = Encargado


class EncargadoDeleteView(DeleteView):
    """
    Esta vista se encarga de la eliminación del
    objeto encargado.
    """
    model = Encargado
    template_name = 'administracion_total/recursos/encargado_elimina.html'

    def get_success_url(self):
        return reverse('encargado_list')


