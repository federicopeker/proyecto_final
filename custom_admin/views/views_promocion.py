# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.contrib import messages
from django.http import HttpResponseRedirect
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from django.core.urlresolvers import reverse
from componentes.models import (Promocion, Actuacion, DetallePromocion)
from custom_admin.forms import (PromocionForm, ActuacionForm,
                                DetallePromocionForm, PromocionPrecioForm)


class CheckEstadoPromocionMixin(object):
    """Mixin para utilizar en las vistas de creación de promociones.
    Utiliza `Promocion.objects.obtener_en_definicion_para_editar()`
    para obtener la promocion pasada por url.
    Este metodo falla si la promocion no deberia ser editada.
    ('editada' en el contexto del proceso de creacion de la promocion)
    """

    def dispatch(self, request, *args, **kwargs):
        self.promocion = Promocion.objects.obtener_en_definicion_para_editar(
            self.kwargs['pk_promocion'])
        return super(CheckEstadoPromocionMixin, self).dispatch(request, *args,
                                                               **kwargs)


class PromocionEnDefinicionMixin(object):
    """Mixin para obtener el objeto promocion que valida que siempre este en
    el estado en definición.
    """

    def get_object(self, queryset=None):
        return Promocion.objects.obtener_en_definicion_para_editar(
            self.kwargs['pk_promocion'])


class PromocionCreateView(CreateView):
    """
    Esta vista crea un objeto Promocion.
    Por defecto su estado es EN_DEFICNICION,
    Redirecciona a crear las opciones para esta
    promocione.
    """

    template_name = 'administracion_total/componentes/promocion_form.html'
    model = Promocion
    context_object_name = 'promocion'
    form_class = PromocionForm

    def get_success_url(self):
        return reverse(
            'actuacion_promocion',
            kwargs={"pk_promocion": self.object.pk})


class PromocionUpdateView(PromocionEnDefinicionMixin, CheckEstadoPromocionMixin,
                          UpdateView):
    """
    Esta vista actualiza una promocion
    """

    template_name = 'administracion_total/componentes/promocion_form.html'
    model = Promocion
    context_object_name = 'promocion'
    form_class = PromocionForm

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()

        form_class = self.get_form_class()
        form = self.get_form(form_class)

        from_valid = form.is_valid()

        if from_valid:
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_success_url(self):
        return reverse(
            'actuacion_promocion',
            kwargs={"pk_promocion": self.object.pk})


class ActuacionPromocionCreateView(CheckEstadoPromocionMixin, CreateView):
    """
    Esta vista crea uno o varios objetos Actuacion
    para la promocion que se este creando.
    Inicializa el form con campo promocion (hidden)
    con el id de promocion que viene en la url.
    """

    template_name = 'administracion_total/componentes/actuacion_promocion.html'
    model = Actuacion
    context_object_name = 'actuacion'
    form_class = ActuacionForm

    def get_initial(self):
        initial = super(ActuacionPromocionCreateView, self).get_initial()
        initial.update({'promocion': self.promocion.id})
        return initial

    def get_context_data(self, **kwargs):
        context = super(
            ActuacionPromocionCreateView, self).get_context_data(**kwargs)
        context['promocion'] = self.promocion
        context['actuaciones_validas'] = \
            self.promocion.obtener_actuaciones_validas()
        return context

    def form_valid(self, form):
        form_valid = super(ActuacionPromocionCreateView, self).form_valid(form)

        if not self.promocion.valida_actuaciones():
            message = """¡Cuidado!
            Los días del rango de fechas seteados en la promocion NO coinciden
            con ningún día de las actuaciones programadas."""
            messages.add_message(
                self.request,
                messages.WARNING,
                message,
            )

        return form_valid

    def get_success_url(self):
        return reverse(
            'actuacion_promocion',
            kwargs={"pk_promocion": self.kwargs['pk_promocion']}
        )


class ActuacionPromocionDeleteView(CheckEstadoPromocionMixin, DeleteView):
    """
    Esta vista se encarga de la eliminación del
    objeto Actuación seleccionado.
    """

    model = Actuacion
    template_name = 'administracion_total/componentes/elimina_actuacion_promocion.html'

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.delete()

        if not self.promocion.valida_actuaciones():
            message = """¡Cuidado!
            Los días del rango de fechas seteados en la promocion NO coinciden
            con ningún día de las actuaciones programadas."""
            messages.add_message(
                self.request,
                messages.WARNING,
                message,
            )

        message = 'Operación Exitosa!\
        Se llevó a cabo con éxito la eliminación de la Actuación.'

        messages.add_message(
            self.request,
            messages.SUCCESS,
            message,
        )
        return HttpResponseRedirect(success_url)

    def get_success_url(self):
        return reverse(
            'actuacion_promocion',
            kwargs={"pk_promocion": self.kwargs['pk_promocion']}
        )


class DetallePromocionCreateView(CheckEstadoPromocionMixin, CreateView):
    """
    Esta vista crea uno o varios objetos DetallePromocion
    para la promoción que se este creando.
    Inicializa el form con campo promocion (hidden)
    con el id de promocion que viene en la url.
    """

    template_name = 'administracion_total/componentes/detalle_promocion.html'
    model = DetallePromocion
    context_object_name = 'detalle_promocion'
    form_class = DetallePromocionForm

    def get_initial(self):
        initial = super(DetallePromocionCreateView, self).get_initial()
        initial.update({'promocion': self.promocion.id})
        return initial

    def get_context_data(self, **kwargs):
        context = super(
            DetallePromocionCreateView, self).get_context_data(**kwargs)
        context['promocion'] = self.promocion
        return context

    def get_success_url(self):
        return reverse(
            'detalle_promocion',
            kwargs={"pk_promocion": self.kwargs['pk_promocion']}
        )


class DetallePromocionDeleteView(CheckEstadoPromocionMixin, DeleteView):
    """
    Esta vista se encarga de la eliminación del
    objeto Actuación seleccionado.
    """

    model = DetallePromocion
    template_name = 'administracion_total/componentes/elimina_detalle_promocion.html'

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.delete()

        message = 'Operación Exitosa!\
        Se llevó a cabo con éxito la eliminación del detalle.'

        messages.add_message(
            self.request,
            messages.SUCCESS,
            message,
        )
        return HttpResponseRedirect(success_url)

    def get_success_url(self):
        return reverse(
            'detalle_promocion',
            kwargs={"pk_promocion": self.kwargs['pk_promocion']}
        )


class PromocionUpdatePrecioView(PromocionEnDefinicionMixin,
                                CheckEstadoPromocionMixin, UpdateView):
    """
    Esta vista actualiza una promocion el precio imagen y descuento
    """

    template_name = 'administracion_total/componentes/promocion_precio.html'
    model = Promocion
    context_object_name = 'promocion'
    form_class = PromocionPrecioForm

    def get_initial(self):
        initial = super(PromocionUpdatePrecioView, self).get_initial()
        initial.update({'precio': self.promocion.get_precio_total()})
        if self.promocion.descuento is not None:
            descuento = self.promocion.get_precio_total() * \
                        self.promocion.descuento / 100
            precio_total = self.promocion.get_precio_total() - descuento
            initial.update({'precio': precio_total})
        return initial

    def form_valid(self, form):
        self.object = form.save(commit=False)
        descuento = self.promocion.get_precio_total() * \
                    self.object.descuento/100
        self.object.precio = self.promocion.get_precio_total() - descuento
        self.object.save()
        return super(PromocionUpdatePrecioView, self).form_valid(form)

    def get_success_url(self):
        return reverse('promocion_precio',
            kwargs={"pk_promocion": self.kwargs['pk_promocion']}
        )


class PromocionDeleteView(DeleteView):
    """
    Esta vista se encarga de la eliminación del
    objeto promocion.
    """
    model = Promocion
    template_name = 'administracion_total/componentes/promocion_elimina.html'

    def get_success_url(self):
        return reverse('promocion_list')


class PromocionListView(ListView):
    template_name = 'administracion_total/componentes/promocion_list.html'
    model = Promocion
