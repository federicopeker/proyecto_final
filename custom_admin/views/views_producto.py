# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.contrib import messages
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.views.generic import DeleteView
from django.views.generic import ListView, CreateView, UpdateView
from django.core.urlresolvers import reverse
from componentes.models import (
    ProductoParaArmar, VersionProducto, Clasificacion, SeccionProducto,
    IngredientesSeccion
)
from custom_admin.forms import (
    VersionProductoForm, SeccionProductoForm, IngredientesSeccionForm,
    ProductoParaArmarForm
)


class CheckEstadoProductoParaArmarMixin(object):
    """Mixin para utilizar en las vistas de creación de productos para armado.
    Utiliza `ProductoParaArmar.objects.obtener_en_definicion_para_editar()`
    para obtener el producto pasada por url.
    Este metodo falla si la promocion no deberia ser editada.
    ('editada' en el contexto del proceso de creacion del producto)
    """

    def dispatch(self, request, *args, **kwargs):
        self.producto = ProductoParaArmar.objects.\
            obtener_en_definicion_para_editar(self.kwargs['pk_producto'])
        return super(CheckEstadoProductoParaArmarMixin, self).\
            dispatch(request, *args, **kwargs)


class ProductoParaArmarCreateView(CreateView):
    model = ProductoParaArmar
    template_name = 'administracion_total/componentes/producto_seleccion.html'
    form_class = ProductoParaArmarForm

    def get_success_url(self):
        return reverse('version_producto',
                       kwargs={"pk_producto": self.object.pk}
                       )


class ProductoParaArmarUpdateView(UpdateView):
    model = ProductoParaArmar
    template_name = 'administracion_total/componentes/producto_seleccion.html'
    form_class = ProductoParaArmarForm

    def get_object(self, queryset=None):
        return ProductoParaArmar.objects.get(pk=self.kwargs['pk_producto'])

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.estado = ProductoParaArmar.ESTADO_EN_DEFINICION
        self.object.save()
        return redirect(self.get_success_url())

    def get_success_url(self):
        return reverse('version_producto',
                       kwargs={"pk_producto": self.object.pk}
                       )


class VersionProductoCreateView(CheckEstadoProductoParaArmarMixin, CreateView):
    model = VersionProducto
    template_name = 'administracion_total/componentes/producto_version.html'
    context_object_name = 'version'
    form_class = VersionProductoForm

    def get_initial(self):
        initial = super(VersionProductoCreateView, self).get_initial()
        initial.update({'producto': self.producto.id})
        return initial

    def get_form(self, form_class):
        clasificaciones = self.producto.version.clasificacion.all()
        return form_class(clasificaciones=clasificaciones,
                          **self.get_form_kwargs())

    def get_context_data(self, **kwargs):
        context = super(
            VersionProductoCreateView, self).get_context_data(**kwargs)
        context['producto'] = self.producto
        return context

    def form_valid(self, form):
        self.object = form.save(commit=False)
        clasificacion = self.request.POST.get('clasificacion')
        if self.producto.valida_versiones(clasificacion):
            clasificacion = Clasificacion.objects.get(pk=clasificacion)
            message = '<Operación Errónea!\
                Ya existe version con la clasificacion {0}. Puede agregar una '\
                      'clasificacion por version.'.format(clasificacion)
            messages.add_message(
                self.request,
                messages.ERROR,
                message,
            )
            return self.form_invalid(form)
        self.object.save()
        return redirect(self.get_success_url())

    def get_success_url(self):
        return reverse('version_producto',
                       kwargs={"pk_producto": self.kwargs['pk_producto']}
                       )


def version_delete_view(request, pk_producto, pk_version):

    version = VersionProducto.objects.get(pk=pk_version)
    version.delete()
    return HttpResponseRedirect("/administrador/producto_armar/" +
                                str(pk_producto) + "/version/")


class SeccionProductoCreateView(CheckEstadoProductoParaArmarMixin, CreateView):
    model = SeccionProducto
    template_name = 'administracion_total/componentes/producto_seccion.html'
    context_object_name = 'seccion'
    form_class = SeccionProductoForm

    def get_initial(self):
        initial = super(SeccionProductoCreateView, self).get_initial()
        initial.update({'producto': self.producto.id})
        return initial

    def get_context_data(self, **kwargs):
        context = super(
            SeccionProductoCreateView, self).get_context_data(**kwargs)
        context['producto'] = self.producto
        return context

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.orden = \
            SeccionProducto.objects.obtener_siguiente_orden(self.producto.pk)
        self.object.save()
        return redirect(self.get_success_url())

    def get_success_url(self):
        return reverse('seccion_producto',
                       kwargs={"pk_producto": self.kwargs['pk_producto']}
                       )


def seccion_delete_view(request, pk_producto, pk_seccion):

    seccion = SeccionProducto.objects.get(pk=pk_seccion)
    seccion.delete()
    return HttpResponseRedirect("/administrador/producto_armar/" +
                                str(pk_producto) + "/seccion/")


class IngredientesSeccionCreateView(CheckEstadoProductoParaArmarMixin, CreateView):
    model = IngredientesSeccion
    template_name = 'administracion_total/componentes/producto_ingredientes.html'
    context_object_name = 'ingrediente_seccion'
    form_class = IngredientesSeccionForm

    def get_form(self, form_class):
        return form_class(producto=self.producto, **self.get_form_kwargs())

    def get_context_data(self, **kwargs):
        context = super(
            IngredientesSeccionCreateView, self).get_context_data(**kwargs)
        context['producto'] = self.producto
        return context

    def get_success_url(self):
        return reverse('ingredientes_producto',
                       kwargs={"pk_producto": self.kwargs['pk_producto']}
                       )


def ingrediente_delete_view(request, pk_producto, pk_ingrediente):

    ingrediente = IngredientesSeccion.objects.get(pk=pk_ingrediente)
    ingrediente.delete()
    return HttpResponseRedirect("/administrador/producto_armar/" +
                                str(pk_producto) + "/ingredientes/")


class ConfirmaProductoParaArmarMixin(object):

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.estado = ProductoParaArmar.ESTADO_ACTIVA
        self.object.save()
        return redirect(self.get_success_url())


class ProductoParaArmarConfirmarView(ConfirmaProductoParaArmarMixin,
                                     CheckEstadoProductoParaArmarMixin,
                                     UpdateView):
    model = ProductoParaArmar
    template_name = 'administracion_total/componentes/producto_confirmar.html'
    context_object_name = 'producto'
    fields = '__all__'

    def get_object(self, queryset=None):
        return ProductoParaArmar.objects.\
            obtener_en_definicion_para_editar(self.kwargs['pk_producto'])

    def get_success_url(self):
        return reverse('producto_armar_list')


class ProductoParaArmarListView(ListView):
    template_name = 'administracion_total/componentes/producto_list.html'
    model = ProductoParaArmar


class ProductoParaArmarDeleteView(DeleteView):
    """
    Esta vista se encarga de la eliminación del
    objeto producto para armar.
    """
    model = ProductoParaArmar
    template_name = 'administracion_total/componentes/producto_elimina.html'

    def get_success_url(self):
        return reverse('producto_armar_list')
