from django.views.generic import ListView
from recursos_de_empresa.models import Sucursal
from django.contrib.admin.views.decorators import staff_member_required

class Sucursales(ListView):
    model = Sucursal
    template_name = 'administracion_total/componentes/gestion_sucursales.html'


sucursales_view = Sucursales.as_view()
