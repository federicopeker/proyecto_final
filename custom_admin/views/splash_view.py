from django.template import RequestContext

from custom_admin.views.auth_view import staff_required
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse, reverse_lazy
from django.shortcuts import render, redirect, render_to_response
from filer.models import Image
from custom_admin.views import auth_view
from componentes.models import ClienteProfile
from recursos_de_empresa.models import Sucursal
from custom_admin.reportes_services.reportes_usuario import \
    UsuarioReporteServices
from django_datatables_view.base_datatable_view import BaseDatatableView


@staff_required
def dashboard(request):
    # service = UsuarioReporteServices()
    # estadisticas = service.general()
    # ctx = {
    #     'estadisticas': estadisticas
    # }
    # return render_to_response('admin_forms-elements.html', ctx,
    #                           context_instance=RequestContext(request))
    return render(request, 'admin_forms-elements.html')


# IFRAME COMPONENTES DEL ADMIN VIEJO(ADMIN DE DJANGO)
def componentes_iframe(request, componente):
    src = '/admin/componentes/' + componente
    ctx = {'src': src}
    return render_to_response('iframe.html', ctx,
                              context_instance=RequestContext(request))


def recursos_de_empresa_iframe(request, recurso):
    src = '/admin/recursos_de_empresa/' + recurso
    ctx = {'src': src}
    return render_to_response('iframe.html', ctx,
                              context_instance=RequestContext(request))


def parametros_iframe(request):
    src = '/admin/constance/config/'
    ctx = {'src': src}
    return render_to_response('iframe.html', ctx,
                              context_instance=RequestContext(request))


def ubicaciones_iframe(request, ubicacion):
    src = '/admin/ubicaciones/' + ubicacion
    ctx = {'src': src}
    return render_to_response('iframe.html', ctx,
                              context_instance=RequestContext(request))


def auth_iframe(request, auth):
    src = '/admin/auth/' + auth
    ctx = {'src': src}
    return render_to_response('iframe.html', ctx,
                              context_instance=RequestContext(request))


class ClientesVariosDatosJson(BaseDatatableView):
    # The model we're going to show
    model = ClienteProfile
    pre_camel_case_notation = False

    def get_initial_queryset(self):
        return ClienteProfile.objects.all()

    # define the columns that will be returned
    columns = ['id', 'user.first_name', 'user.last_name', 'barrio', 'red_social', 'cantidad_pedidos', 'cantidad_cancelados']

    # define column names that will be used in sorting
    # order is important and should be same as order of columns
    # displayed by datatables. For non sortable columns use empty
    # value like ''
    order_columns = ['id', 'user.first_name', 'user.last_name', 'barrio', 'red_social', 'cantidad_pedidos', 'cantidad_cancelados']

    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = -1

    def render_column(self, row, column):
        # We want to render user as a custom column
        if column == 'created_time':
            return '{0}'.format(row.created_time.strftime("%d/%m/%y %H:%M:%S"))
        elif column == 'action':
            data = {'pedido': row,}
            return render(self.request, 'datatable/pedidos_buttons.html', data).content
        elif column == 'tel_fijo_cliente':
            return str(row.cliente.codigo_area_telefono_fijo_cliente) + '-' + str(row.cliente.telefono_fijo_cliente)
        elif column == 'tel_movil_cliente':
            return str(row.cliente.codigo_area_telefono_celular_cliente) + '-' + str(row.cliente.telefono_celular_cliente)
        elif column == 'dato_cliente':
            return row.cliente.nombre_completo()
        elif column == 'cantidad_pedidos':
            return str(row.get_count_entregados())
        elif column == 'cantidad_cancelados':
            return str(row.get_count_cancelados())
        elif column == 'barrio':
            return row.barrio.nombre if row.barrio is not None else 'sin barrio'
        elif column == 'red_social':
            return row.get_red_social()
        else:
            return super(ClientesVariosDatosJson, self).render_column(row, column)


class SucursalesVariosDatosJson(BaseDatatableView):
    # The model we're going to show
    model = Sucursal
    pre_camel_case_notation = False

    def get_initial_queryset(self):
        return Sucursal.objects.all()

    # define the columns that will be returned
    columns = ['nombre', 'servicio', 'direccion', 'encargado', 'telefono', 'cantidad_pedidos', 'cantidad_cancelados']

    # define column names that will be used in sorting
    # order is important and should be same as order of columns
    # displayed by datatables. For non sortable columns use empty
    # value like ''
    order_columns = ['nombre', 'servicio', 'direccion', 'encargado', 'telefono', 'cantidad_pedidos', 'cantidad_cancelados']

    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = -1

    def render_column(self, row, column):
        #import ipdb; ipdb.set_trace()
        # We want to render user as a custom column
        if column == 'created_time':
            return '{0}'.format(row.created_time.strftime("%d/%m/%y %H:%M:%S"))
        elif column == 'action':
            data = {'pedido': row,}
            return render(self.request, 'datatable/pedidos_buttons.html', data).content
        elif column == 'servicio':
            servicio = " "
            if row.ofrece_servicio_delivery:
                servicio += "Delivery - "
            if row.ofrece_servicio_para_llevar:
                servicio += "Retiro en Sucursal - "
            if row.ofrece_sercicio_consumo_sucursal:
                servicio += "Consumo en Sucursal"
            return servicio
        elif column == 'direccion':
            return u"{} {} {} ".format(row.direccion,row.numero_direccion,row.barrio.nombre,)
        elif column == 'encargado':
            if row.encargado:
                return row.encargado.get_full_name()
            else:
                return "no tiene encargado asignado"
        elif column == 'cantidad_pedidos':
            return str(row.get_count_entregados())
        elif column == 'cantidad_cancelados':
            return str(row.get_count_cancelados())
        elif column == 'telefono':
            telefonos = ""
            for telefono in row.telefonosucursal_set.all():
                telefonos += telefono.numero + " / "
            return telefonos

        else:
            return super(SucursalesVariosDatosJson, self).render_column(row, column)