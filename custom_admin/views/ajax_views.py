from componentes.models import Pedido
from django.contrib import messages
from django.shortcuts import redirect
from libs.util.deco_utils import jsonResponse


@jsonResponse
def update_order_status(request):
    id_pedido = int(request.GET.get('id_pedido'))
    new_status_id = int(request.GET.get('new_status_id'))
    observaciones = request.GET.get('observaciones')
    pedido = Pedido.objects.get(id=id_pedido)
    try:
        pedido.change_status(new_status_id, observaciones)
        return {'response': 'success', 'message': 'pedido actualizado correctamente'}
    except Exception as e:
        return {'response': 'fail', 'message': e.message}

