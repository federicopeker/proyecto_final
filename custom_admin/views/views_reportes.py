# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.views.generic import ListView, UpdateView
from django.shortcuts import redirect
from componentes.models import ClienteProfile
from recursos_de_empresa.models import Sucursal
from custom_admin.reportes_services.reportes_usuario import \
    UsuarioReporteServices
from custom_admin.reportes_services.clientes_csv import ExportarClientesService
from custom_admin.reportes_services.sucursales_csv import \
    ExportarSucursalService
from allauth.socialaccount.models import SocialAccount


class UsuarioDatosReporteDetailView(ListView):

    template_name = 'reportes/usuario_datos.html'
    context_object_name = 'clienteprofile'
    model = ClienteProfile

    def get_context_data(self, **kwargs):
        context = super(UsuarioDatosReporteDetailView, self).get_context_data(
           **kwargs)
        service_csv = ExportarClientesService()
        service_csv.crea_reporte_csv()
        return context


class SucursalDatosReporteDetailView(ListView):

    template_name = 'reportes/sucursal_datos.html'
    context_object_name = 'sucursal'
    model = Sucursal

    def get_context_data(self, **kwargs):
        context = super(SucursalDatosReporteDetailView, self).get_context_data(
           **kwargs)
        service_csv = ExportarSucursalService()
        service_csv.crea_reporte_csv()
        return context


class UsuarioRegistroRedSocialReporteDetailView(ListView):

    template_name = 'reportes/registro_red_social.html'
    context_object_name = 'sucursal'
    model = Sucursal

    def get_context_data(self, **kwargs):
        context = super(UsuarioRegistroRedSocialReporteDetailView,
                        self).get_context_data(**kwargs)
        service = UsuarioReporteServices()
        context['estadisticas'] = service.registro_red_social()
        return context


class PedidosPorSucursalReporteDetailView(ListView):

    template_name = 'reportes/pedidos_sucursal.html'
    context_object_name = 'sucursal'
    model = Sucursal

    def get_context_data(self, **kwargs):
        context = super(PedidosPorSucursalReporteDetailView,
                        self).get_context_data(**kwargs)
        service = UsuarioReporteServices()
        context['sucursales_bar'] = service.pedidos_por_sucursal()
        return context


def exporta_clientes_csv(request):
    service = ExportarClientesService()
    clientes = ClienteProfile.objects.all()
    url = service.obtener_url_reporte_csv_descargar(clientes)
    return redirect(url)


def exporta_sucursales_csv(request):
    service = ExportarSucursalService()
    sucursales = Sucursal.objects.all()
    url = service.obtener_url_reporte_csv_descargar(sucursales)
    return redirect(url)


class SocialAccountReporteDetailView(ListView):

    template_name = 'reportes/tipo_user_registrado.html'
    context_object_name = 'socialaccount'
    model = SocialAccount

    def get_context_data(self, **kwargs):
        context = super(SocialAccountReporteDetailView,
                        self).get_context_data(**kwargs)
        service = UsuarioReporteServices()
        service.tipo_user_registrado()
        return context


class EvolucionUserReporteDetailView(ListView):

    template_name = 'reportes/evolucion_user_registrado.html'
    context_object_name = 'clienteprofile'
    model = ClienteProfile

    def get_context_data(self, **kwargs):
        context = super(EvolucionUserReporteDetailView,
                        self).get_context_data(**kwargs)
        service = UsuarioReporteServices()
        service.evolucion_user()
        return context


class TopTenReporteDetailView(ListView):

    template_name = 'reportes/top_ten_consumidores.html'
    context_object_name = 'clienteprofile'
    model = ClienteProfile

    def get_context_data(self, **kwargs):
        context = super(TopTenReporteDetailView,
                        self).get_context_data(**kwargs)
        service = UsuarioReporteServices()
        context['estadisticas'] = service.top_ten_consumidores()
        return context


class TopTenBarrioDetailView(ListView):

    template_name = 'reportes/top_ten_barrio.html'
    context_object_name = 'clienteprofile'
    model = ClienteProfile

    def get_context_data(self, **kwargs):
        context = super(TopTenBarrioDetailView,
                        self).get_context_data(**kwargs)
        service = UsuarioReporteServices()
        context['estadisticas'] = service.top_ten_barrio()
        return context
