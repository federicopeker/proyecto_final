# -*- coding: utf-8 -*-

"""
Formularios de Django
"""

from __future__ import unicode_literals

from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Field, Layout, Div, MultiField, HTML
# from bootstrap3_datetime.widgets import DateTimePicker
from componentes.models import (
    Promocion, Actuacion, DetallePromocion, VersionProducto,
    SeccionProducto, IngredientesSeccion, TiempoPromedioProducto,
    TiempoPromedioEntrega, TiempoPromedioCola, ProductoParaArmar)
from recursos_de_empresa.models import Encargado


class PromocionForm(forms.ModelForm):
    descripcion = forms.CharField(widget=forms.Textarea)

    class Meta:
        model = Promocion
        fields = ('nombre', 'descripcion',
                  'precio', 'stock', 'fecha_inicio',
                  'fecha_fin')
        widgets = {
            "nombre": forms.TextInput(attrs={'class': 'form-control'}),
            "precio": forms.NumberInput(attrs={'class': 'form-control'}),
            "stock": forms.NumberInput(attrs={'class': 'form-control'}),
            "fecha_inicio": forms.TextInput(attrs={'class': 'form-control'}),
            "fecha_fin": forms.TextInput(attrs={'class': 'form-control'}),
        }


class ActuacionForm(forms.ModelForm):

    class Meta:
        model = Actuacion
        fields = ('dia_semanal', 'hora_desde', 'hora_hasta', 'promocion')
        widgets = {
            'promocion': forms.HiddenInput()
        }


class DetallePromocionForm(forms.ModelForm):
    #tipo_detalle_choice = list(DetallePromocion.DETALLE_CHOICES)
    #tipo_detalle = forms.ChoiceField(choices=tipo_detalle_choice, widget=forms.RadioSelect())

    class Meta:
        model = DetallePromocion
        fields = ('tipo_detalle', 'promocion', 'tipo_producto', 'producto',
                  'version_producto', 'menu', 'cantidad')
        widgets = {
            'tipo_detalle': forms.HiddenInput(),
            'promocion': forms.HiddenInput(),
            "cantidad": forms.NumberInput(attrs={'class': 'form-control'}),
            #"tipo_producto": forms.Select(attrs={'disabled': 'disabled'}),
            #"producto": forms.Select(attrs={'disabled': 'disabled'}),
            #"version_producto": forms.Select(attrs={'disabled': 'disabled'}),
            "menu": forms.Select(attrs={'class': 'form-control'}),
        }


class EncargadoForm(forms.ModelForm):
    password1 = forms.CharField(max_length=20, required=False, widget=forms.PasswordInput())
    password2 = forms.CharField(max_length=20, required=False, widget=forms.PasswordInput())

    class Meta:
        model = Encargado
        fields = [
            'first_name',
            'last_name',
            'email', ]

    def clean(self):
        super(EncargadoForm, self).clean()
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if not self.user.is_authenticated():
            if not password1:
                raise forms.ValidationError("La contraseña es requerida")

        if password1 != password2:
            msg = 'Las claves no coinciden'
            self._errors["password1"] = self.error_class([msg])
            raise forms.ValidationError(msg)

        return self.cleaned_data

    def save(self, commit=True):
        # import ipdb; ipdb.set_trace()
        user = super(EncargadoForm, self).save(commit=False)
        user.set_password(self.cleaned_data.get('password1'))
        user.is_active = True
        if commit:
            user.save()

        return user


class PromocionPrecioForm(forms.ModelForm):
    """
    El form para actualizar el precio y descuento de la promocion
    """

    def __init__(self, *args, **kwargs):
        super(PromocionPrecioForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            self.fields['precio'].widget.attrs['readonly'] = True
            self.fields['imagen'].widget.attrs['required'] = True

    class Meta:
        model = Promocion
        fields = ('descuento', 'precio', 'imagen')
        widgets = {
            "descuento": forms.NumberInput(attrs={'class': 'form-control'}),
            "precio": forms.NumberInput(attrs={'class': 'form-control'}),
        }


class ProductoParaArmarForm(forms.ModelForm):

    class Meta:
        model = ProductoParaArmar
        fields = ('tipo_producto', 'slogan', 'version')
        widgets = {
            "slogan": forms.TextInput(attrs={'class': 'form-control'}),
        }


class VersionProductoForm(forms.ModelForm):

    def __init__(self, clasificaciones, *args, **kwargs):
        super(VersionProductoForm, self).__init__(*args, **kwargs)
        self.fields['clasificacion'].queryset = clasificaciones

    class Meta:
        model = VersionProducto
        fields = ('clasificacion', 'imagen_producto', 'precio', 'producto')
        widgets = {
            'producto': forms.HiddenInput(),
            'precio': forms.NumberInput(attrs={'step': 0.01,
                                               'class': 'form-control'}),
        }


class SeccionProductoForm(forms.ModelForm):

    class Meta:
        model = SeccionProducto
        fields = ('nombre', 'tipo_ingrediente', 'descripcion', 'obligatoria',
                  'excluyente', 'cantidad_exclusiones', 'producto')
        widgets = {
            'producto': forms.HiddenInput(),
            "nombre": forms.TextInput(attrs={'class': 'form-control'}),
            "descripcion": forms.Textarea(attrs={'class': 'form-control'}),
            "cantidad_exclusiones": forms.NumberInput(attrs={'class': 'form-control'}),

        }

    def clean(self):
        super(SeccionProductoForm, self).clean()
        excluyente = self.cleaned_data.get('excluyente')
        cantidad_exclusiones = self.cleaned_data.get('cantidad_exclusiones')

        if excluyente and cantidad_exclusiones is None:
            msg = 'Es una seccion excluyente debe indicar las cantidad de ' \
                  'exclusiones de la misma'
            self._errors["cantidad_exclusiones"] = self.error_class([msg])
            raise forms.ValidationError(msg)

        return self.cleaned_data


class IngredientesSeccionForm(forms.ModelForm):

    def __init__(self, producto, *args, **kwargs):
        super(IngredientesSeccionForm, self).__init__(*args, **kwargs)
        self.fields['seccion'].queryset = producto.secciones.all()

    class Meta:
        model = IngredientesSeccion
        fields = ('seccion', 'ingrediente')


class TiempoPromedioProductoForm(forms.ModelForm):

    class Meta:
        model = TiempoPromedioProducto
        fields = ('producto', 'cantidad_minima', 'cantidad_maxima',
                  'promedio')
        widgets = {
            "cantidad_minima": forms.NumberInput(
                attrs={'class': 'form-control'}),
            "cantidad_maxima": forms.NumberInput(
                attrs={'class': 'form-control'}),
            "promedio": forms.NumberInput(attrs={'class': 'form-control'}),
        }

    def clean(self):
        super(TiempoPromedioProductoForm, self).clean()
        cantidad_minima = self.cleaned_data.get('cantidad_minima')
        cantidad_maxima = self.cleaned_data.get('cantidad_maxima')

        if cantidad_minima > cantidad_maxima:
            msg = 'La cantidad máxima debe ser mayor a la cantidad mínima '
            self._errors["cantidad_maxima"] = self.error_class([msg])
            raise forms.ValidationError(msg)

        return self.cleaned_data


class TiempoPromedioEntregaForm(forms.ModelForm):

    class Meta:
        model = TiempoPromedioEntrega
        fields = ('dia_semanal', 'hora_desde', 'hora_hasta', 'delivery',
                  'promedio')
        widgets = {
            "hora_desde": forms.TextInput(
                attrs={'class': 'form-control'}),
            "hora_hasta": forms.TextInput(
                attrs={'class': 'form-control'}),
            "delivery": forms.NumberInput(attrs={'class': 'form-control'}),
            "promedio": forms.NumberInput(attrs={'class': 'form-control'}),
        }

    def clean(self):
        super(TiempoPromedioEntregaForm, self).clean()
        hora_desde = self.cleaned_data.get('hora_desde')
        hora_hasta = self.cleaned_data.get('hora_hasta')

        if hora_desde > hora_hasta:
            msg = 'La hora hasta debe ser posteria a la hora desde'
            self._errors["hora_hasta"] = self.error_class([msg])
            raise forms.ValidationError(msg)

        return self.cleaned_data


class TiempoPromedioColaForm(forms.ModelForm):

    class Meta:
        model = TiempoPromedioCola
        fields = ('dia_semanal', 'hora_desde', 'hora_hasta',
                  'cantidad_cocineros', 'promedio')
        widgets = {
            "hora_desde": forms.TextInput(
                attrs={'class': 'form-control'}),
            "hora_hasta": forms.TextInput(
                attrs={'class': 'form-control'}),
            "cantidad_cocineros": forms.NumberInput(
                attrs={'class': 'form-control'}),
            "promedio": forms.NumberInput(attrs={'class': 'form-control'}),
        }

    def clean(self):
        super(TiempoPromedioColaForm, self).clean()
        hora_desde = self.cleaned_data.get('hora_desde')
        hora_hasta = self.cleaned_data.get('hora_hasta')

        if hora_desde > hora_hasta:
            msg = 'La hora hasta debe ser posteria a la hora desde'
            self._errors["hora_hasta"] = self.error_class([msg])
            raise forms.ValidationError(msg)

        return self.cleaned_data


class EncargadoUserCreateForm(forms.ModelForm):

    password1 = forms.CharField(max_length=20,
                                required=True,  # will be overwritten by __init__()
                                help_text='',  # will be overwritten by __init__()
                                widget=forms.PasswordInput(
                                    attrs={'class': 'form-control'}),
                                label='Contrasena')

    password2 = forms.CharField(max_length=20,
                                required=True,  # will be overwritten by __init__()
                                help_text='',  # will be overwritten by __init__()
                                widget=forms.PasswordInput(
                                    attrs={'class': 'form-control'}),
                                label='Contrasena (otra vez)')

    def clean(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 != password2:
            raise forms.ValidationError('Los passwords no concuerdan')
        return self.cleaned_data

    class Meta:
        model = Encargado
        fields = (
            'username', 'first_name', 'last_name', 'email', 'password1',
            'password2', 'legajo'
        )
        widgets = {
            "username": forms.TextInput(attrs={'class': 'form-control'}),
            "first_name": forms.TextInput(attrs={'class': 'form-control'}),
            "last_name": forms.TextInput(attrs={'class': 'form-control'}),
            "email": forms.EmailInput(attrs={'class': 'form-control'}),
            "legajo": forms.NumberInput(attrs={'class': 'form-control'}),
        }


class EncargadoUserUpdateForm(forms.ModelForm):

    password1 = forms.CharField(max_length=20,
                                required=False,  # will be overwritten by __init__()
                                help_text='',  # will be overwritten by __init__()
                                widget=forms.PasswordInput(
                                    attrs={'class': 'form-control'}),
                                label='Contrasena')

    password2 = forms.CharField(max_length=20,
                                required=False,  # will be overwritten by __init__()
                                help_text='Ingrese la nueva contraseña (sólo si desea cambiarla)',  # will be overwritten by __init__()
                                widget=forms.PasswordInput(
                                    attrs={'class': 'form-control'}),
                                label='Contrasena (otra vez)')

    def clean(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 != password2:
            raise forms.ValidationError('Los passwords no concuerdan')
        return self.cleaned_data

    class Meta:
        model = Encargado
        fields = (
            'username', 'first_name', 'last_name', 'email', 'password1',
            'password2', 'legajo'
        )
        widgets = {
            "username": forms.TextInput(attrs={'class': 'form-control'}),
            "first_name": forms.TextInput(attrs={'class': 'form-control'}),
            "last_name": forms.TextInput(attrs={'class': 'form-control'}),
            "email": forms.EmailInput(attrs={'class': 'form-control'}),
            "legajo": forms.NumberInput(attrs={'class': 'form-control'}),
        }


class BusquedaPedidoForm(forms.Form):
    buscar = forms.CharField(required=False,
        widget=forms.TextInput(attrs={'class': 'form-control',
                                      'placeholder': 'Email ó telefono'}))
