from componentes import views_pedido
from custom_admin.views import sucursales_view
from custom_admin.views.ajax_views import update_order_status
from custom_admin.views.views_pedido import (
    pedidos_view, pedidos_management_view, BusquedaPedidoFormView,
    DetallePedidoView
)
from django.conf.urls import url
from django.contrib.auth.views import (
    login, logout, password_reset, password_reset_done,
    password_reset_complete, password_reset_confirm)
from views.auth_view import cms_password_change, admin_required, staff_required
from custom_admin.views import (
    views_promocion, splash_view, views_producto, views_tiempo_preparacion,
    views_encargado, views_empresa, views_reportes
)

urlpatterns = [
    url(r'^password-reset/$', password_reset,
        kwargs={'template_name': 'password_reset/form.html'},
        name='password-reset'),
    url(r'^password_reset/done/$', password_reset_done,
        kwargs={'template_name': 'password_reset/done.html'},
        name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        password_reset_confirm,
        kwargs={'template_name': 'password_reset/confirm.html'},
        name='password_reset_confirm'),
    url(r'^reset/done/$', password_reset_complete,
        kwargs={'template_name': 'password_reset/complete.html'},
        name='password_reset_complete'),

    url(r'^accounts/password_change/$', cms_password_change,
        kwargs={'template_name': 'change_password.html',
                'post_change_redirect': '/'}, name='cms_password_change'),

    # sucursales
    url(r'^gestion_sucursales$',
        sucursales_view.sucursales_view, name='sucursales'),

    # promocion
    url(r'^promocion/nueva_promocion/$',
        staff_required(views_promocion.PromocionCreateView.as_view()),
        name='nueva_promocion'),
    url(r'^promocion/promocion_list/$',
        staff_required(views_promocion.PromocionListView.as_view()),
        name='promocion_list'),
    url(r'^promocion/(?P<pk_promocion>\d+)/actuacion/$',
        staff_required(views_promocion.ActuacionPromocionCreateView.as_view()),
        name='actuacion_promocion'),
    url(r'^promocion/(?P<pk_promocion>\d+)/datos_basicos/$',
        staff_required(views_promocion.PromocionUpdateView.as_view()),
        name='datos_basicos_promocion'),
    url(r'^promocion/(?P<pk_promocion>\d+)/actuacion/(?P<pk>\d+)/elimina/$',
        staff_required(views_promocion.ActuacionPromocionDeleteView.as_view()),
        name='actuacion_promocion_elimina'),
    url(r'^promocion/(?P<pk_promocion>\d+)/detalles/$',
        staff_required(views_promocion.DetallePromocionCreateView.as_view()),
        name='detalle_promocion'),
    url(r'^promocion/(?P<pk_promocion>\d+)/detalle/(?P<pk>\d+)/elimina/$',
        staff_required(views_promocion.DetallePromocionDeleteView.as_view()),
        name='detalle_promocion_elimina'),
    url(r'^promocion/(?P<pk_promocion>\d+)/precio/$',
        staff_required(views_promocion.PromocionUpdatePrecioView.as_view()),
        name='promocion_precio'),
    url(r'^promocion/(?P<pk>\d+)/elimina/$',
        staff_required(views_promocion.PromocionDeleteView.as_view()),
        name='promocion_elimina'),

    # Producto para armar
    url(r'^producto_armar/nuevo_producto/$',
        staff_required(views_producto.ProductoParaArmarCreateView.as_view()),
        name='nuevo_producto_armar'),
    url(r'^producto_armar/(?P<pk_producto>\d+)/update/$',
        staff_required(views_producto.ProductoParaArmarUpdateView.as_view()),
        name='producto_armar_update'),
    url(r'^producto_armar/(?P<pk_producto>\d+)/version/$',
        staff_required(views_producto.VersionProductoCreateView.as_view()),
        name='version_producto'),
    url(r'^version/(?P<pk_producto>\d+)/delete/(?P<pk_version>\d+)/$',
        staff_required(views_producto.version_delete_view),
        name='version_elimina'),
    url(r'^producto_armar/(?P<pk_producto>\d+)/seccion/$',
        staff_required(views_producto.SeccionProductoCreateView.as_view()),
        name='seccion_producto'),
    url(r'^seccion/(?P<pk_producto>\d+)/delete/(?P<pk_seccion>\d+)/$',
        staff_required(views_producto.seccion_delete_view),
        name='seccion_elimina'),
    url(r'^producto_armar/(?P<pk_producto>\d+)/ingredientes/$',
        staff_required(views_producto.IngredientesSeccionCreateView.as_view()),
        name='ingredientes_producto'),
    url(r'^ingrediente/(?P<pk_producto>\d+)/delete/(?P<pk_ingrediente>\d+)/$',
        staff_required(views_producto.ingrediente_delete_view),
        name='ingrediente_elimina'),
    url(r'^producto_armar/(?P<pk_producto>\d+)/confirmar/$',
        staff_required(views_producto.ProductoParaArmarConfirmarView.as_view()),
        name='producto_armar_confirmar'),
    url(r'^producto_armar/list/$',
        staff_required(views_producto.ProductoParaArmarListView.as_view()),
        name='producto_armar_list'),
    url(r'^producto_armar/(?P<pk>\d+)/elimina/$',
        staff_required(views_producto.ProductoParaArmarDeleteView.as_view()),
        name='producto_armar_elimina'),

    # Tiempo preparacion
    url(r'^tiempo/promedio_pedido_cola/$',
        staff_required(
            views_tiempo_preparacion.TiempoPromedioColaCreateView.as_view()),
        name='nuevo_promedio_cola'),
    url(r'^tiempo/promedio_pedido_cola/list/$',
        staff_required(
            views_tiempo_preparacion.TiempoPromedioColaListView.as_view()),
        name='promedio_cola_list'),
    url(r'^tiempo/(?P<pk>\d+)/promedio_pedido_cola/elimina/$',
        staff_required(
            views_tiempo_preparacion.TiempoPromedioColaDeleteView.as_view()),
        name='promedio_cola_elimina'),
    url(r'^tiempo/(?P<pk>\d+)/promedio_pedido_cola/update/$',
        staff_required(
            views_tiempo_preparacion.TiempoPromedioColaUpdateView.as_view()),
        name='promedio_cola_update'),
    url(r'^tiempo/promedio_entrega/$',
        staff_required(
            views_tiempo_preparacion.TiempoPromedioEntregaCreateView.as_view()),
        name='nuevo_promedio_entrega'),
    url(r'^tiempo/promedio_entrega/list/$',
        staff_required(
            views_tiempo_preparacion.TiempoPromedioEntregaListView.as_view()),
        name='promedio_entrega_list'),
    url(r'^tiempo/(?P<pk>\d+)/promedio_entrega/elimina/$',
        staff_required(
            views_tiempo_preparacion.TiempoPromedioEntregaDeleteView.as_view()),
        name='promedio_entrega_elimina'),
    url(r'^tiempo/(?P<pk>\d+)/promedio_entrega/update/$',
        staff_required(
            views_tiempo_preparacion.TiempoPromedioEntregaUpdateView.as_view()),
        name='promedio_entrega_update'),
    url(r'^tiempo/promedio_producto/$',
        staff_required(
            views_tiempo_preparacion.TiempoPromedioProductoCreateView.as_view()),
        name='nuevo_promedio_producto'),
    url(r'^tiempo/promedio_producto/list/$',
        staff_required(
            views_tiempo_preparacion.TiempoPromedioProductoListView.as_view()),
        name='promedio_producto_list'),
    url(r'^tiempo/(?P<pk>\d+)/promedio_producto/elimina/$',
        staff_required(
            views_tiempo_preparacion.TiempoPromedioProductoDeleteView.as_view()),
        name='promedio_producto_elimina'),
    url(r'^tiempo/(?P<pk>\d+)/promedio_producto/update/$',
        staff_required(
            views_tiempo_preparacion.TiempoPromedioProductoUpdateView.as_view()),
        name='promedio_producto_update'),

    # URL de IFRAME
    url(r'^configuracion/componentes/(?P<componente>[\w\-]+)$',
        splash_view.componentes_iframe, name='componentes_iframe'),
    url(r'^configuracion/recursos/(?P<recurso>[\w\-]+)$',
        splash_view.recursos_de_empresa_iframe, name='recursos_iframe'),
    url(r'^configuracion/ubicaciones/(?P<ubicacion>[\w\-]+)$',
        splash_view.ubicaciones_iframe, name='ubicaciones_iframe'),
    url(r'^configuracion/auth/(?P<auth>[\w\-]+)$',
        splash_view.auth_iframe, name='auth_iframe'),

    # encargado
    url(r'^encargado/nuevo_encargado/$',
        admin_required(views_encargado.EncargadoCreateView.as_view()),
        name='nuevo_encargado'),
    url(r'^encargado/(?P<pk>\d+)/actualizar_encargado/$',
        admin_required(views_encargado.EncargadoUpdateView.as_view()),
        name='actualizar_encargado'),
    url(r'^encargado/encargado_list/$',
        admin_required(views_encargado.EncargadoListView.as_view()),
        name='encargado_list'),
    url(r'^encargado/(?P<pk>\d+)/eliminar_encargado/$',
        admin_required(views_encargado.EncargadoDeleteView.as_view()),
        name='eliminar_encargado'),

    # empresa

    url(r'^configuracion/parametros/?$', admin_required(splash_view.parametros_iframe),
        name='configuracion_parametros'),


    url(r'^admin/login/?$', login, {'template_name': 'login.html'},
        name='custom_admin_login'),
    url(r'^admin/logout/?$', logout,
        {'next_page': '/administrador/admin/login?next=/administrador/'},
        name='custom_admin_logout'),
    url(r'^$', splash_view.dashboard, name='dashboard'),

    # pedidos
    url(r'^pedidos/$', staff_required(pedidos_view), name='pedidos_view'),
    url(r'^gestor_pedidos/$', staff_required(pedidos_management_view), name='order_management_view'),
    url(r'^cambiar_estado_pedido/$', staff_required(update_order_status),
        name='cambiar_estado_pedido'),
    url(r'^pedidos/buscar/$', staff_required(BusquedaPedidoFormView.as_view()),
        name='pedido_busqueda'),
    url(r'^pedidos/(?P<pk_pedido>\d+)/detalle/$',
        staff_required(DetallePedidoView.as_view()),
        name='pedido_detalle'),

    # reportes
    url(r'^reportes/clientes/$',
        admin_required(splash_view.ClientesVariosDatosJson.as_view()),
        name='reportes_clientes'),
    url(r'^reportes/sucursal/$',
        admin_required(splash_view.SucursalesVariosDatosJson.as_view()),
        name='reportes_sucursales'),
    url(r'^reportes/clientes/datos/$',
        admin_required(views_reportes.UsuarioDatosReporteDetailView.as_view()),
        name='reportes_clientes_datos'),
    url(r'^reportes/sucursal/datos/$',
        admin_required(views_reportes.SucursalDatosReporteDetailView.as_view()),
        name='reportes_sucursal_datos'),
    url(r'^reportes/red_social/$',
        admin_required(
            views_reportes.UsuarioRegistroRedSocialReporteDetailView.as_view()),
        name='reportes_red_social'),
    url(r'^reportes/pedidos/sucursal/$',
        admin_required(
            views_reportes.PedidosPorSucursalReporteDetailView.as_view()),
        name='reportes_pedidos_por_sucursal'),
    url(r'^reportes/clientes/exporta/$',
        admin_required(views_reportes.exporta_clientes_csv),
        name='reportes_clientes_exporta'),
    url(r'^reportes/sucursales/exporta/$',
        admin_required(views_reportes.exporta_sucursales_csv),
        name='reportes_sucursales_exporta'),
    url(r'^reportes/tipo_user/$',
        admin_required(views_reportes.SocialAccountReporteDetailView.as_view()),
        name='reportes_tipo_user'),
    url(r'^reportes/evolucion_user/$',
        admin_required(views_reportes.EvolucionUserReporteDetailView.as_view()),
        name='reportes_evolucion_user'),
    url(r'^reportes/top_ten_consumidores/$',
        admin_required(views_reportes.TopTenReporteDetailView.as_view()),
        name='reportes_top_ten_consumidores'),
    url(r'^reportes/top_ten_barrio/$',
        admin_required(views_reportes.TopTenBarrioDetailView.as_view()),
        name='reportes_top_ten_barrio'),
]
