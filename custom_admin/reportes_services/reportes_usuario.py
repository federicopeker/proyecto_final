# -*- coding: utf-8 -*-

import pygal
import os
from pygal.style import Style, RedBlueStyle

from django.conf import settings
from componentes.models import Pedido, ClienteProfile, HistorialEstadoPedido
from componentes.pedido_services import PedidoServices
from recursos_de_empresa.models import Sucursal
from allauth.socialaccount.models import SocialAccount
import logging as _logging

logger = _logging.getLogger(__name__)


ESTILO_AZUL_ROJO_AMARILLO = Style(
    background='transparent',
    plot_background='transparent',
    foreground='#555',
    foreground_light='#555',
    foreground_dark='#555',
    opacity='1',
    opacity_hover='.6',
    transition='400ms ease-in',
    colors=('#428bca', '#5cb85c', '#5bc0de', '#f0ad4e', '#d9534f',
            '#a95cb8', '#5cb8b5', '#caca43', '#96ac43', '#ca43ca')
)



class UsuarioReporteServices():

    def obetner_count_google(self):
        google = SocialAccount.objects.filter(provider='google')
        count_google_masculino = 0
        count_google_femenino = 0
        for user in google:
            if user.extra_data['gender'] == 'male':
                count_google_masculino += 1
            elif user.extra_data['gender'] == 'female':
                count_google_femenino += 1
        return count_google_masculino, count_google_femenino

    def obetner_count_facebook(self):
        facebook = SocialAccount.objects.filter(provider='facebook')
        count_acebook_masculino = 0
        count_facebook_femenino = 0
        for user in facebook:
            if user.extra_data['gender'] == 'male':
                count_acebook_masculino += 1
            elif user.extra_data['gender'] == 'female':
                count_facebook_femenino += 1
        return count_acebook_masculino, count_facebook_femenino

    def obtener_count_total_pedidos(self, pedidos):

        count_total = 0
        for cliente in pedidos:
            count_total += cliente['cantidad_pedidos']

        return count_total

    def obtener_lista_cliente_porcentaje(self, pedidos, count_total):
        clientes = []
        for cliente in pedidos:
            cant_pedidos = int(cliente['cantidad_pedidos'])
            porcentaje_usuario = (cant_pedidos * 100) / count_total
            red_social = cliente['cliente__user__socialaccount__provider']
            nombre = cliente['cliente__user__first_name'] + " "
            nombre += cliente['cliente__user__last_name']
            clientes.append([red_social, nombre, porcentaje_usuario])
        return clientes

    def obtener_lista_barrio_porcentaje(self, pedidos, count_total):
        barrios = []
        for barrio in pedidos:
            cant_pedidos = int(barrio['cantidad_pedidos'])
            porcentaje_barrio = (cant_pedidos * 100) / count_total
            nombre = barrio['barrio__nombre']
            barrios.append([nombre, porcentaje_barrio])
        return barrios

    def obtener_cliente_count_(self):
        pedido_service = PedidoServices()
        pedidos_cancelados = pedido_service.obtener_pedidos_por_estado(
            Pedido.objects.all(), HistorialEstadoPedido.CANCELADO)
        clientes_cancelados = pedido_service.obtener_count_pedidos_generico(
            pedidos_cancelados)
        pedidos_entregados = pedido_service.obtener_pedidos_por_estado(
            Pedido.objects.all(), HistorialEstadoPedido.ENTREGADO)
        clientes_entregados = pedido_service.obtener_count_pedidos_generico(
            pedidos_entregados)

        clientes = []

        for cliente in clientes_entregados:
            cliente_cancelado = clientes_cancelados.filter(
                cliente=cliente['cliente__id'])
            if cliente_cancelado.exists():
                cliente.update({'cantidad_cancelados': cliente_cancelado.first()['cantidad_pedidos']})
            else:
                cliente.update({'cantidad_cancelados': 0})
            clientes.append(cliente)

        for cliente in clientes_cancelados:
            cliente_entregado = clientes_entregados.filter(
                cliente=cliente['cliente__id'])
            if not cliente_entregado.exists():
                cliente.update({'cantidad_cancelados': cliente['cantidad_pedidos'], 'cantidad_pedidos': 0})
                clientes.append(cliente)

        return clientes

    def obtener_count_sucursales(self):
        pedido_service = PedidoServices()
        sucursales_list = []
        for sucursal in Sucursal.objects.all():
            pedidos = Pedido.objects.filter(sucursal=sucursal)
            if pedidos.count() > 0:
                cancelados = pedido_service.obtener_pedidos_por_estado(pedidos, HistorialEstadoPedido.CANCELADO).count()
                entregados = pedido_service.obtener_pedidos_por_estado(pedidos, HistorialEstadoPedido.ENTREGADO)
                en_tiempo_id = [pedido.id for pedido in entregados
                                if pedido.get_historico_estado_actual().fecha.time() < pedido.hora_entrega]
                en_tiempo = entregados.filter(id__in=en_tiempo_id).count()
                demorado_id = [pedido.id for pedido in entregados
                               if pedido.get_historico_estado_actual().fecha.time() > pedido.hora_entrega]
                demorado = entregados.filter(id__in=demorado_id).count()
                if cancelados > 0 or en_tiempo > 0 or demorado > 0:
                    sucursales_list.append((sucursal.nombre, cancelados, en_tiempo, demorado))
        return sucursales_list

    def _calcular_estadisticas(self):
        #pedidos = Pedido.objects.pedido_count_cliente()
        pedido_service = PedidoServices()
        pedidos = pedido_service.obtener_pedidos_exclude_definicion()
        pedidos_barrio = pedido_service.obtener_count_pedidos_barrio(pedidos)
        pedidos = pedido_service.obtener_count_pedidos(pedidos)
        sucursales = self.obtener_count_sucursales()

        # year
        pedidos_year = pedidos.filter(fechaPedido__year=2016)
        count_total = self.obtener_count_total_pedidos(pedidos_year)
        clientes_year = self.obtener_lista_cliente_porcentaje(pedidos_year,
                                                              count_total)
        pedidos_barrio_year = pedidos_barrio.filter(fechaPedido__year=2016)
        count_total = self.obtener_count_total_pedidos(pedidos_barrio_year)
        barrios_year = self.obtener_lista_barrio_porcentaje(pedidos_barrio_year,
                                                            count_total)

        clientes_month = {}

        for mes in range(1, 13):
            pedidos_mes = pedidos.filter(fechaPedido__month=mes)
            count_total = self.obtener_count_total_pedidos(pedidos_mes)
            clientes_mes = self.obtener_lista_cliente_porcentaje(pedidos_mes,
                                                                 count_total)
            clientes_month.update({str(mes): clientes_mes})

        barrios_month = {}

        for mes in range(1, 13):
            pedidos_mes = pedidos_barrio.filter(fechaPedido__month=mes)
            count_total = self.obtener_count_total_pedidos(pedidos_mes)
            barrio_mes = self.obtener_lista_barrio_porcentaje(pedidos_mes,
                                                              count_total)
            barrios_month.update({str(mes): barrio_mes})

        dic_estadisticas = {
            'clientes_year': clientes_year,
            'clientes_month': clientes_month,
            'sucursales': sucursales,
            'barrios_year': barrios_year,
            'barrios_month': barrios_month,
        }
        return dic_estadisticas

    def general(self):
        estadisticas = self._calcular_estadisticas()

        if estadisticas:
            logger.info("Generando grafico para reportes de usuario")

        tipo_user_femenino = pygal.SolidGauge(inner_radius=0.70, show_legends=False)
        percent_formatter = lambda x: '{:.10g}'.format(x)
        dollar_formatter = lambda x: '{:.10g}$'.format(x)
        tipo_user_femenino.value_formatter = percent_formatter
        tipo_user_femenino.title = 'Cantidad de usuarios mujeres por rango de edad'
        tipo_user_femenino.add('hasta 20', [{'value': 80, 'max_value': 1203}],
                  formatter=lambda x: '{:.10g}(hasta 20)'.format(x))
        tipo_user_femenino.add('20 a 30', [{'value': 456, 'max_value': 1203}],
                  formatter=lambda x: '{:.10g}(20 a 30)'.format(x))
        tipo_user_femenino.add('30 a 40', [{'value': 353, 'max_value': 1203}],
                               formatter=lambda x: '{:.10g}(30 a 40)'.format(x))
        tipo_user_femenino.add('40 a 60', [{'value': 197, 'max_value': 1203}],
                  formatter=lambda x: '{:.10g}(40 a 60)'.format(x))
        tipo_user_femenino.add('+60', [{'value': 117, 'max_value': 1203}],
                  formatter=lambda x: '{:.10g}(+60)'.format(x))
        tipo_user_femenino.render_to_file(os.path.join(settings.MEDIA_ROOT,
            "imagenes/reportes", "tipo_user_femenino.svg"))


        tipo_user_masculino = pygal.SolidGauge(inner_radius=0.70, show_legends=False)
        percent_formatter = lambda x: '{:.10g}'.format(x)
        dollar_formatter = lambda x: '{:.10g}$'.format(x)
        tipo_user_masculino.value_formatter = percent_formatter
        tipo_user_masculino.title = 'Cantidad de usuarios masculinos por rango de edad'
        tipo_user_masculino.add('hasta 20', [{'value': 73, 'max_value': 806}],
                  formatter=lambda x: '{:.10g}(hasta 20)'.format(x))
        tipo_user_masculino.add('20 a 30', [{'value': 261, 'max_value': 806}],
                  formatter=lambda x: '{:.10g}(20 a 30)'.format(x))
        tipo_user_masculino.add('30 a 40', [{'value': 239, 'max_value': 806}],
                               formatter=lambda x: '{:.10g}(30 a 40)'.format(x))
        tipo_user_masculino.add('40 a 60', [{'value': 180, 'max_value': 806}],
                  formatter=lambda x: '{:.10g}(40 a 60)'.format(x))
        tipo_user_masculino.add('+60', [{'value': 53, 'max_value': 806}],
                  formatter=lambda x: '{:.10g}(+60)'.format(x))
        tipo_user_masculino.render_to_file(os.path.join(settings.MEDIA_ROOT,
                                                       "imagenes/reportes",
                                                       "tipo_user_masculino.svg"))

        tipo_user_ambos = pygal.SolidGauge(inner_radius=0.70, show_legends=False)
        percent_formatter = lambda x: '{:.10g}'.format(x)
        dollar_formatter = lambda x: '{:.10g}$'.format(x)
        tipo_user_ambos.value_formatter = percent_formatter
        tipo_user_ambos.title = 'Cantidad de usuarios por rango de edad'
        tipo_user_ambos.add('hasta 20', [{'value': 153, 'max_value': 2009}],
                                formatter=lambda x: '{:.10g}(hasta 20)'.format(
                                    x))
        tipo_user_ambos.add('20 a 30', [{'value': 717, 'max_value': 2009}],
                                formatter=lambda x: '{:.10g}(20 a 30)'.format(
                                    x))
        tipo_user_ambos.add('30 a 40', [{'value': 592, 'max_value': 2009}],
                                formatter=lambda x: '{:.10g}(30 a 40)'.format(
                                    x))
        tipo_user_ambos.add('40 a 60', [{'value': 377, 'max_value': 2009}],
                                formatter=lambda x: '{:.10g}(40 a 60)'.format(
                                    x))
        tipo_user_ambos.add('+60', [{'value': 170, 'max_value': 2009}],
                                formatter=lambda x: '{:.10g}(+60)'.format(x))
        tipo_user_ambos.render_to_file(os.path.join(settings.MEDIA_ROOT,
                                                        "imagenes/reportes",
                                                        "tipo_user_ambos.svg"))

        evolucion_registro = pygal.Line()
        evolucion_registro.title = 'Evolucion de registro de usuarios'
        evolucion_registro.x_labels = ["Ene", "Feb", "Mar", "Abr", "May", "Jun",
                                       "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"]
        evolucion_registro.add('Femenino',
                       [80, 100, 33, 44, 56, 35, 123, 89, 67, 98, 107, 118])
        evolucion_registro.add('Masculino',
                        [65, 87, 23, 31, 48, 28, 104, 78, 51, 88, 91, 95])
        evolucion_registro.add('Ambos',
                        [145, 187, 56, 75, 104, 63, 227, 167, 118, 186, 198, 213])
        evolucion_registro.render_to_file(os.path.join(settings.MEDIA_ROOT,
                                                    "imagenes/reportes",
                                                    "evolucion_registro.svg"))
        #evolucion_registro.render_table(style=True)



        # registro por red social
        count_google_masculino, count_google_femenino = self.obetner_count_google()
        count_facebook_masculino, count_facebook_femenino = self.obetner_count_facebook()
        total_gmail_femenino = count_google_femenino
        total_facebook_femenino = count_facebook_femenino
        total_social_femenino = total_gmail_femenino + total_facebook_femenino

        total_gmail_masculino = count_google_masculino
        total_facebook_masculino = count_facebook_masculino
        total_social_masculino = total_gmail_masculino + total_facebook_masculino

        total_gmail = SocialAccount.objects.filter(provider='google').count()
        total_facebok = SocialAccount.objects.filter(provider='facebook').count()
        total_social = SocialAccount.objects.all().count()

        sucursales_bar = []
        for sucursal in estadisticas['sucursales']:
            line_chart = pygal.HorizontalBar(print_values=True, height=250, width=1000,
                                             no_data_text='No tiene pedidos esta sucursal',
                                             legend_at_bottom=True)
            line_chart.title = 'Sucursal {0}'.format(sucursal[0])
            line_chart.add('Pedidos Cancelados', sucursal[1])
            line_chart.add('Pedidos Completados En tiempo', sucursal[2])
            line_chart.add('Pedidos Completados Demorados', sucursal[3])
            nombre_file = "{0}.svg".format(sucursal[0])
            line_chart.render_to_file(os.path.join(settings.MEDIA_ROOT,
                                                           "imagenes/reportes",
                                                           nombre_file))
            sucursales_bar.append(nombre_file)

        return {
            'estadisticas': estadisticas,
            'tipo_user_femenino': tipo_user_femenino,
            'tipo_user_masculino': tipo_user_masculino,
            'tipo_user_ambos': tipo_user_ambos,
            'evolucion_registro': evolucion_registro.render_table(
                style=True, total=True),
            'total_gmail_femenino': total_gmail_femenino,
            'total_facebook_femenino': total_facebook_femenino,
            'total_social_femenino': total_social_femenino,
            'total_gmail_masculino': total_gmail_masculino,
            'total_facebook_masculino': total_facebook_masculino,
            'total_social_masculino': total_social_masculino,
            'total_gmail': total_gmail,
            'total_facebok': total_facebok,
            'total_social': total_social,
            'sucursales_bar': sucursales_bar
        }

    def registro_red_social(self):
        # registro por red social
        count_google_masculino, count_google_femenino = \
            self.obetner_count_google()
        count_facebook_masculino, count_facebook_femenino = \
            self.obetner_count_facebook()
        total_gmail_femenino = count_google_femenino
        total_facebook_femenino = count_facebook_femenino
        total_social_femenino = total_gmail_femenino + total_facebook_femenino

        total_gmail_masculino = count_google_masculino
        total_facebook_masculino = count_facebook_masculino
        total_social_masculino = total_gmail_masculino + \
                                 total_facebook_masculino

        total_gmail = SocialAccount.objects.filter(provider='google').count()
        total_facebok = SocialAccount.objects.filter(
            provider='facebook').count()
        total_social = SocialAccount.objects.all().count()
        return {
            'total_gmail_femenino': total_gmail_femenino,
            'total_facebook_femenino': total_facebook_femenino,
            'total_social_femenino': total_social_femenino,
            'total_gmail_masculino': total_gmail_masculino,
            'total_facebook_masculino': total_facebook_masculino,
            'total_social_masculino': total_social_masculino,
            'total_gmail': total_gmail,
            'total_facebok': total_facebok,
            'total_social': total_social,
        }

    def pedidos_por_sucursal(self):
        sucursales_bar = []
        sucursales = self.obtener_count_sucursales()
        for sucursal in sucursales:
            line_chart = pygal.HorizontalBar(print_values=True, height=250,
                                             width=1000,
                                             no_data_text='No tiene pedidos esta sucursal',
                                             legend_at_bottom=True)
            line_chart.title = 'Sucursal {0}'.format(sucursal[0])
            line_chart.add('Pedidos Cancelados', sucursal[1])
            line_chart.add('Pedidos Completados En tiempo', sucursal[2])
            line_chart.add('Pedidos Completados Demorados', sucursal[3])
            nombre_file = "{0}.svg".format(sucursal[0])
            line_chart.render_to_file(os.path.join(settings.MEDIA_ROOT,
                                                   "imagenes/reportes",
                                                   nombre_file))
            sucursales_bar.append(nombre_file)
        return sucursales_bar

    def tipo_user_registrado(self):
        tipo_user_femenino = pygal.SolidGauge(inner_radius=0.70,
                                              show_legends=False)
        percent_formatter = lambda x: '{:.10g}'.format(x)
        dollar_formatter = lambda x: '{:.10g}$'.format(x)
        tipo_user_femenino.value_formatter = percent_formatter
        tipo_user_femenino.title = 'Cantidad de usuarios mujeres por rango de edad'
        tipo_user_femenino.add('hasta 20', [{'value': 1, 'max_value': 8}],
                               formatter=lambda x: '{:.10g}(hasta 20)'.format(
                                   x))
        tipo_user_femenino.add('20 a 30', [{'value': 3, 'max_value': 8}],
                               formatter=lambda x: '{:.10g}(20 a 30)'.format(x))
        tipo_user_femenino.add('30 a 40', [{'value': 2, 'max_value': 8}],
                               formatter=lambda x: '{:.10g}(30 a 40)'.format(x))
        tipo_user_femenino.add('40 a 60', [{'value': 1, 'max_value': 8}],
                               formatter=lambda x: '{:.10g}(40 a 60)'.format(x))
        tipo_user_femenino.add('+60', [{'value': 1, 'max_value': 8}],
                               formatter=lambda x: '{:.10g}(+60)'.format(x))
        tipo_user_femenino.render_to_file(os.path.join(settings.MEDIA_ROOT,
                                                       "imagenes/reportes",
                                                       "tipo_user_femenino.svg"))

        tipo_user_masculino = pygal.SolidGauge(inner_radius=0.70,
                                               show_legends=False)
        percent_formatter = lambda x: '{:.10g}'.format(x)
        dollar_formatter = lambda x: '{:.10g}$'.format(x)
        tipo_user_masculino.value_formatter = percent_formatter
        tipo_user_masculino.title = 'Cantidad de usuarios masculinos por rango de edad'
        tipo_user_masculino.add('hasta 20', [{'value': 0, 'max_value': 4}],
                                formatter=lambda x: '{:.10g}(hasta 20)'.format(
                                    x))
        tipo_user_masculino.add('20 a 30', [{'value': 2, 'max_value': 4}],
                                formatter=lambda x: '{:.10g}(20 a 30)'.format(
                                    x))
        tipo_user_masculino.add('30 a 40', [{'value': 1, 'max_value': 4}],
                                formatter=lambda x: '{:.10g}(30 a 40)'.format(
                                    x))
        tipo_user_masculino.add('40 a 60', [{'value': 1, 'max_value': 4}],
                                formatter=lambda x: '{:.10g}(40 a 60)'.format(
                                    x))
        tipo_user_masculino.add('+60', [{'value': 0, 'max_value': 4}],
                                formatter=lambda x: '{:.10g}(+60)'.format(x))
        tipo_user_masculino.render_to_file(os.path.join(settings.MEDIA_ROOT,
                                                        "imagenes/reportes",
                                                        "tipo_user_masculino.svg"))

        tipo_user_ambos = pygal.SolidGauge(inner_radius=0.70,
                                           show_legends=False)
        percent_formatter = lambda x: '{:.10g}'.format(x)
        dollar_formatter = lambda x: '{:.10g}$'.format(x)
        tipo_user_ambos.value_formatter = percent_formatter
        tipo_user_ambos.title = 'Cantidad de usuarios por rango de edad'
        tipo_user_ambos.add('hasta 20', [{'value': 1, 'max_value': 12}],
                            formatter=lambda x: '{:.10g}(hasta 20)'.format(
                                x))
        tipo_user_ambos.add('20 a 30', [{'value': 5, 'max_value': 12}],
                            formatter=lambda x: '{:.10g}(20 a 30)'.format(
                                x))
        tipo_user_ambos.add('30 a 40', [{'value': 3, 'max_value': 12}],
                            formatter=lambda x: '{:.10g}(30 a 40)'.format(
                                x))
        tipo_user_ambos.add('40 a 60', [{'value': 2, 'max_value': 12}],
                            formatter=lambda x: '{:.10g}(40 a 60)'.format(
                                x))
        tipo_user_ambos.add('+60', [{'value': 1, 'max_value': 12}],
                            formatter=lambda x: '{:.10g}(+60)'.format(x))
        tipo_user_ambos.render_to_file(os.path.join(settings.MEDIA_ROOT,
                                                    "imagenes/reportes",
                                                    "tipo_user_ambos.svg"))

    def evolucion_user(self):
        evolucion_registro = pygal.Line(width=500, height=300, style=Style(
            title_font_size=8, label_font_size=8))
        evolucion_registro.title = 'Evolucion de registro de usuarios'
        evolucion_registro.x_labels = ["Ene", "Feb", "Mar", "Abr", "May", "Jun",
                                       "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"]
        evolucion_registro.add('Femenino',
                               [0, 2, 1, 0, 0, 1, 0, 0, 0, 1, 1, 2])
        evolucion_registro.add('Masculino',
                               [0, 0, 2, 0, 0, 1, 0, 0, 0, 0, 0, 1])
        evolucion_registro.add('Ambos',
                               [0, 2, 3, 0, 0, 2, 0, 0, 0, 1, 1, 3])
        evolucion_registro.render_to_file(os.path.join(settings.MEDIA_ROOT,
                                                       "imagenes/reportes",
                                                       "evolucion_registro.svg"))

    def top_ten_consumidores(self):
        pedido_service = PedidoServices()
        pedidos = pedido_service.obtener_pedidos_exclude_definicion()
        pedidos = pedido_service.obtener_count_pedidos(pedidos)
        # year
        pedidos_year = pedidos.filter(fechaPedido__year=2016)
        count_total = self.obtener_count_total_pedidos(pedidos_year)
        clientes_year = self.obtener_lista_cliente_porcentaje(pedidos_year,
                                                              count_total)

        clientes_month = {}

        for mes in range(1, 13):
            pedidos_mes = pedidos.filter(fechaPedido__month=mes)
            count_total = self.obtener_count_total_pedidos(pedidos_mes)
            clientes_mes = self.obtener_lista_cliente_porcentaje(pedidos_mes,
                                                                 count_total)
            clientes_month.update({str(mes): clientes_mes})

        pedidos = pedido_service.obtener_pedidos_exclude_definicion()
        pedidos = pedido_service.obtener_count_pedidos(pedidos)
        # year
        pedidos_year = pedidos.filter(fechaPedido__year=2017)
        count_total = self.obtener_count_total_pedidos(pedidos_year)
        clientes_year_2017 = self.obtener_lista_cliente_porcentaje(pedidos_year,
                                                              count_total)

        clientes_month_2017 = {}

        for mes in range(1, 3):
            pedidos_mes = pedidos.filter(fechaPedido__month=mes)
            count_total = self.obtener_count_total_pedidos(pedidos_mes)
            clientes_mes = self.obtener_lista_cliente_porcentaje(pedidos_mes,
                                                                 count_total)
            clientes_month_2017.update({str(mes): clientes_mes})

        return {
            'clientes_year': clientes_year,
            'clientes_month': clientes_month,
            'clientes_year_2017': clientes_year_2017,
            'clientes_month_2017': clientes_month_2017,
        }

    def top_ten_barrio(self):
        pedido_service = PedidoServices()
        pedidos = pedido_service.obtener_pedidos_exclude_definicion()
        pedidos_barrio = pedido_service.obtener_count_pedidos_barrio(pedidos)
        # year
        pedidos_barrio_year = pedidos_barrio.filter(fechaPedido__year=2016)
        count_total = self.obtener_count_total_pedidos(pedidos_barrio_year)
        barrios_year = self.obtener_lista_barrio_porcentaje(pedidos_barrio_year,
                                                            count_total)

        barrios_month = {}

        for mes in range(1, 13):
            pedidos_mes = pedidos_barrio.filter(fechaPedido__month=mes)
            count_total = self.obtener_count_total_pedidos(pedidos_mes)
            barrio_mes = self.obtener_lista_barrio_porcentaje(pedidos_mes,
                                                              count_total)
            barrios_month.update({str(mes): barrio_mes})

        pedidos = pedido_service.obtener_pedidos_exclude_definicion()
        pedidos_barrio = pedido_service.obtener_count_pedidos_barrio(pedidos)
        # year
        pedidos_barrio_year = pedidos_barrio.filter(fechaPedido__year=2017)
        count_total = self.obtener_count_total_pedidos(pedidos_barrio_year)
        barrios_year_2017 = self.obtener_lista_barrio_porcentaje(pedidos_barrio_year,
                                                                 count_total)

        barrios_month_2017 = {}

        for mes in range(1, 13):
            pedidos_mes = pedidos_barrio.filter(fechaPedido__month=mes)
            count_total = self.obtener_count_total_pedidos(pedidos_mes)
            barrio_mes = self.obtener_lista_barrio_porcentaje(pedidos_mes,
                                                              count_total)
            barrios_month_2017.update({str(mes): barrio_mes})

        return {
            'barrios_year': barrios_year,
            'barrios_month': barrios_month,
            'barrios_year_2017': barrios_year_2017,
            'barrios_month_2017': barrios_month_2017,
        }
