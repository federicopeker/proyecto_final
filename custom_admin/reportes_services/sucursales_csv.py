# -*- coding: utf-8 -*-

"""
Servicio de exportacion de base de datos de contactos para discador
"""

from __future__ import unicode_literals

import csv
import logging
import os
import json


from django.conf import settings
from sigep.utiles import crear_archivo_en_media_root
from django.utils.encoding import force_text
from recursos_de_empresa.models import Sucursal

logger = logging.getLogger(__name__)


class ArchivoDeReporteCsv(object):
    def __init__(self, sucursales):
        self._sucursales = sucursales
        self.nombre_del_directorio = 'reportes'
        self.prefijo_nombre_de_archivo = "-sucursales"
        self.sufijo_nombre_de_archivo = ".csv"
        self.nombre_de_archivo = "{0}{1}".format(
            self.prefijo_nombre_de_archivo, self.sufijo_nombre_de_archivo)
        self.url_descarga = os.path.join(settings.MEDIA_URL,
                                         self.nombre_del_directorio,
                                         self.nombre_de_archivo)
        self.ruta = os.path.join(settings.MEDIA_ROOT,
                                 self.nombre_del_directorio,
                                 self.nombre_de_archivo)

    def crear_archivo_en_directorio(self):
        if self.ya_existe():
            # Esto puede suceder si en un intento previo de depuracion, el
            # proceso es abortado, y por lo tanto, el archivo puede existir.
            logger.warn("ArchivoDeReporteCsv: Ya existe archivo CSV de "
                        "reporte para las sucursales. Archivo: %s. "
                        "El archivo sera sobreescrito",
                        self.ruta)

        crear_archivo_en_media_root(
            self.nombre_del_directorio,
            self.prefijo_nombre_de_archivo,
            self.sufijo_nombre_de_archivo)

    def escribir_archivo_csv(self, sucursales):

        with open(self.ruta, 'wb') as csvfile:


            # Creamos encabezado
            encabezado = []
            encabezado.append("nombre")
            encabezado.append("servicio")
            encabezado.append("direccion")
            encabezado.append("encargado")
            encabezado.append("telefono")
            encabezado.append("cantidad_pedido_realizado")
            encabezado.append("cantidad_pedido_cancelados")


            # Creamos csvwriter
            csvwiter = csv.writer(csvfile)

            # guardamos encabezado
            lista_encabezados_utf8 = [force_text(item).encode('utf-8')
                                      for item in encabezado]
            csvwiter.writerow(lista_encabezados_utf8)

            # Iteramos cada uno de los contactos, con los eventos de TODOS los intentos
            for sucursal in sucursales:
                lista_opciones = []

                # --- Buscamos datos
                lista_opciones.append(sucursal.nombre)
                servicio = " "
                if sucursal.ofrece_servicio_delivery:
                    servicio += "Delivery - "
                if sucursal.ofrece_servicio_para_llevar:
                    servicio += "Retiro en Sucursal - "
                if sucursal.ofrece_sercicio_consumo_sucursal:
                    servicio += "Consumo en Sucursal"
                lista_opciones.append(servicio)
                lista_opciones.append(u"{} {} {} ".format(sucursal.direccion,
                    sucursal.numero_direccion, sucursal.barrio.nombre,))
                if sucursal.encargado:
                    lista_opciones.append(sucursal.encargado.get_full_name())
                else:
                    lista_opciones.append("no tiene encargado asignado")
                telefonos = ""
                for telefono in sucursal.telefonosucursal_set.all():
                    telefonos += telefono.numero + " / "
                lista_opciones.append(telefonos)
                lista_opciones.append(sucursal.get_count_entregados())
                lista_opciones.append(sucursal.get_count_cancelados())


                # --- Finalmente, escribimos la linea

                lista_opciones_utf8 = [force_text(item).encode('utf-8')
                                       for item in lista_opciones]
                csvwiter.writerow(lista_opciones_utf8)

    def ya_existe(self):
        return os.path.exists(self.ruta)


class ExportarSucursalService(object):

    def crea_reporte_csv(self):
        sucursales = Sucursal.objects.all()
        archivo_de_reporte = ArchivoDeReporteCsv(sucursales)
        archivo_de_reporte.crear_archivo_en_directorio()

        archivo_de_reporte.escribir_archivo_csv(sucursales)

    def obtener_url_reporte_csv_descargar(self, sucursales):
        archivo_de_reporte = ArchivoDeReporteCsv(sucursales)
        if archivo_de_reporte.ya_existe():
            return archivo_de_reporte.url_descarga

        # Esto no debería suceder.
        logger.error("obtener_url_reporte_csv_descargar(): NO existe archivo"
                     " CSV de descarga para las sucursales")
        assert os.path.exists(archivo_de_reporte.url_descarga)
