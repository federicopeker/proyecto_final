# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import os
import datetime
import time
import tempfile
from pytz import timezone as pytz_timezone
from django.conf import settings
import logging as _logging


logger = _logging.getLogger(__name__)


def fecha_hora_actual():
    my_timezone = pytz_timezone('UTC')
    return my_timezone.localize(datetime.datetime.now())


def resolve_strftime(text):
    """Ejecuta strftime() en texto pasado por parametro"""
    return time.strftime(text)  # time.gmtime(


def crear_archivo_en_media_root(dirname_template, prefix, suffix=""):
    """Crea un archivo en el directorio MEDIA_ROOT. Si los directorios
    no existen, los crea tambien.

    Para la creacion del archivo usa `tempfile.mkstemp`

    Parametros:
        - dirname_template (directorio, acepta %Y, %m, etc.)
        - prefix (prefijo para nombre de archivo)
        - suffix (para poder especificar una extension)

    Devuelve: tupla con
        (directorio_relativo_a_MEDIA_ROOT, nombre_de_archivo)

    Ej:
        crear_archivo('data/%Y/%m', 'audio-original'):
            En este caso, se creara (si no existen) los
            directorios `data`, ANIO y MES, y crea un archivo que
            comienza con `audio-original`
    """
    assert dirname_template[-1] != '/'
    assert dirname_template[0] != '/'
    assert prefix.find('/') == -1

    # relative_filename = resolve_strftime(dirname_template)
    #output_directory_rel, tempfile_prefix = os.path.split(relative_filename)
    #output_directory_abs = os.path.join(
    #    settings.MEDIA_ROOT, output_directory_rel)

    relative_dirname = resolve_strftime(dirname_template)

    # Creamos directorios si no existen
    abs_output_dir = os.path.join(settings.MEDIA_ROOT, relative_dirname)
    if not os.path.exists(abs_output_dir):
        logger.info("Se crearan directorios: %s", abs_output_dir)
        os.makedirs(abs_output_dir, mode=0755)

    fd, output_filename = tempfile.mkstemp(dir=abs_output_dir, prefix=prefix,
        suffix=suffix)

    # Cerramos FD
    os.close(fd)

    os.chmod(output_filename, 0644)

    _, generated_filename = os.path.split(output_filename)
    return relative_dirname, generated_filename
