"""sigep URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
    Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic.base import TemplateView
from django.contrib.auth.decorators import login_required
from sigep import view_user

urlpatterns = [
    url(r'^user/update/$',
        login_required(view_user.CustomerUserUpdateView.as_view()),
        name="user_update"),
    url(r'^cliente_profile/update/$',
        login_required(view_user.ClienteProfileUpdateView.as_view()),
        name="cliente_profile_update"),
    url(r'^cliente_profile/nuevo/$',
        login_required(view_user.ClienteProfileCreateView.as_view()),
        name="cliente_profile_nuevo"),
    url(r'^admin/', admin.site.urls),
    url(r'^chaining/', include('smart_selects.urls')),
    url(r'^administrador/', include('custom_admin.urls')),
    url(r'^', include('website.urls')),
    url(r'^', include('componentes.urls')),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^accounts/profile/$', TemplateView.as_view(template_name='profile.html')),
    url(r'^', include('cms.urls')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


