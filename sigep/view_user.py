# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.contrib import messages
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.views.generic import ListView, CreateView, UpdateView
from sigep.form_user import ClienteUserChangeForm, ClienteProfileForm
from componentes.models import ClienteProfile


class CustomerUserUpdateView(UpdateView):
    model = User
    form_class = ClienteUserChangeForm
    template_name = 'user/user_update_form.html'

    def get_object(self, queryset=None):
        return self.request.user

    def form_valid(self, form):
        # TODO: not sure how to enforce *minimum* length of a field.

        form.save()
        messages.add_message(self.request, messages.INFO,
                             'User profile updated')
        return super(CustomerUserUpdateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('home_page_view')


class ClienteProfileUpdateView(UpdateView):
    model = ClienteProfile
    form_class = ClienteProfileForm
    template_name = 'user/user_update_cliente_form.html'

    def get_object(self, queryset=None):
        return ClienteProfile.objects.get(user=self.request.user)

    def dispatch(self, *args, **kwargs):
        #user = User.objects.get(pk=self.request.user)
        cliente_profile = ClienteProfile.objects.obtener_cliente_profile_editar(
            user=self.request.user)
        if not cliente_profile:
            return HttpResponseRedirect('/cliente_profile/nuevo/')
        else:
            return super(ClienteProfileUpdateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        # TODO: not sure how to enforce *minimum* length of a field.

        form.save()
        messages.add_message(self.request, messages.INFO,
                             'User profile updated')
        return super(ClienteProfileUpdateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('home_page_view')


class ClienteProfileCreateView(CreateView):
    model = ClienteProfile
    form_class = ClienteProfileForm
    template_name = 'user/user_update_cliente_form.html'

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user

        self.object.save()

        return super(ClienteProfileCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('home_page_view')
