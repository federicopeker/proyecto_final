# -*- coding: utf-8 -*-

from django import forms
from django.contrib.auth.models import User
from componentes.models import ClienteProfile


class ClienteUserChangeForm(forms.ModelForm):

    class Meta():
        model = User
        fields = ('first_name', 'last_name', 'email')
        exclude = ('password',)


class ClienteProfileForm(forms.ModelForm):

    class Meta():
        model = ClienteProfile
        fields = ('telefono', 'direccion', 'numero_direccion', 'piso', 'depto',
                  'codigo_postal', 'localidad', 'barrio')
