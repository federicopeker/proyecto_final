from django.contrib import admin
from ubicaciones.models import *
# Register your models here.


class BarrioAdmin(admin.ModelAdmin):
    ordering = ['nombre',]
    search_fields = ['nombre', ]

admin.site.register(Barrio, BarrioAdmin)
admin.site.register(Localidad)
admin.site.register(Provincia)
