from __future__ import unicode_literals

from django.db import models


class Provincia(models.Model):
    """Clase Provincia
    Atributos: nombre, localidad"""
    nombre = models.CharField(max_length=100, unique=True)
    # localidad = models.ManyToManyField(Localidad)

    def __unicode__(self):
        return self.nombre


class Localidad(models.Model):
    """Clase Localidad
    Atributos: nombre, barrio"""
    nombre = models.CharField(max_length=100, unique=True)
    # barrio = models.ManyToManyField(Barrio)
    provincia = models.ForeignKey(Provincia)

    def __unicode__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Localidades'


class Barrio(models.Model):
    """Clase Barrio
    Atributo: nombre"""
    nombre = models.CharField(max_length=50, unique=True)
    localidad = models.ForeignKey(Localidad)

    def __unicode__(self):
        return self.nombre
