#encoding:utf-8
from django.shortcuts import render, render_to_response
from django.contrib import messages
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.views.generic import CreateView

from componentes.models import Pedido
from website.forms import ContactoForm, MensajeContactoForm
from django.core.mail.message import EmailMultiAlternatives
from django.template import RequestContext

__author__ = 'mobydigital'


def home_view(request):
    ctx = {}
    if "pk_pedido" in request.session and request.session['pk_pedido'] is not None:
        orden_de_pedido = Pedido.objects.get(id=request.session["pk_pedido"])
        request.session["pk_pedido"] = None
        ctx['orden_de_pedido'] = orden_de_pedido
    return render(request, 'home.html', ctx)


def products_view(request):
    return render(request, 'productos.html')


def contact_view(request):
    if request.method == "POST":
        contact_form = ContactoForm(request.POST, request.FILES)
        message_contact_form = MensajeContactoForm(request.POST, request.FILES)

        if contact_form.is_valid() and message_contact_form.is_valid():
            nombre = contact_form.cleaned_data['nombre']
            apellido = contact_form.cleaned_data['apellido']
            email = contact_form.cleaned_data['email']
            mensaje = message_contact_form.cleaned_data['mensaje']

            email_context = {
                'titulo': 'Has recibido un correo del usuario:',
                'usuario': nombre + ',' + apellido,
                'mensaje': mensaje,
            }
            # se renderiza el template con el context
            email_html = render_to_string('emails/email_contacto.html', email_context)
            email_text = strip_tags(email_html)
            correo = EmailMultiAlternatives('Mensaje de Usuario: '+nombre+', '+apellido, email_text, email,
                                           ['info.sigep@gmail.com'],)
            # se especifica que el contenido es html
            correo.attach_alternative(email_html, 'text/html')
            # se envía el correo
            correo.send()

            return render_to_response('RecursosDeEmpresa/message_sent.html', context_instance=RequestContext(request))

    else:
        contact_form = ContactoForm()
        message_contact_form = MensajeContactoForm()

    boton = 'Enviar Solicitud'
    ctx = {'contact_form': contact_form, 'message_contact_form': message_contact_form, 'boton': boton}
    return render_to_response('contacto.html', ctx, context_instance=RequestContext(request))

