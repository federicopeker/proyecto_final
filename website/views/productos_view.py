# -*- coding: utf-8 -*-

from componentes.models import Producto, TipoProducto
from django.views.generic.list import ListView


class ProductosListView(ListView):

    template_name = 'producto_tipo.html'
    context_object_name = 'producto'
    model = Producto

    def get_context_data(self, **kwargs):
        context = super(ProductosListView, self).get_context_data(
           **kwargs)
        tipo_producto = TipoProducto.objects.get(
            pk=self.kwargs['pk_tipo_producto'])
        context['tipo_producto'] = tipo_producto.nombre
        context['productos'] = Producto.objects.\
            obtener_productos_por_tipo_producto(self.kwargs['pk_tipo_producto'])
        return context
