from django.shortcuts import render_to_response
from django.template.context import RequestContext
from recursos_de_empresa.models import Sucursal
from django.shortcuts import render


def sucursales_view(request):
    sucursales = Sucursal.objects.all()
    return render_to_response('nuevo_listado_sucursales.html',
                              {'lista_sucursales': sucursales},
                              context_instance=RequestContext(request))


def detalle_sucursal(request):
    id_sucursal = int(request.GET['id_sucursal'])
    servicio = int(request.GET['servicio_tipo'])
    sucursal = Sucursal.objects.get(id=id_sucursal)
    servicio_class = "delivery_button"
    if servicio == 1:
        servicio_class = "delivery_button"
    elif servicio == 2:
        servicio_class = "sucursal_button"
    data = {
        'sucursal': sucursal,
        'servicio_class': servicio_class,
        'servicio': servicio,

    }
    return render(request, 'partial/detalle_sucursal.html', data)
