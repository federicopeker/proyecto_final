# -*- coding: utf-8 -*-

from componentes.models import Promocion
from django.views.generic.list import ListView


class PromocionListView(ListView):

    template_name = 'promocion_list.html'
    context_object_name = 'promocion'
    model = Promocion

    def get_context_data(self, **kwargs):
        context = super(PromocionListView, self).get_context_data(
           **kwargs)
        #context['promociones'] = Promocion.objects.filter(
         #   estado=Promocion.ESTADO_ACTIVA)
        context['promociones'] = Promocion.objects.all()
        return context
