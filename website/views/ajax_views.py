from django.shortcuts import render

from componentes.models import Pedido
from libs.util.deco_utils import jsonResponse
from recursos_de_empresa.models import ZonaCobertura

__author__ = 'mobydigital'


@jsonResponse
def get_sucursales_by_zona_cobertura(request):
    id_barrio = request.GET.get('id_barrio')
    sucursales_list = []
    if id_barrio:
        zonas = ZonaCobertura.objects.filter(barrios__id__exact=id_barrio).\
            values('sucursal', 'sucursal__ofrece_servicio_delivery')
        for zona in zonas:
            sucursal_obj = {
                'id': zona['sucursal'],
            }
            if zona['sucursal__ofrece_servicio_delivery']:
                sucursales_list.append(sucursal_obj)
        return sucursales_list


def detalle_pedido(request):
    id_pedido = int(request.GET['id_pedido'])
    pedido = Pedido.objects.get(id=id_pedido)
    data = {
        'pedido': pedido,
    }
    return render(request, 'partial/detalle_pedido.html', data)
