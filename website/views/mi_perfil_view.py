# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.views.generic.list import ListView
from componentes.models import Pedido
from componentes.pedido_services import PedidoServices


class MyPedidosList(ListView):
    """
    Esta vista muestras los pedidos realizados por los clientes
    """

    template_name = 'mispedidos.html'
    context_object_name = 'pedido'
    model = Pedido

    def get_context_data(self, **kwargs):
        context = super(MyPedidosList, self).\
            get_context_data(**kwargs)
        service = PedidoServices()
        if self.request.user.is_authenticated():
            cliente = self.request.user.clienteprofile
            pedidos = Pedido.objects.obtener_pedido_por_cliente(cliente)
            pedidos = service.obtener_pedidos_exclude_definicion(pedidos)
            context['pedidos'] = pedidos.order_by('-id')
        return context