function actualizarCantidad(input, pk_pedido)
	{
		var id_det = $(input).attr('id');
		var cantidad = $(input).val();
		$.get("/pedido/productos_actualizar/" + pk_pedido +"/" + cantidad +"/"+id_det+"/",function(data){
		$('#TU_PEDIDO_ARMADO').html(data);
    	});

	}

function updateDetalle(input, pk_pedido)
{
    var id_det = $(input).attr('id');
    var cantidad = $(input).val();
    $.get("/pedido/updateDetallePedido/" + pk_pedido + "/" +cantidad+"/"+id_det+"/",function(data){
    	$('#UpdateDetalle').html(data);
    });
}


function quitarDetalle(aquitar, pk_pedido)
    {
        var id_det = $(aquitar).attr('id');       
        $.get("/pedido/"+ pk_pedido +"/del/productos/"+id_det,function(data){
        $('#TU_PEDIDO_ARMADO').html(data);
        $("#TU_PEDIDO_ARMADO").find("select").each(function(){
    		var valor=$(this).attr("cantidad");
    		$(this).val(valor);
    	});
        }).done(function () {
            $('select').select2();
        });
    }
    


function agregarProductoAlPedido(codigoProducto, pk_pedido){
	
		var fila = $(codigoProducto).closest("tr");
        var numeroCombo = $(fila).find("select").find("option:selected").val();
        var idProducto = $(codigoProducto).attr('id');
        if(numeroCombo >0)
        {
        	$.ajax({
        		data: {cantidad: numeroCombo, codigoProducto:codigoProducto.id, tipoProducto: 1,
				pk_pedido: pk_pedido },
        		datatype: "json",
        		type:"get",
        		url: '/ajax/agregar_producto_pedido/',
        		success: function(response){
        		
            	$.get("/pedido/actualizar_tu_pedido/"+ pk_pedido + "/",function(data){
                    $('#TU_PEDIDO_ARMADO').html(data);
                    $("#TU_PEDIDO_ARMADO").find("select").each(function(){
                		var valor=$(this).attr("cantidad");
                		$(this).val(valor);
                	});
                    }).done(function () {
                        $('select').select2();
                        });
        		}
        	});

        }
}



function agregarMenuAlPedido(codigoProducto, pk_pedido){
	
	var fila = $(codigoProducto).closest("tr");
    var numeroCombo = $(fila).find("select").find("option:selected").val();
    var idProducto = $(codigoProducto).attr('id');
    if(numeroCombo >0)
    {
    	$.ajax({
    		data: {cantidad: numeroCombo, codigoProducto:codigoProducto.id, tipoProducto: 2,
			pk_pedido: pk_pedido },
    		datatype: "json",
    		type:"get",
    		url: '/ajax/agregar_producto_pedido/',
    		success: function(response){
    		
            	$.get("/pedido/actualizar_tu_pedido/"+ pk_pedido + "/",function(data){
                    $('#TU_PEDIDO_ARMADO').html(data);
                    $("#TU_PEDIDO_ARMADO").find("select").each(function(){
                		var valor=$(this).attr("cantidad");
                		$(this).val(valor);
                	});
                }).done(function () {
                        $('select').select2();
                        });
    		}
    	});

    }
}

function agregarPromocionAlPedido(codigoProducto, pk_pedido){
	
	var fila = $(codigoProducto).closest("tr");
    var numeroCombo = $(fila).find("select").find("option:selected").val();
    var idProducto = $(codigoProducto).attr('id');
    if(numeroCombo >0)
    {
    	$.ajax({
    		data: {cantidad: numeroCombo, codigoProducto:codigoProducto.id, tipoProducto: 3,
			pk_pedido: pk_pedido},
    		datatype: "json",
    		type:"get",
    		url: '/ajax/agregar_producto_pedido/',
    		success: function(response){
    		
            	$.get("/pedido/actualizar_tu_pedido/"+ pk_pedido + "/",function(data){
                    $('#TU_PEDIDO_ARMADO').html(data);
                    $("#TU_PEDIDO_ARMADO").find("select").each(function(){
                		var valor=$(this).attr("cantidad");
                		$(this).val(valor);
                	});
                }).done(function () {
                        $('select').select2();
                        });
    		}
    	});

    }
}

function actualizarfavorito(span, pk_pedido)
	{
		var favorito = $(span).attr("favorito");
		selector_id = '#star' + pk_pedido
		$.get("/pedido/actualizar_favorito_pedido/" + pk_pedido +"/" + favorito +"/",function(data){
		$(selector_id).html(data);
    	});

	}