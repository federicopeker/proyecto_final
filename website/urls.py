from django.conf.urls import url
from django.views.generic import TemplateView
from website import views
from website.views import home_view, products_view, sucursales_view,\
    contact_view, detalle_sucursal
from website.views.ajax_views import get_sucursales_by_zona_cobertura, detalle_pedido
from website.views import productos_view, promocion_view, mi_perfil_view

urlpatterns = [
    url(r'^$', home_view, name='home_page_view'),
    url(r'^productos/?$', products_view, name='products_view'),
    url(r'^listado_sucursales/?$', sucursales_view, name='sucursales_view'),
    url(r'^detalle_sucursal/?$', detalle_sucursal,
        name='detalle_sucursal_view'),
    url(r'^detalle_pedido/?$', detalle_pedido,
        name='detalle_pedido_view'),
    url(r'^contacto/?$', contact_view, name='contacto_view'),
    url(r'^sucursales_ajax/?$', get_sucursales_by_zona_cobertura,
        name='sucursales_por_barrio'),
    url(r'^productos/(?P<pk_tipo_producto>\d+)/tipo/?$',
        productos_view.ProductosListView.as_view(), name='productos_tipo'),
    url(r'^promociones/$', promocion_view.PromocionListView.as_view(),
        name='promociones_list'),
    url(r'^mispedidos/$', mi_perfil_view.MyPedidosList.as_view(),
        name='mispedidos_list'),

]

