from django import forms


class ContactoForm(forms.Form):
    nombre = forms.CharField()
    apellido = forms.CharField()
    email = forms.EmailField()


class MensajeContactoForm(forms.Form):
    mensaje = forms.CharField(widget=forms.Textarea(attrs={'cols': 50, 'rows': 10}))
