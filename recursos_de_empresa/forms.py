from django import forms
from django.contrib.admin.widgets import FilteredSelectMultiple

from recursos_de_empresa.models import ZonaCobertura
from ubicaciones.models import Barrio


class ZonaCoberturaForm(forms.ModelForm):
    barrios = forms.ModelMultipleChoiceField(queryset=Barrio.objects.all().order_by('nombre'),
                                             widget=FilteredSelectMultiple("Barrios",
                                                                           is_stacked=False))

    class Meta:
        model = ZonaCobertura
        fields = '__all__'