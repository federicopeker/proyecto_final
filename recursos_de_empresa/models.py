# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from smart_selects.db_fields import ChainedForeignKey
from ubicaciones.models import Provincia, Localidad, Barrio
from geoposition.fields import GeopositionField
#from componentes.models import HistorialEstadoPedido
# Create your models here.


class Persona(models.Model):
    """Clase Persona
    Atributos: nombre, apellido, sexo, email, numero_documento, tipo_documento, domicilio, usuario"""
    sexo_choice = (
                  ('F', 'Femenino'),
                  ('M', 'Masculino'),
                  )
    dni = 1
    libreta_civica = 2
    TIPO_DOCUMENTO_CHOICES = (
        (1, 'Documento Nacional de Identidad'),
        (2, 'Libreta Civica')
    )

    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)
    sexo = models.CharField(max_length=1, choices=sexo_choice)
    email = models.EmailField(max_length=50, help_text='to@example.com')
    tipo_documento= models.IntegerField(choices=TIPO_DOCUMENTO_CHOICES, default=dni)
    numero_documento = models.IntegerField()
    direccion = models.CharField(max_length=250, verbose_name='Dirección')
    numero_direccion = models.IntegerField(verbose_name='Número')
    piso = models.IntegerField(blank=True, null=True)
    depto = models.CharField(max_length=50, blank=True, null=True)
    codigo_postal = models.CharField(max_length=100, verbose_name='Código Postal')
    provincia = models.ForeignKey(Provincia)
    localidad = models.ForeignKey(Localidad)
    barrio = models.ForeignKey(Barrio)
    telefono_particular = models.CharField(max_length=100, help_text='Código de área + Nº. Ej.: 351-473-9643.',
                                           verbose_name='Teléfono Particular')
    telefono_domicilio = models.CharField(max_length=100, blank=True, null=True,
                                          help_text='Código de área + Nº. Ej.: 351-473-9643.',
                                          verbose_name='Teléfono Domicilio')

    def __unicode__(self):
        return self.nombre + self.apellido


class Sucursal(models.Model):
    """ Clase Sucursal
    Atributos: codigo, nombre, domicilio, calificacion_servicio, imagen"""

    ofrece_servicio_delivery = models.BooleanField(default=True, verbose_name='ofece servicio de delivery?')
    ofrece_servicio_para_llevar = models.BooleanField(default=True, verbose_name= 'ofrece servicio para retirar?')
    ofrece_sercicio_consumo_sucursal = models.BooleanField(default=False,
                                                           verbose_name='ofrece servicio para consumir en el local?')
    nombre = models.CharField(max_length=100)
    direccion = models.CharField(max_length=250, verbose_name='Dirección')
    numero_direccion = models.IntegerField(verbose_name='Número')
    codigo_postal = models.CharField(max_length=100, verbose_name='Código Postal')
    provincia = models.ForeignKey(Provincia)
    localidad = ChainedForeignKey(Localidad, chained_field='provincia', chained_model_field='provincia', auto_choose=True)
    barrio = ChainedForeignKey(Barrio, chained_field='localidad', chained_model_field='localidad', auto_choose=True)
    posicion = GeopositionField(help_text='Ayuda a dar mejor ubicacion de la posicion de la sucursal', blank=True, null=True)
    encargado = models.ForeignKey('encargado', blank=True, null=True)
    imagen = models.ImageField(upload_to='imagenes/Sucursales', verbose_name='Imágen')
    # tipo_servicio = models.(choices=TIPO_SERVICIO_CHOICES, default=delivery)
    email = models.EmailField(max_length=50, help_text='to@example.com')
    horarios_de_atencion_display = models.TextField(max_length=300, blank=True, null=True,
                                                    verbose_name='Horario a mostrar')

    def vista_previa(self):
        return '<a href="/media/%s"><img src="/media/%s" width=50px height=50px/></a>' % (self.imagen, self.imagen)
    vista_previa.allow_tags = True

    def __unicode__(self):
        return self.nombre

    def get_horarios_de_atencion(self):
        return [unicode(horarios) for horarios in self.horarioatencion_set.all()]

    def get_count_entregados(self):
        from componentes.models import HistorialEstadoPedido
        estado_entregado = HistorialEstadoPedido.ENTREGADO
        includes = [pedido.id for pedido in self.pedidossucursal.all()
                    if pedido.get_historico_estado_actual().estado ==
                    estado_entregado]
        return self.pedidossucursal.filter(id__in=includes).count()

    def get_count_cancelados(self):
        from componentes.models import HistorialEstadoPedido
        estado_cancelado = HistorialEstadoPedido.CANCELADO
        includes = [pedido.id for pedido in self.pedidossucursal.all()
                    if pedido.get_historico_estado_actual().estado ==
                    estado_cancelado]
        return self.pedidossucursal.filter(id__in=includes).count()

    class Meta:
        verbose_name_plural = 'Sucursales'


class HorarioAtencion (models.Model):
    lunes = models.BooleanField(default=False,  verbose_name='Lunes')
    martes = models.BooleanField(default=False,  verbose_name='Martes')
    miercoles = models.BooleanField(default=False,  verbose_name='Miercoles')
    jueves = models.BooleanField(default=False,  verbose_name='Jueves')
    viernes = models.BooleanField(default=False,  verbose_name='Viernes')
    sabado = models.BooleanField(default=False,  verbose_name='Sabado')
    domingo = models.BooleanField(default=False,  verbose_name='Domingo')
    hora_desde = models.TimeField(verbose_name='Hora desde', help_text='Formato 24hs')
    hora_hasta = models.TimeField(verbose_name='Hora hasta', help_text='Formato 24hs')
    sucursal = models.ForeignKey(Sucursal)

    def __unicode__(self):
        horarios = ''
        if self.lunes:
            horarios += u'Lunes, '
        if self.martes:
            horarios += u'Martes, '
        if self.miercoles:
            horarios += u'Miercoles, '
        if self.jueves:
            horarios +=u'Jueves, '
        if self.viernes:
            horarios +=u'Viernes, '
        if self.sabado:
            horarios +=u'Sabados, '
        if self.domingo:
            horarios +=u'Domingos, '
        horarios += u'Desde: %s Hasta: %s\n'%(self.hora_desde, self.hora_hasta)
        return horarios

    def __str__(self):
        return unicode(self).encode('utf-8')

    class Meta:
        verbose_name_plural = 'Horas y Dias de Atención'


class CostoEnvio(models.Model):
    """Clase Costo Envio Sucursal
    Atributos:  costo, kmDesde, kmHasta"""
    costo_envio = models.DecimalField(max_digits=5, decimal_places=2, verbose_name='Costo de Envio($)')
    kmDesde = models.IntegerField(verbose_name='Km desde')
    kmHasta = models.IntegerField(verbose_name='Km desde')
    sucursal = models.ForeignKey(Sucursal)

    def __unicode__(self):
        return self.costo_envio

    class Meta:
        verbose_name_plural = 'Costos de Envio'


class ZonaCobertura(models.Model):
    """Clase Zona cobertira Sucursal: indica a que barrios puede realizar envios la sucursal
    Atributo: sucursal, barrios"""
    nombre = models.CharField(max_length=50, blank=True, null=True)
    sucursal = models.ForeignKey(Sucursal, related_name='zonas')
    barrios = models.ManyToManyField(Barrio)
    precio_envio = models.DecimalField(max_digits=5, decimal_places=2)

    def __unicode__(self):
        return self.sucursal.nombre


class TelefonoSucursal(models.Model):
    """Clase TelfonoSucursal
    Atributos: numero, sucursal"""
    numero = models.CharField(max_length=100, help_text='Código de área + Nº. Ej.: 351-473-9643.')
    sucursal = models.ForeignKey(Sucursal)

    def __unicode__(self):
        return self.numero

    class Meta:
        verbose_name = 'Teléfono de Sucursal'
        verbose_name_plural = 'Teléfonos de Sucursales'


class Encargado(User):
    """Clase Encargado, simboliza a la persona que va a estar al frente de una sucursal
    Atributos: legajo y todos los datos comunes de un usuario"""
    legajo = models.IntegerField(unique=True,  blank=True, null=True)


    class Meta:
        verbose_name_plural = 'Administradores de Sucursales'
        verbose_name = 'Administrador de Sucursal'

    def __unicode__(self):
        full_name = self.get_full_name()
        return full_name if full_name else str(self.legajo)
