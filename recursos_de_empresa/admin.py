# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from django.forms import Textarea

from recursos_de_empresa.forms import ZonaCoberturaForm
from recursos_de_empresa.models import *
# Register your models here.


class TelefonoSucursalInline(admin.StackedInline):
    model = TelefonoSucursal
    verbose_name_plural = 'Telefonos de Sucursales'
    extra = 1


class HorarioAtencionInline(admin.TabularInline):
    model = HorarioAtencion
    extra = 1


class ZonaCoberturaInline(admin.StackedInline):
    model = ZonaCobertura
    form = ZonaCoberturaForm
    extra = 1
    #max_num = 1


class SucursalAdmin(admin.ModelAdmin):
    fieldsets = (
        ('Datos de Sucursal', {
            'fields': ('nombre', 'email',
                       'imagen')
        }),
        ('Posicion', {
            'fields': ('posicion',)
        }),
        ('Informacion de Dirección', {
            'classes': ('wide', 'extrapretty'),
            'fields': (
                ('provincia', 'localidad', 'barrio'),
                ('direccion', 'numero_direccion', 'codigo_postal'))
        }),
        ('Servicios', {
                'fields': (('ofrece_servicio_delivery',),
                ('ofrece_servicio_para_llevar',),
                ('ofrece_sercicio_consumo_sucursal',))
        }),
        ('Encargado', {
            'fields': ('encargado',),
        }),
        ('Horarios a mostrar', {
            'fields': ('horarios_de_atencion_display',),
        }),

    )
    inlines = (TelefonoSucursalInline, HorarioAtencionInline, ZonaCoberturaInline)
    readonly_fields = ('id',)
    formfield_overrides = {
        models.TextField: {'widget': Textarea(
            attrs={'rows': 3,
                   'cols': 80})},
    }

admin.site.register(Sucursal, SucursalAdmin)
admin.site.register(Persona)
