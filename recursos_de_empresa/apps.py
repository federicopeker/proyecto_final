from __future__ import unicode_literals

from django.apps import AppConfig


class RecursosDeEmpresaConfig(AppConfig):
    name = 'recursos_de_empresa'
