# -*- coding: utf-8 -*-
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext as _
from django.contrib import admin
from cmscontent.models import (
    HeaderSliderPlugin, HeaderImageItemPlugin, PromotionSliderPlugin,
    PromotionItemPlugin, BrandSliderPlugin, BrandItemPlugin,
    PhotoArticleSliderPlugin, PhotoArticlePlugin, TipoProductoPlugin,
    TipoProductoItemPlugin, TipoProductoSugerenciaPlugin,
    TipoProductoItemSugerenciaPlugin
)


class CMSHeaderSliderPlugin(CMSPluginBase):
    model = HeaderSliderPlugin
    name = "Contenedor de Sliders principal"
    allow_children = True
    render_template = "plugins/header_slider.html"
    child_classes = ['CMSHeaderImageItemPlugin']

    def render(self, context, instance, placeholder):
        context.update({
            'object': instance,
            'placeholder': placeholder,
        })
        return context

plugin_pool.register_plugin(CMSHeaderSliderPlugin)


class CMSHeaderImageItemPlugin(CMSPluginBase):
    model = HeaderImageItemPlugin
    name = "Imagen de Slider Principal"
    require_parent = True
    render_template = 'plugins/header_image.html'
    parent_classes = ['CMSHeaderSliderPlugin']

    def render(self, context, instance, placeholder):
        context.update({
            'object': instance,
            'placeholder': placeholder,
        })
        return context

plugin_pool.register_plugin(CMSHeaderImageItemPlugin)


class CMSPromotionSliderPlugin(CMSPluginBase):
    model = PromotionSliderPlugin
    name = "Contenedor de Sliders de promo"
    allow_children = True
    render_template = "plugins/promotion_slider.html"
    child_classes = ['CMSPromotionItemPlugin']

    def render(self, context, instance, placeholder):
        context.update({
            'object': instance,
            'placeholder': placeholder,
        })
        return context

plugin_pool.register_plugin(CMSPromotionSliderPlugin)


class CMSPromotionItemPlugin(CMSPluginBase):
    model = PromotionItemPlugin
    name = "Imagen de Slider de promo"
    require_parent = True
    parent_classes = ['CMSPromotionSliderPlugin']
    render_plugin = False

plugin_pool.register_plugin(CMSPromotionItemPlugin)


class CMSBrandSliderPlugin(CMSPluginBase):
    model = BrandSliderPlugin
    name = "Contenedor de Sliders de marcas"
    allow_children = True
    render_template = "plugins/brand_slider.html"
    child_classes = ['CMSBrandItemPlugin']

    def render(self, context, instance, placeholder):
        context.update({
            'object': instance,
            'placeholder': placeholder,
        })
        return context

plugin_pool.register_plugin(CMSBrandSliderPlugin)


class CMSBrandItemPlugin(CMSPluginBase):
    model = BrandItemPlugin
    name = "Imagen de Slider de marcas"
    require_parent = True
    render_template = "plugins/brand_item.html"
    parent_classes = ['CMSBrandSliderPlugin']

    def render(self, context, instance, placeholder):
        context.update({
            'object': instance,
            'placeholder': placeholder,
        })
        return context

plugin_pool.register_plugin(CMSBrandItemPlugin)


class CMSPhotoArticleSliderPlugin(CMSPluginBase):
    model = PhotoArticleSliderPlugin
    name = 'Contenedor de slider de Articulos'
    render_template = "plugins/photo_article_slider.html"
    allow_children = True
    child_classes = ['CMSPhotoArticlePlugin']

    def render(self, context, instance, placeholder):
        context.update({
            'object': instance,
            'placeholder': placeholder,
        })
        return context

plugin_pool.register_plugin(CMSPhotoArticleSliderPlugin)


class CMSPhotoArticlePlugin(CMSPluginBase):
    model = PhotoArticlePlugin
    name = _("Plugin de Articulos")
    render_template = "plugins/photo_article.html"
    require_parent = True
    parent_classes = ['CMSPhotoArticleSliderPlugin']

    def render(self, context, instance, placeholder):
        context.update({
            'object': instance,
            'placeholder': placeholder,
        })
        return context

plugin_pool.register_plugin(CMSPhotoArticlePlugin)


class TipoProductoPlugin(CMSPluginBase):
    model = TipoProductoPlugin
    name = "Contenedor de Sliders de tipo producto"
    allow_children = True
    render_template = "plugins/tipo_producto_container.html"
    child_classes = ['TipoProductoItemPlugin']

    def render(self, context, instance, placeholder):
        context.update({
            'object': instance,
            'placeholder': placeholder,
        })
        return context

plugin_pool.register_plugin(TipoProductoPlugin)


class TipoProductoItemPlugin(CMSPluginBase):
    model = TipoProductoItemPlugin
    name = "Tipo de producto "
    require_parent = True
    parent_classes = ['TipoProductoPlugin']
    render_template = "plugins/tipo_producto_item.html"

    def render(self, context, instance, placeholder):
        context.update({
            'object': instance,
            'placeholder': placeholder,
        })
        return context


plugin_pool.register_plugin(TipoProductoItemPlugin)


class TipoProductoSugerenciaPlugin(CMSPluginBase):
    model = TipoProductoSugerenciaPlugin
    name = "Contenedor de Sliders para sugerencias"
    allow_children = True
    render_template = "plugins/tipo_producto_sugerencia_slider.html"
    child_classes = ['TipoProductoItemSugerenciaPlugin']

    def render(self, context, instance, placeholder):
        context.update({
            'object': instance,
            'placeholder': placeholder,
        })
        return context

plugin_pool.register_plugin(TipoProductoSugerenciaPlugin)


class TipoProductoItemSugerenciaPlugin(CMSPluginBase):
    model = TipoProductoItemSugerenciaPlugin
    name = "Tipo de producto "
    require_parent = True
    parent_classes = ['TipoProductoSugerenciaPlugin']
    render_template = "plugins/tipo_producto_sugerencia_item.html"

    def render(self, context, instance, placeholder):
        context.update({
            'object': instance,
            'placeholder': placeholder,
        })
        return context


plugin_pool.register_plugin(TipoProductoItemSugerenciaPlugin)
