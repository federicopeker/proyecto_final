# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from cms.extensions import PageExtension, extension_pool
from cms.models import CMSPlugin
from filer.fields.image import FilerImageField

from django.db import models
from componentes.models import TipoProducto
# Create your models here.


class HeaderSliderPlugin(CMSPlugin):
    title = models.CharField(max_length=140, verbose_name='titulo')

    def __unicode__(self):
        return self.title

    class Meta(object):
        verbose_name = 'Contenedor de slider principal'


class HeaderImageItemPlugin(CMSPlugin):
    imagen = FilerImageField(blank=True, null=True)
    texto_inferior = models.CharField(max_length=100, blank=True, null=True)

    def __unicode__(self):
        return self.texto_inferior


class PromotionSliderPlugin(CMSPlugin):
    titulo = models.CharField(max_length=140, blank=True, null=True)

    def __unicode__(self):
        return self.title


class PromotionItemPlugin(CMSPlugin):
    imagen = FilerImageField(blank=True, null=True, help_text='se recomienda resolucion 300x300')
    texto_inferior = models.CharField(max_length=100)
    url = models.CharField(max_length=250)


class BrandSliderPlugin(CMSPlugin):
    titulo = models.CharField(max_length=140, blank=True, null=True)

    def __unicode__(self):
        # if self.titulo
        return self.titulo

    class Meta(object):
        verbose_name = 'Contenedor de slider de marca'


class BrandItemPlugin(CMSPlugin):
    imagen = FilerImageField(blank=True, null=True, help_text='se recomienda resolucion 140x50')
    nombre = models.CharField(max_length=100)
    url = models.CharField(max_length=250, help_text='Ingrese url completa, incluyendo http, https o lo necesario')

    def __unicode__(self):
        # if self.titulo
        return self.nombre

    class Meta(object):
        verbose_name = 'Plugin de imagen de marca'


class PhotoArticleSliderPlugin(CMSPlugin):
    title = models.CharField(max_length=200, blank=True, null=True, verbose_name='titulo')
    subtitle = models.CharField(max_length=200, blank=True, null=True, verbose_name='subtitulo')
    rows_to_show = models.PositiveSmallIntegerField(default=2, verbose_name='filas a mostrar')
    slides_to_show = models.PositiveSmallIntegerField(default=2, verbose_name='slides a mostrar')
    slide_time = models.PositiveIntegerField(default=10, verbose_name='tiempo de transicion de slide')

    def __unicode__(self):
        return self.title

    class Meta(object):
        verbose_name = 'Contenedor de slider de Articulos'


class PhotoArticlePlugin(CMSPlugin):
    TARGET_BLANK = 'bla'
    TARGET_SELF = 'sel'
    TARGET_PARENT = 'par'
    TARGET_TOP = 'top'

    LINK_TARGETS = [(TARGET_BLANK, '_blank'),
                    (TARGET_SELF, '_self'),
                    (TARGET_PARENT, '_parent'),
                    (TARGET_TOP, '_top')]
    title = models.CharField(max_length=200, verbose_name='titulo')
    summary = models.TextField(blank=True, null=True, verbose_name='resumen')
    image = FilerImageField(blank=True, null=True, verbose_name='imagen', help_text='se recomienda resolucion 300x300')
    url = models.CharField(max_length=250, help_text='La url a la que redireccionara la noticia')
    target = models.CharField(max_length=3, choices=LINK_TARGETS, default=TARGET_SELF,
                              help_text="Accion que desencadena el click en el vinculo")

    def __unicode__(self):
        return self.title

    class Meta(object):
        verbose_name = 'Plugin de Artículos'


class TipoProductoPlugin(CMSPlugin):
    title = models.CharField(blank=True, null=True,max_length=140,
                             verbose_name='titulo')

    def __unicode__(self):
        return self.title

    class Meta(object):
        verbose_name = 'Contenedor de tipo de productos'


class TipoProductoItemPlugin(CMSPlugin):
    tipo_producto = models.ForeignKey(TipoProducto)
    order = models.PositiveIntegerField(help_text=
                                        "Indicar orden de muestra de productos")

    def __unicode__(self):
        return self.tipo_producto.nombre


class TipoProductoSugerenciaPlugin(CMSPlugin):
    title = models.CharField(blank=True, null=True,max_length=140,
                             verbose_name='titulo')

    def __unicode__(self):
        return self.title

    class Meta(object):
        verbose_name = 'Contenedor de tipo de productos para sugerencia'


class TipoProductoItemSugerenciaPlugin(CMSPlugin):
    tipo_producto = models.ForeignKey(TipoProducto)
    order = models.PositiveIntegerField(help_text=
                                        "Indicar orden de muestra de productos")

    def __unicode__(self):
        return self.tipo_producto.nombre
