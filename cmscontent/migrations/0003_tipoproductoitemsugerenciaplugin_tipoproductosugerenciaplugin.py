# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-11-17 18:07
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('componentes', '0004_auto_20161112_1807'),
        ('cms', '0013_urlconfrevision'),
        ('cmscontent', '0002_tipoproductoitemplugin_tipoproductoplugin'),
    ]

    operations = [
        migrations.CreateModel(
            name='TipoProductoItemSugerenciaPlugin',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('order', models.PositiveIntegerField(help_text='Indicar orden de muestra de productos')),
                ('tipo_producto', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='componentes.TipoProducto')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='TipoProductoSugerenciaPlugin',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(blank=True, max_length=140, null=True, verbose_name='titulo')),
            ],
            options={
                'verbose_name': 'Contenedor de tipo de productos',
            },
            bases=('cms.cmsplugin',),
        ),
    ]
