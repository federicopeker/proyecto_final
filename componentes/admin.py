# -*- coding: utf-8 -*-


from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from django.db import models
from django.forms import Textarea
from form_utils.widgets import ImageWidget
from componentes.models import (
    Clasificacion, Ingrediente, Promocion, Producto,
    Menu, TipoIngrediente, TipoProducto, Version,
    UnidadDeMedida, DetalleVersiones, DetalleMenu,
    DetalleIngredientes, DetallePromocion,
    Actuacion, ClienteProfile, ProductoParaArmar, SeccionProducto,
    IngredientesSeccion, VersionProducto
)


class ClasificacionAdmin(admin.ModelAdmin):
    search_fields = ('nombre', 'descripcion')
    list_display = ('nombre', 'descripcion')
    ordering = ('nombre',)


class IngredienteAdmin(admin.ModelAdmin):
    search_fields = ('nombre', )
    list_display = ('nombre', 'unidad_de_medida', 'precio', 'stock_actual',
                    'stock_minimo')

    fields = ('nombre', 'tipo_ingrediente', 'unidad_de_medida', 'imagen',
              'stock_actual', 'stock_minimo', 'stock_corte', 'precio')
    formfield_overrides = {
        models.ImageField: {'widget': ImageWidget},
    }


class DetallePromocion(admin.StackedInline):
    model = DetallePromocion
    extra = 1


class ActuacionPromocion(admin.StackedInline):
    model = Actuacion
    extra = 1


class PromocionAdmin(admin.ModelAdmin):
    inlines = [DetallePromocion, ActuacionPromocion]
    search_fields = ('nombre',)
    list_display = ('nombre', 'precio', 'stock', 'vista_previa',
                    'actuaciones_promocion', "estado")
    ordering = ('nombre', )
    fields = ('nombre', 'descripcion', 'imagen', 'precio', 'stock',
              'fecha_inicio', 'fecha_fin', "estado")
    formfield_overrides = {
        models.ImageField: {'widget': ImageWidget},
    }


class DetalleVersionInline(admin.StackedInline):
    model = DetalleVersiones
    extra = 1


class DetalleIngredientesInline(admin.StackedInline):
    model = DetalleIngredientes
    verbose_name_plural = "Detalle de Ingredientes"
    extra = 4


class ProductoAdmin(admin.ModelAdmin):
    inlines = [DetalleVersionInline, DetalleIngredientesInline]
    search_fields = ('nombre', 'tipo_producto__nombre')
    list_display = ('nombre', 'tipo_producto', 'estado')
    ordering = ('nombre',)
    fields = ('nombre', 'descripcion', 'tipo_producto', 'version',
              'estado',)
    formfield_overrides = {
        models.TextField: {'widget': Textarea(
            attrs={'rows': 3,
                   'cols': 80})},
    }


class DetalleMenuInline(admin.StackedInline):
    model = DetalleMenu
    extra = 1


class MenuAdmin(admin.ModelAdmin):
    inlines = [DetalleMenuInline]
    search_fields = ('nombre',)
    list_display = ('nombre', 'precio_venta', 'vista_previa')
    ordering = ('nombre',)
    fields = ('nombre', 'precio_venta', 'imagen')
    formfield_overrides = {
        models.ImageField: {'widget': ImageWidget},
    }


class TipoIngredienteAdmin(admin.ModelAdmin):
    search_fields = ('nombre',)
    ordering = ('nombre',)
    fields = ('nombre',)


class TipoProductoAdmin(admin.ModelAdmin):
    search_fields = ('nombre',)
    list_display = ('nombre', 'vista_previa')
    ordering = ('nombre',)
    fields = ('nombre', 'imagen')
    formfield_overrides = {
        models.ImageField: {'widget': ImageWidget},
    }


class UnidadDeMedidaAdmin(admin.ModelAdmin):
    search_fields = ('nombre',)
    ordering = ('nombre',)
    fields = ('nombre',)


class VersionAdmin(admin.ModelAdmin):
    search_fields = ('nombre',)
    ordering = ('nombre',)
    fields = ('nombre', 'clasificacion')


class ClienteProfileAdminInline(admin.StackedInline):
    model = ClienteProfile


class UserAdmin(BaseUserAdmin):
    inlines = (ClienteProfileAdminInline,)


class VersionProductoInline(admin.StackedInline):
    model = VersionProducto
    extra = 1


class SeccionProductoInline(admin.StackedInline):
    model = SeccionProducto
    extra = 1


class IngredientesSeccionProductoInline(admin.StackedInline):
    model = IngredientesSeccion
    extra = 1


class SeccionProductoAdmin(admin.ModelAdmin):
    inlines = [IngredientesSeccionProductoInline]
    search_fields = ('nombre', 'orden', 'tipo_ingrediente.nombre')
    list_display = ('producto', 'orden', 'nombre', 'tipo_ingrediente')
    list_filter = ('nombre', 'orden')
    ordering = ('producto', 'orden')


class ProductoParaArmarAdmin(admin.ModelAdmin):
    inlines = [VersionProductoInline, SeccionProductoInline]
    search_fields = ('tipo_producto__nombre', 'slogan')
    list_display = ('tipo_producto', 'slogan')
    list_filter = ('tipo_producto', 'slogan')
    ordering = ('tipo_producto', 'slogan')


admin.site.register(TipoIngrediente, TipoIngredienteAdmin)
admin.site.register(Clasificacion, ClasificacionAdmin)
admin.site.register(UnidadDeMedida, UnidadDeMedidaAdmin)
admin.site.register(TipoProducto, TipoProductoAdmin)
admin.site.register(Ingrediente, IngredienteAdmin)
# admin.site.register(DetalleIngredientes)
admin.site.register(Producto, ProductoAdmin)
admin.site.register(Menu, MenuAdmin)
# admin.site.register(Programacion,ProgramacionAdmin)
admin.site.register(Promocion, PromocionAdmin)
admin.site.register(Version, VersionAdmin)
# admin.site.register(DetalleVersiones, DetalleVersionesAdmin)
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(ProductoParaArmar, ProductoParaArmarAdmin)
admin.site.register(SeccionProducto, SeccionProductoAdmin)
