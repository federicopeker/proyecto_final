# -*- coding: utf-8 -*-

from django import forms
from componentes.models import Pedido
from datetime import datetime

from recursos_de_empresa.models import Sucursal
from captcha.fields import ReCaptchaField


class PedidoForm(forms.ModelForm):

    def __init__(self, sucursales, *args, **kwargs):
        super(PedidoForm, self).__init__(*args, **kwargs)
        self.fields['barrio'].empty_label = 'seleccione barrio'
        self.fields['sucursal'].queryset = sucursales
        self.fields['sucursal'].empty_label = 'seleccione local'

    class Meta:
        model = Pedido
        fields = ['direccion', 'piso', 'depto', 'barrio', 'sucursal', 'servicio']
        widgets = {
            "direccion": forms.TextInput(
                attrs={'placeholder': 'Dirección Ej: San Martín 145',
                       'class': 'form-control'}),
            "piso": forms.TextInput(
                attrs={'placeholder': 'Piso', 'class': 'form-control'}),
            "depto": forms.TextInput(
                attrs={'placeholder': 'Depto', 'class': 'form-control'}),
            "servicio": forms.HiddenInput()
        }


class HoraPedidoForm(forms.Form):
    captcha = ReCaptchaField()
    horaPedir = forms.TimeField(label='', widget=forms.TextInput(
        attrs={'placeholder': datetime.now().time().strftime("%H:%M"),
               'class': 'form-control'}))
    importe_abonar = forms.FloatField(label='', required=True,
                                      widget=forms.HiddenInput())

    def clean_horaPedir(self):
        horaPedir = self.cleaned_data['horaPedir']
        if horaPedir >= datetime.now().time():
            pass
        else:
            raise forms.ValidationError('Debe ingresar una hora igual posterior'
                                        'a la actual {0}'.format(
                datetime.now().time().strftime("%H:%M")))
        return horaPedir


class PedidoNotificacionForm(forms.ModelForm):
    #captcha = ReCaptchaField()

    def __init__(self, *args, **kwargs):
        super(PedidoNotificacionForm, self).__init__(*args, **kwargs)
        self.fields['email'].widget.attrs['disabled'] = True
        self.fields['celular'].widget.attrs['disabled'] = True

    def clean_hora_solicitud(self):
        hora_solicitud = self.cleaned_data['hora_solicitud']
        if hora_solicitud >= datetime.now().time():
            pass
        else:
            raise forms.ValidationError('Debe ingresar una hora igual posterior'
                                        'a la actual {0}'.format(
                datetime.now().time().strftime("%H:%M")))
        return hora_solicitud

    class Meta:
        model = Pedido
        fields = ['notificar_email', 'notificar_sms', 'email', 'celular',
                  'hora_solicitud', 'importe_abonado', 'observaciones']
        widgets = {
            'hora_solicitud': forms.TextInput(attrs={'class': 'form-control'}),
            "importe_abonado": forms.NumberInput(attrs={'class': 'form-control'}),
            'email': forms.TextInput(attrs={'class': 'form-control'}),
            'celular': forms.TextInput(attrs={'class': 'form-control'}),
            'observaciones': forms.Textarea(attrs={'class': 'form-control',
                                                   'rows': '5',
                                                   }),
        }
