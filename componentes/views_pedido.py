# -*- coding: utf-8 -*-


from django.contrib import messages
from componentes.form_pedido import (
    HoraPedidoForm, PedidoForm, PedidoNotificacionForm
)
from componentes.models import (
    DomicilioSearch, Pedido, DetallePedido, TipoProducto, Producto, Menu,
    Promocion, DetalleVersiones, ProductoParaArmar, SeccionProducto,
    VersionProducto, ProductoArmado, DetalleProductoArmado, IngredientesSeccion,
    HistorialEstadoPedido
)
from recursos_de_empresa.models import Sucursal
from datetime import date, datetime, timedelta
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.base import RedirectView
from django.views.generic.edit import (CreateView, UpdateView, DeleteView,
                                       BaseUpdateView)
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from libs.util.deco_utils import jsonResponse
from componentes.pedido_services import PedidoServices
from django.core.exceptions import SuspiciousOperation
from sigep.utiles import fecha_hora_actual


class PedidoCreateView(CreateView):
    model = Pedido
    form_class = PedidoForm
    template_name = 'pedido/1_pedido_informacion.html'

    def get_initial(self):
        initial = super(PedidoCreateView, self).get_initial()
        if self.request.user.is_authenticated():
            cliente = self.request.user.clienteprofile
            direccion = cliente.get_direccion_numero()
            if direccion:
                initial.update({'direccion': direccion})
            if cliente.piso:
                initial.update({'piso': str(cliente.piso)})
            if cliente.depto:
                initial.update({'depto': cliente.depto})
            if cliente.barrio:
                initial.update({'barrio': cliente.barrio})
        return initial

    def get_form(self, form_class):
        sucursales = Sucursal.objects.filter(ofrece_servicio_para_llevar=True)
        return form_class(sucursales=sucursales, **self.get_form_kwargs())

    def form_valid(self, form):
        self.object = form.save(commit=False)
        form.instance.fechaPedido = datetime.now()
        servicio = form.cleaned_data.get('servicio')
        self.object.servicio = servicio
        self.object.precio_envio = 0
        if self.object.servicio is Pedido.SERVICIO_DELIVERY:
            barrio = self.object.barrio
            sucursal = self.object.sucursal
            precio_envio = barrio.zonacobertura_set.filter(
                sucursal=sucursal).first().precio_envio
            self.object.precio_envio = precio_envio
        self.object = form.save()
        HistorialEstadoPedido.objects.create(pedido=self.object,
            estado=HistorialEstadoPedido.ESTADO_EN_DEFINICION,
                                             fecha=fecha_hora_actual())
        return super(PedidoCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse(
            'vista_arma_tu_pedido',
            kwargs={"pk_pedido": self.object.pk})


class PedidoUpdateView(UpdateView):
    model = Pedido
    form_class = PedidoForm
    template_name = 'pedido/1_pedido_informacion.html'

    def get_form(self, form_class):
        sucursales = Sucursal.objects.filter(ofrece_servicio_para_llevar=True)
        return form_class(sucursales=sucursales, **self.get_form_kwargs())

    def form_valid(self, form):
        self.object = form.save(commit=False)
        servicio = form.cleaned_data.get('servicio')
        self.object.servicio = servicio
        self.object.precio_envio = 0
        if self.object.servicio is Pedido.SERVICIO_DELIVERY:
            barrio = self.object.barrio
            sucursal = self.object.sucursal
            precio_envio = barrio.zonacobertura_set.filter(
                sucursal=sucursal).first().precio_envio
            self.object.precio_envio = precio_envio
        self.object = form.save()
        return super(PedidoUpdateView, self).form_valid(form)

    def get_success_url(self):
        return reverse(
            'vista_arma_tu_pedido',
            kwargs={"pk_pedido": self.object.pk})


class ArmaTuPedidoListView(ListView):
    """
    Esta vista lista los objetos pedido
    diferenciadas por sus estados actuales.
    Pasa un diccionario al template
    con las claves como estados.
    """

    template_name = 'pedido/2_arma_pedido.html'
    context_object_name = 'pedido'
    model = Pedido

    def get_context_data(self, **kwargs):
        context = super(ArmaTuPedidoListView, self).get_context_data(
           **kwargs)
        context['tipoProducto'] = TipoProducto.objects.all()
        context['pedido'] = Pedido.objects.get(pk=self.kwargs['pk_pedido'])
        context['range'] = range(11)
        context['productoParaArmar'] = ProductoParaArmar.objects.all()
        #context['domicilio'] = DomicilioSearch.objects.\
         #   obtener_domicilio_pedido(self.kwargs['pk_pedido'])
        if self.request.user.is_authenticated():
            cliente = self.request.user.clienteprofile
            pedidos = Pedido.objects.obtener_pedido_por_cliente(
                cliente).exclude(pk=self.kwargs['pk_pedido'])
            service = PedidoServices()
            pedidos = service.obtener_pedidos_por_estado(
                pedidos, HistorialEstadoPedido.ENTREGADO)[:3]
            context['pedido_historico'] = pedidos
        context['ver_pedido'] = True
        return context


class ProductoPedidoListView(ListView):
    """
    Esta vista lista los objetos pedido
    diferenciadas por sus estados actuales.
    Pasa un diccionario al template
    con las claves como estados.
    """

    template_name = 'pedido/3a_productos_solicitados.html'
    context_object_name = 'pedido'
    model = Pedido

    def get_context_data(self, **kwargs):
        context = super(ProductoPedidoListView, self).get_context_data(
           **kwargs)
        tipo_producto = TipoProducto.objects.get(
            pk=self.kwargs['pk_tipo_producto'])
        context['productos'] = Producto.objects.\
            obtener_productos_por_tipo_producto(self.kwargs['pk_tipo_producto'])
        context['pedido'] = Pedido.objects.get(pk=self.kwargs['pk_pedido'])
        context['range'] = range(11)
        # context['domicilio'] = DomicilioSearch.objects.\
        #     obtener_domicilio_pedido(self.kwargs['pk_pedido'])
        if self.request.user.is_authenticated():
            cliente = self.request.user.clienteprofile
            pedidos = Pedido.objects.obtener_pedido_por_cliente(
                cliente).exclude(pk=self.kwargs['pk_pedido'])
            service = PedidoServices()
            pedidos = service.obtener_pedidos_por_estado(
                pedidos, HistorialEstadoPedido.ENTREGADO)[:3]
            context['pedido_historico'] = pedidos
        context['ver_pedido'] = True
        return context


class MenuPedidoListView(ListView):
    """
    Esta vista lista los objetos pedido
    diferenciadas por sus estados actuales.
    Pasa un diccionario al template
    con las claves como estados.
    """

    template_name = 'pedido/3b_menu_disponibles.html'
    context_object_name = 'pedido'
    model = Pedido

    def get_context_data(self, **kwargs):
        context = super(MenuPedidoListView, self).get_context_data(
           **kwargs)
        context['productos'] = Menu.objects.all()
        context['pedido'] = Pedido.objects.get(pk=self.kwargs['pk_pedido'])
        context['range'] = range(11)
        # context['domicilio'] = DomicilioSearch.objects.\
        #     obtener_domicilio_pedido(self.kwargs['pk_pedido'])
        if self.request.user.is_authenticated():
            cliente = self.request.user.clienteprofile
            pedidos = Pedido.objects.obtener_pedido_por_cliente(
                cliente).exclude(pk=self.kwargs['pk_pedido'])
            service = PedidoServices()
            pedidos = service.obtener_pedidos_por_estado(
                pedidos, HistorialEstadoPedido.ENTREGADO)[:3]
            context['pedido_historico'] = pedidos
        context['ver_pedido'] = True
        return context


class PromocionPedidoListView(ListView):
    """
    Esta vista lista los objetos pedido
    diferenciadas por sus estados actuales.
    Pasa un diccionario al template
    con las claves como estados.
    """

    template_name = 'pedido/3c_promos_disponibles.html'
    context_object_name = 'pedido'
    model = Pedido

    def get_context_data(self, **kwargs):
        context = super(PromocionPedidoListView, self).get_context_data(
           **kwargs)
        context['productos'] = Promocion.objects.all()
        context['pedido'] = Pedido.objects.get(pk=self.kwargs['pk_pedido'])
        context['range'] = range(11)
        # context['domicilio'] = DomicilioSearch.objects.\
        #     obtener_domicilio_pedido(self.kwargs['pk_pedido'])
        if self.request.user.is_authenticated():
            cliente = self.request.user.clienteprofile
            pedidos = Pedido.objects.obtener_pedido_por_cliente(
                cliente).exclude(pk=self.kwargs['pk_pedido'])
            service = PedidoServices()
            pedidos = service.obtener_pedidos_por_estado(
                pedidos, HistorialEstadoPedido.ENTREGADO)[:3]
            context['pedido_historico'] = pedidos
        context['ver_pedido'] = True
        return context


def actualizar_tu_pedido_view(request, pk_pedido):
    pedido = Pedido.objects.get(pk=pk_pedido)
    ctx = {'pedido': pedido, 'range': range(11)}
    return render(request, 'pedido/sidebar_tu_pedido.html', ctx)


@jsonResponse
def agregar_producto_pedido_ajax_view(request):
    id_tip = request.GET['tipoProducto']
    id_pro = request.GET['codigoProducto']
    cantidad = request.GET['cantidad']
    pk_pedido = request.GET['pk_pedido']
    pedido = Pedido.objects.get(pk=pk_pedido)
    pedido.save()

    if int(id_tip) == 1:

        pro = DetalleVersiones.objects.get(pk=id_pro)
        try:
            detalle = pedido.detalles.get(producto=pro)
            detalle.cantidad += int(cantidad)
            detalle.save()
        except DetallePedido.DoesNotExist:
            precion = pro.precio
            d = DetallePedido(pedido=pedido, cantidad=cantidad, producto=pro,
                              precio=precion)
            d.save()
#         tipo = pro.producto.tipoProducto.codigo
#         return HttpResponseRedirect(
#             '/pedido/armaTuPedido/productosSolicitados/' + str(tipo))
        return {'status': 'ok'}
    elif int(id_tip) == 2:
        pro = Menu.objects.get(pk=id_pro)
        precion = pro.precio_venta
        d = DetallePedido(pedido=pedido, cantidad=cantidad, menu=pro,
                          precio=precion)
        d.save()
#         return HttpResponseRedirect('/pedido/armaTuPedido/menusDisponibles/')
        return {'status': 'ok'}
    elif int(id_tip) == 3:
        pro = Promocion.objects.get(pk=id_pro)
        precion = pro.precio
        d = DetallePedido(pedido=pedido, cantidad=cantidad, promocion=pro,
                          precio=precion)
        d.save()
#         return HttpResponseRedirect('/pedido/armaTuPedido/promosDisponibles/')
        return {'status': 'ok'}
    return {'status': 'fails'}


def actualizar_detalle_pedido_view(request, pk_pedido, nueva_cantidad, id_det):
    pedido = Pedido.objects.get(pk=pk_pedido)
    if pedido is not None:
        DetallePedido.objects.filter(pk=id_det).update(cantidad=nueva_cantidad)
    ctx = {'pedido': pedido, 'range': range(11)}
    return render_to_response('pedido/sidebar_tu_pedido.html', ctx,
                              context_instance=RequestContext(request))


def eliminar_detalle_view(request, pk_pedido, id_det):
    pedido = Pedido.objects.get(pk=pk_pedido)
    d = DetallePedido.objects.get(pk=id_det)
    if d.pedido == pedido:
        d.delete()
    ctx = {'pedido': pedido, 'range': range(11)}
    return render(request, 'pedido/sidebar_tu_pedido.html', ctx)


class DetallePedidoListView(ListView):
    """
    Esta vista lista los objetos pedido
    diferenciadas por sus estados actuales.
    Pasa un diccionario al template
    con las claves como estados.
    """

    template_name = 'pedido/4_detallePedido.html'
    context_object_name = 'pedido'
    model = Pedido

    def dispatch(self, *args, **kwargs):
        pedido = Pedido.objects.get(pk=self.kwargs['pk_pedido'])
        if not pedido.detalles.all():
            return HttpResponseRedirect(reverse('vista_arma_tu_pedido',
                                                kwargs={"pk_pedido":
                                                            self.kwargs[
                                                                'pk_pedido']}))
        else:
            return super(DetallePedidoListView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DetallePedidoListView, self).\
            get_context_data(**kwargs)
        context['pedido'] = Pedido.objects.get(pk=self.kwargs['pk_pedido'])
        context['range'] = range(11)
        if self.request.user.is_authenticated():
            cliente = self.request.user.clienteprofile
            pedidos = Pedido.objects.obtener_pedido_por_cliente(
                cliente).exclude(pk=self.kwargs['pk_pedido'])
            service = PedidoServices()
            pedidos = service.obtener_pedidos_por_estado(
                pedidos, HistorialEstadoPedido.ENTREGADO)[:3]
            context['pedido_historico'] = pedidos
        return context


def update_detalle_pedido_view(request, pk_pedido, nueva_cantidad, id_det):
    pedido = Pedido.objects.get(pk=pk_pedido)
    if pedido is not None:
        DetallePedido.objects.filter(pk=id_det).update(cantidad=nueva_cantidad)
    ctx = {'pedido': pedido, 'range': range(11)}
    return render_to_response('pedido/4a_update_detallePedido.html', ctx,
                              context_instance=RequestContext(request))


def detalle_pago_view(request, pk_pedido):
    horaPedido = datetime.now().time()
    pedido = Pedido.objects.get(pk=pk_pedido)

    # asigno al pedido el cliente que se encuentra logeado
    pedido.cliente = request.user.clienteprofile
    pedido.save()

    if request.method == "POST":
        form = HoraPedidoForm(request.POST, request.FILES)
        if form.is_valid():

            hora_pedido = form.cleaned_data['horaPedir']
            importe_abonado = form.cleaned_data['importe_abonar']
            # Fixed: agregar campo de hora pedido en pedido
            pedido.hora_solicitud = hora_pedido
            pedido.importe_abonado = float(importe_abonado)
            pedido.save()
            return HttpResponseRedirect('/pedido/pedido_notificacion/' +
                                        str(pedido.pk) + "/")

    else:
        form = HoraPedidoForm()
        pedido_service = PedidoServices()
        pedido_service.descontar_stock(pedido)

    horaActual = datetime.now().time()
    ctx = {'pedido': pedido, 'form': form, 'horaActual': horaActual}

    return render_to_response('pedido/5_detalle_pago.html', ctx,
                              context_instance=RequestContext(request))


class CancelaPedidoView(RedirectView):
    """
    Esta vista actualiza el pedido cancelandola.
    """

    pattern_name = 'home'

    # @@@@@@@@@@@@@@@@@@@@

    def get(self, request, *args, **kwargs):

        pedido = Pedido.objects.get(pk=self.kwargs['pk_pedido'])
        HistorialEstadoPedido.objects.create(pedido=pedido,
            estado=HistorialEstadoPedido.ESTADO_EN_DEFINICION,
            fecha=fecha_hora_actual(),
            observaciones="cancelado via web en momento de definicion")
        return HttpResponseRedirect('/')


class ProductoSolicitadosPedidoListView(ListView):
    """
    Esta vista lista los objetos pedido
    diferenciadas por sus estados actuales.
    Pasa un diccionario al template
    con las claves como estados.
    """

    template_name = 'pedido/3a_productos_solicitados.html'
    context_object_name = 'pedido'
    model = Pedido

    def get_context_data(self, **kwargs):
        context = super(ProductoSolicitadosPedidoListView, self).\
            get_context_data(**kwargs)
        pedido_service = PedidoServices()
        context['productos'] = pedido_service.obtener_productos_popularares()
        context['pedido'] = Pedido.objects.get(pk=self.kwargs['pk_pedido'])
        context['range'] = range(11)
        # context['domicilio'] = DomicilioSearch.objects.\
        #     obtener_domicilio_pedido(self.kwargs['pk_pedido'])
        if self.request.user.is_authenticated():
            cliente = self.request.user.clienteprofile
            pedidos = Pedido.objects.obtener_pedido_por_cliente(
                cliente).exclude(pk=self.kwargs['pk_pedido'])
            service = PedidoServices()
            pedidos = service.obtener_pedidos_por_estado(
                pedidos, HistorialEstadoPedido.ENTREGADO)[:3]
            context['pedido_historico'] = pedidos
        context['ver_pedido'] = True
        return context


def producto_para_armar_view(request, pk_producto, pk_pedido):

    pedido = Pedido.objects.get(pk=pk_pedido)
    producto_armar = ProductoParaArmar.objects.get(pk=pk_producto)
    pedido_historico = None
    if request.user.is_authenticated():
        cliente = request.user.clienteprofile
        pedidos = Pedido.objects.obtener_pedido_por_cliente(
            cliente).exclude(pk=pk_pedido)
        service = PedidoServices()
        pedidos = service.obtener_pedidos_por_estado(
            pedidos, HistorialEstadoPedido.ENTREGADO)[:3]
        pedido_historico = pedidos
    ctx = {
        'pedido': pedido, 'producto_armar': producto_armar, 'ver_pedido': True,
        'pedido_historico': pedido_historico
    }
    return render_to_response('pedido/3d_producto_para_armar.html', ctx,
                              context_instance=RequestContext(request))


def producto_armado_create_view(request, pk_pedido, pk_producto, pk_version):
    pedido = Pedido.objects.get(pk=pk_pedido)
    producto_armar = ProductoParaArmar.objects.get(pk=pk_producto)
    version = VersionProducto.objects.get(pk=pk_version)
    # FIXME: manejar try catch con posible errores
    producto_armado = ProductoArmado(
        producto=producto_armar, version=version,
        estado=ProductoArmado.PRODUCTO_DEFINICION)
    producto_armado.save()
    pedido.producto_armar = producto_armado
    pedido.save()
    ctx = {'pedido': pedido,  'range': range(11)}
    return render(request, 'pedido/sidebar_tu_pedido.html', ctx)


def producto_armado_ingrediente_view(request, pk_pedido, id_ing):
    pedido = Pedido.objects.get(pk=pk_pedido)
    ingrediente = IngredientesSeccion.objects.get(pk=id_ing)
    if pedido.producto_armar.validar_exclusion_ingrediente(ingrediente):
        detalle = DetalleProductoArmado(producto=pedido.producto_armar,
                                        ingrediente=ingrediente)
        detalle.save()
    ctx = {'pedido': pedido, 'range': range(11)}
    #return render(request, 'pedido/sidebar_tu_pedido.html', ctx)
    return render_to_response('pedido/sidebar_tu_pedido.html', ctx,
                              context_instance=RequestContext(request))


def tu_producto_view(request,  pk_pedido):
    pedido = Pedido.objects.get(pk=pk_pedido)
    producto_armado = ProductoArmado.objects.get(pk=pedido.producto_armar.pk)
    secciones = SeccionProducto.objects.filter(
        producto=producto_armado.producto).order_by('orden')
    ctx = {'pedido': pedido, 'secciones': secciones}
    return render_to_response('pedido/tu_producto.html', ctx,
                              context_instance=RequestContext(request))


def guardar_producto_view(request, pk_pedido):
    pedido = Pedido.objects.get(pk=pk_pedido)
    producto_armado = ProductoArmado.objects.get(pk=pedido.producto_armar.pk)
    precio = producto_armado.version.precio
    detalle_pedido = DetallePedido(pedido=pedido, cantidad=1,
                      producto_armado=producto_armado,
                      precio=precio)
    detalle_pedido.save()
    pedido.producto_armar = None
    pedido.save()
    return HttpResponseRedirect('/pedido/armaTuPedido/' + str(pedido.pk))


class PedidoHistoricoListView(DetailView):
    """
    Esta vista lista los objetos pedido
    historio con su detalles
    """

    template_name = 'pedido/3e_pedido_historico.html'
    context_object_name = 'pedido_historia'
    model = Pedido

    def get_context_data(self, **kwargs):
        context = super(PedidoHistoricoListView, self).get_context_data(
           **kwargs)
        context['pedido'] = Pedido.objects.get(pk=self.kwargs['pk_pedido'])
        context['range'] = range(11)
        if self.request.user.is_authenticated():
            cliente = self.request.user.clienteprofile
            pedidos = Pedido.objects.obtener_pedido_por_cliente(
                cliente).exclude(pk=self.kwargs['pk_pedido'])
            service = PedidoServices()
            pedidos = service.obtener_pedidos_por_estado(
                pedidos, HistorialEstadoPedido.ENTREGADO)[:3]
            context['pedido_historico'] = pedidos
        context['ver_pedido'] = True
        return context

    def get_object(self, *args, **kwargs):
        return Pedido.objects.get(pk=self.kwargs['pk_pedido_historico'])


class PedidoNotificacionView(UpdateView):
    model = Pedido
    form_class = PedidoNotificacionForm
    template_name = 'pedido/5_detalle_pago.html'

    def get_object(self, *args, **kwargs):
        return Pedido.objects.get(pk=self.kwargs['pk_pedido'])

    def get_initial(self):
        initial = super(PedidoNotificacionView, self).get_initial()
        cliente = self.request.user.clienteprofile
        if self.request.user.email:
            initial.update({'email': self.request.user.email})
        if cliente.telefono:
            initial.update({'celular': cliente.telefono})
        hora_actual = datetime.now().time()
        self.object = self.get_object()
        pedido_service = PedidoServices()
        tiempo_pedido = pedido_service.calcular_tiempo_pedido(self.object)
        dt = datetime.combine(date.today(), hora_actual) + \
             timedelta(minutes=tiempo_pedido)
        initial.update({'hora_solicitud': dt.time().strftime("%H:%M")})
        return initial

    def get(self, request, *args, **kwargs):
        hora_actual = datetime.now().time()
        self.object = self.get_object()
        pedido_service = PedidoServices()
        tiempo_pedido = pedido_service.calcular_tiempo_pedido(self.object)
        return self.render_to_response(self.get_context_data(pedido=self.object,
            total=tiempo_pedido, hora_actual=hora_actual))

    def form_valid(self, form):
        self.object = form.save(commit=False)
        pedido_service = PedidoServices()
        tiempo_pedido = pedido_service.calcular_tiempo_pedido(self.object)
        #dt = datetime.combine(date.today(), self.object.hora_solicitud) + \
         #    timedelta(minutes=tiempo_pedido)
        self.object.hora_entrega = self.object.hora_solicitud
        mensaje = "Tu pedido numero:{0} se entregara aproxidamente a las {1}hs"\
            "-Sigep".format(self.object.id, self.object.hora_entrega)
        self.object.cliente = self.request.user.clienteprofile
        self.object.save()
        pedido_service.descontar_stock(self.object)
        HistorialEstadoPedido.objects.create(pedido=self.object,
            estado=HistorialEstadoPedido.PENDIENTE, fecha=fecha_hora_actual())
        pedido_service.notificar_pedido(self.object, mensaje)
        return super(PedidoNotificacionView, self).form_valid(form)

    def form_invalid(self, form):
        hora_actual = datetime.now().time()
        self.object = self.get_object()
        pedido_service = PedidoServices()
        tiempo_pedido = pedido_service.calcular_tiempo_pedido(self.object)
        return self.render_to_response(self.get_context_data(pedido=self.object,
                                                             total=tiempo_pedido,
                                                             hora_actual=hora_actual))

    def get_success_url(self):
        self.request.session["pk_pedido"] = self.object.pk
        return reverse('home_page_view')


def actualizar_favorito_pedido_view(request, pk_pedido, favorito):
    pedido = Pedido.objects.get(pk=pk_pedido)
    if int(favorito) == 0:
        pedido.favorito = True
        pedido.save()
    elif int(favorito) == 1:
        pedido.favorito = False
        pedido.save()
    ctx = {'pedido_his': pedido}
    return render_to_response('pedido/sidebar_historico_pedido.html', ctx,
                              context_instance=RequestContext(request))


class RepetirPedidoView(RedirectView):
    """
    Esta vista repite el pedido
    """

    pattern_name = 'repetir_pedido'

    def get(self, request, *args, **kwargs):

        pedido = Pedido.objects.get(pk=self.kwargs['pk_pedido'])
        service = PedidoServices()
        pedido_nuevo = service.repetir_pedido(pedido)
        HistorialEstadoPedido.objects.create(
            pedido=pedido_nuevo,
            estado=HistorialEstadoPedido.ESTADO_EN_DEFINICION,
            fecha=fecha_hora_actual())
        return HttpResponseRedirect(
            reverse(
            'vista_arma_tu_pedido',
            kwargs={"pk_pedido": pedido_nuevo.pk}))
