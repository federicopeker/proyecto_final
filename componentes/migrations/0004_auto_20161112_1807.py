# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-11-12 21:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('componentes', '0003_auto_20161022_1647'),
    ]

    operations = [
        migrations.AddField(
            model_name='pedido',
            name='celular',
            field=models.CharField(blank=True, max_length=32, null=True),
        ),
        migrations.AddField(
            model_name='pedido',
            name='email',
            field=models.EmailField(blank=True, max_length=254, null=True),
        ),
        migrations.AddField(
            model_name='pedido',
            name='notificar_email',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='pedido',
            name='notificar_sms',
            field=models.BooleanField(default=False),
        ),
    ]
