# -*- coding: utf-8 -*-


from __future__ import unicode_literals

import datetime
import hashlib

from django.db import models
from django.core.exceptions import ValidationError, SuspiciousOperation
from smart_selects.db_fields import ChainedForeignKey

from recursos_de_empresa.models import Sucursal
from ubicaciones.models import Localidad, Barrio
from django.db.models import Count, Sum, Q
from django.contrib.auth.models import User
from allauth.account.signals import user_signed_up
from django.dispatch import receiver

from sigep.utiles import fecha_hora_actual


class TipoIngrediente(models.Model):
    """Clase Tipo Ingrediente
    Atributos: IdTipoIngrediente, NombreIngrediente """
    nombre = models.CharField(max_length=50, verbose_name='Nombre')

    def __unicode__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Tipos de Ingredientes'


class Clasificacion(models.Model):
    """Clase clasificacion:
    Atributos: Nombre, Descripcion """
    nombre = models.CharField(max_length=50)
    descripcion = models.CharField(max_length=50)

    def __unicode__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Clasificaciones'


class Version(models.Model):
    """Clase Version
    Atributos: Clasificacion, nombre. """
    nombre = models.CharField(max_length=50)
    clasificacion = models.ManyToManyField(Clasificacion)

    def __unicode__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Versión'
        verbose_name_plural = 'Versiones'


class UnidadDeMedida(models.Model):
    """Clase Unidad de medida:
    Atributos:  Descripcion. """
    nombre = models.CharField(max_length=50)

    def __unicode__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Unidades de medida'


class TipoProducto(models.Model):
    """Clase TipoProducto
    Atributos: CódigoTipoProducto, Nombre,Imagen """
    nombre = models.CharField(max_length=50)
    imagen = models.ImageField(upload_to='imagenes/ComponentesDePedido/TipoProductos',
                               verbose_name='Vista Previa')

    def __unicode__(self):
        return self.nombre

    def vista_previa(self):
        return '<a href="/media/%s"><img src="/media/%s" width=50px heigth=50px/></a>' % (
        self.imagen, self.imagen)

    vista_previa.allow_tags = True

    class Meta:
        verbose_name_plural = 'Tipos de Productos'


class Ingrediente(models.Model):
    """
    Clase Ingriente
    """
    nombre = models.CharField(max_length=50)
    tipo_ingrediente = models.ForeignKey(TipoIngrediente,
                                         verbose_name='Tipo de ingrediente')
    unidad_de_medida = models.ForeignKey(UnidadDeMedida,
                                         verbose_name='Unidad de Medida')
    imagen = models.ImageField(upload_to='imagenes/Componentes/Ingredientes',
                               verbose_name='Vista Previa')
    stock_actual = models.IntegerField(verbose_name='Stock Actual')
    stock_minimo = models.IntegerField(verbose_name='Stock Mínimo')
    stock_corte = models.IntegerField(verbose_name='Stock Corte')
    precio = models.DecimalField(max_digits=5, decimal_places=2,
                                 verbose_name='Precio($)')

    def __unicode__(self):
        return self.nombre


class ProductoManager(models.Manager):
    def obtener_productos_por_tipo_producto(self, tipo_producto):
        try:
            return self.filter(
                tipo_producto=tipo_producto)
        except Producto.DoesNotExist:
            raise (SuspiciousOperation("No se encontro productos con"
                                       "ese tipo de producto"))


class Producto(models.Model):
    """
    Clase Producto
    """

    objects_default = models.Manager()
    # Por defecto django utiliza el primer manager instanciado. Se aplica al
    # admin de django, y no aplica las customizaciones del resto de los
    # managers que se creen.

    objects = ProductoManager()

    nombre = models.CharField(max_length=50)
    tipo_producto = models.ForeignKey(TipoProducto,
                                      verbose_name='Tipo de Producto',
                                      related_name="productos")
    version = models.ForeignKey(Version)
    estado = models.BooleanField(default=True)
    descripcion = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return self.nombre

    def get_detalle_versiones(self):
        return DetalleVersiones.objects.filter(producto=self)

    def get_ingredientes(self):
        return DetalleIngredientes.objects.filter(producto=self)


class DetalleIngredientes(models.Model):
    """
    Clase DetalleIngrediente
    """
    tipo_ingrediente = models.ForeignKey(TipoIngrediente,
                                         verbose_name='Tipo de Ingrediente')
    ingrediente = ChainedForeignKey(
        Ingrediente,
        chained_field='tipo_ingrediente',
        chained_model_field='tipo_ingrediente',
        show_all=False,
        auto_choose=True)
    producto = models.ForeignKey(Producto, related_name="ingredientes")
    cantidad = models.IntegerField()

    def __unicode__(self):
        return u'%s- %s' % (self.producto.nombre, self.ingrediente.nombre)

    class Meta:
        verbose_name_plural = 'Detalle de ingredientes'
        verbose_name = 'ingrediente'


class DetalleVersiones(models.Model):
    """Clase DetalleVersiones
    Atributos: Clasificacion, Imagen de producto, Precio """
    clasificacion = models.ForeignKey(Clasificacion,
                                      related_name='%(app_label)s_%(class)s_related')
    imagen_producto = models.ImageField(upload_to='imagenes/Componentes/Productos',
                                        verbose_name='Imágen Producto')
    precio = models.DecimalField(max_digits=5, decimal_places=2,
                                 verbose_name='Precio($)')
    producto = models.ForeignKey(Producto,
                                 related_name="detalle_version_producto")

    def __unicode__(self):
        return u'%s- %s' % (self.producto.nombre, self.clasificacion.nombre)

    class Meta:
        verbose_name_plural = "Versiones de producto"
        verbose_name = "version"


class Menu(models.Model):
    """
    Clase Menu
    """

    def vista_previa(self):
        return '<a href="/media/%s"><img src="/media/%s" width=50px heigth=50px/></a>' % (
        self.imagen, self.imagen)

    vista_previa.allow_tags = True
    nombre = models.CharField(max_length=50, verbose_name='Nombre Menú')
    precio_venta = models.DecimalField(max_digits=5, decimal_places=2,
                                       verbose_name='Precio($)')
    imagen = models.ImageField(upload_to='imagenes/Componentes/Menus',
                               verbose_name='Vista Previa')

    def __unicode__(self):
        return self.nombre

    def get_detalle_menu(self):
        return DetalleMenu.objects.filter(menu=self)

    class Meta:
        verbose_name = 'Menú'
        verbose_name_plural = 'Menús'


class DetalleMenu(models.Model):
    """
    Clase DetalleMenu
    """
    menu = models.ForeignKey(Menu)
    tipo_producto = models.ForeignKey(TipoProducto)
    producto = ChainedForeignKey(
        Producto,
        chained_field='tipo_producto',
        chained_model_field='tipo_producto',
        show_all=False,
        auto_choose=True
    )
    version_producto = models.ForeignKey(Clasificacion,
                                         verbose_name='Versiones de Producto ')

    cantidad = models.IntegerField()

    def __unicode__(self):
        return u'%s, %s' % (self.menu.nombre, self.producto.nombre)

    class Meta:
        verbose_name = 'Detalle de Menú'
        verbose_name_plural = 'Detalles de Menú'


class PromocionManager(models.Manager):
    def obtener_en_definicion_para_editar(self, promocion_id):
        """Devuelve la promocion pasada por ID, siempre que dicha
        promocion pueda ser editar (editada en el proceso de
        definirla, o sea, en el proceso de "creacion" de la
        promocion).

        En caso de no encontarse, lanza Suspicious
        Operation
        """
        try:
            return self.filter(
                estado=self.model.ESTADO_EN_DEFINICION).get(
                pk=promocion_id)
        except self.model.DoesNotExist:
            raise (SuspiciousOperation("No se encontro promocion %s en "
                                       "estado ESTADO_EN_DEFINICION"))


class Promocion(models.Model):
    """
    Clase Promocion
    """

    objects_default = models.Manager()
    # Por defecto django utiliza el primer manager instanciado. Se aplica al
    # admin de django, y no aplica las customizaciones del resto de los
    # managers que se creen.

    objects = PromocionManager()

    ESTADO_EN_DEFINICION = 1
    """La promocion esta siendo definida en el wizard"""

    ESTADO_ACTIVA = 2
    """La promocion es activa """

    ESTADO_FINALIZADA = 3
    """La promocion esta finalziada """

    ESTADOS = (
        (ESTADO_EN_DEFINICION, '(en definicion)'),
        (ESTADO_ACTIVA, 'Activa'),
        (ESTADO_FINALIZADA, 'Finalizada')
    )

    estado = models.PositiveIntegerField(
        choices=ESTADOS,
        default=ESTADO_EN_DEFINICION,
    )
    nombre = models.CharField(max_length=50)
    descripcion = models.TextField(verbose_name='Descripción')
    imagen = models.ImageField(upload_to='imagenes/componentes/promocion',
                               verbose_name='Imágen Promocion',
                               blank=True, null=True)
    precio = models.DecimalField(blank=True, null=True,
                                 max_digits=5, decimal_places=2,
                                 verbose_name='Precio($)')
    stock = models.IntegerField(blank=True, null=True)
    fecha_inicio = models.DateField()
    fecha_fin = models.DateField()
    descuento = models.IntegerField(blank=True, null=True,
                                    verbose_name='Descuento(%)')

    def __unicode__(self):
        return self.nombre

    def vista_previa(self):
        return '<a href="/media/%s"><img src="/media/%s" width=50px' \
               'heigth=50px/></a>' % (self.imagen, self.imagen)

    vista_previa.allow_tags = True

    def obtener_actuaciones_validas(self):
        """
        Este método devuelve un lista con las actuaciones válidas de una
        promocion. Teniendo como válidas aquellas que se van a ser procesadas
        teniendo en cuenta las fechas y horas que se le setearon.

        En caso de que las fecha_iniio y fecha_fin sean nulas, como ser en un
        template, devuelve una lista vacia.
        """
        hoy_ahora = datetime.datetime.today()
        hoy = hoy_ahora.date()
        ahora = hoy_ahora.time()

        lista_actuaciones = [actuacion.dia_semanal for actuacion in
                             self.actuaciones.all()]
        lista_actuaciones_validas = []

        dias_totales = (self.fecha_fin - self.fecha_inicio).days + 1
        for numero_dia in range(dias_totales):
            dia_actual = (self.fecha_inicio + datetime.timedelta(
                days=numero_dia))
            dia_semanal_actual = dia_actual.weekday()

            if dia_semanal_actual in lista_actuaciones:
                actuaciones_diaria = self.actuaciones.filter(
                    dia_semanal=dia_semanal_actual)
                for actuacion in actuaciones_diaria:
                    actuacion_valida = True
                    if dia_actual == hoy and ahora > actuacion.hora_hasta:
                        actuacion_valida = False

                    if actuacion_valida:
                        lista_actuaciones_validas.append(actuacion)
        return lista_actuaciones_validas

    def obtener_actuacion_actual(self):
        """
        Este método devuelve la actuación correspondiente al
        momento de hacer la llamada al método.
        Si no hay ninguna devuelve None.
        """
        hoy_ahora = datetime.datetime.today()
        assert hoy_ahora.tzinfo is None
        dia_semanal = hoy_ahora.weekday()
        hora_actual = hoy_ahora.time()

        # FIXME: PERFORMANCE: ver si el resultado de 'filter()' se cachea,
        # sino, usar algo que se cachee, ya que este metodo es ejecutado
        # muchas veces desde el daemon
        actuaciones_hoy = self.actuaciones.filter(dia_semanal=dia_semanal)
        if not actuaciones_hoy:
            return None

        for actuacion in actuaciones_hoy:
            if actuacion.hora_desde <= hora_actual <= actuacion.hora_hasta:
                return actuacion
        return None

    def verifica_fecha(self, hoy_ahora):
        """
        Este método se encarga de verificar si la fecha pasada como
        parametro en hoy_ahora es válida para la campaña actual.
        Devuelve True o False.
        """
        assert isinstance(hoy_ahora, datetime.datetime)

        fecha_actual = hoy_ahora.date()

        if self.fecha_inicio <= fecha_actual <= self.fecha_fin:
            return True
        return False

    def valida_actuaciones(self):
        """
        Este método verifica que la actuaciones de una promocion, sean válidas.
        Corrobora que al menos uno de los días semanales del rango de fechas
        de la promocion concuerde con algún dia de la actuación que tenga la
        promocion al momento de ser consultado este método.
        Si la promocion tiene un solo día de ejecución o si tiene un solo día
        de actuación, y los días semanales coinciden y es hoy, valida que
        hora actual sea menor que la hora_hasta de la actuación.
        """
        valida = False
        hoy_ahora = datetime.datetime.today()
        hoy = hoy_ahora.date()
        ahora = hoy_ahora.time()

        fecha_inicio = self.fecha_inicio
        fecha_fin = self.fecha_fin

        lista_actuaciones = [actuacion.dia_semanal for actuacion in
                             self.actuaciones.all()]

        dias_totales = (self.fecha_fin - self.fecha_inicio).days + 1
        for numero_dia in range(dias_totales):
            dia_actual = (self.fecha_inicio + datetime.timedelta(
                days=numero_dia))
            dia_semanal_actual = dia_actual.weekday()

            if dia_semanal_actual in lista_actuaciones:
                valida = True
                actuaciones_diaria = self.actuaciones.filter(
                    dia_semanal=dia_semanal_actual)
                for actuacion in actuaciones_diaria:
                    if dia_actual == hoy and ahora > actuacion.hora_hasta:
                        valida = False
        return valida

    def actuaciones_promocion(self):
        """

        :return: las actuaciones de la promocion
        """
        map_int_dia_semana = {
            0: "Lunes",
            1: "Martes",
            2: "Miércoles",
            3: "Jueves",
            4: "Viernes",
            5: "Sabado",
            6: "Domingo"
        }
        actuaciones = "no tiene programación"
        if self.actuaciones.all():
            actuaciones = ""
            for actuacion in self.actuaciones.all():
                actuaciones += map_int_dia_semana[actuacion.dia_semanal] \
                               + " desde " + str(actuacion.hora_desde) + "hs hasta" + \
                               str(actuacion.hora_hasta) + "hs,"
        return actuaciones

    def get_detalle_promocion(self):
        return DetallePromocion.objects.filter(promocion=self)

    def get_cantidad_total_productos(self):
        cantidad = DetallePromocion.objects.filter(promocion=self). \
            aggregate(Sum('cantidad'))
        return cantidad['cantidad__sum']

    def get_precio_total(self):
        total = 0
        for detalle in self.get_detalle_promocion():
            if detalle.menu:
                total += detalle.precio_total_menu()
            elif detalle.version_producto:
                total += detalle.precio_total_producto()
        return total

    class Meta:
        verbose_name = 'Promoción'
        verbose_name_plural = 'Promociones'


class Actuacion(models.Model):
    """
    Representa los días de la semana y los
    horarios en que una promocion se ejecuta.
    """

    """Dias de la semana, compatibles con datetime.date.weekday()"""
    LUNES = 0
    MARTES = 1
    MIERCOLES = 2
    JUEVES = 3
    VIERNES = 4
    SABADO = 5
    DOMINGO = 6

    DIA_SEMANAL_CHOICES = (
        (LUNES, 'LUNES'),
        (MARTES, 'MARTES'),
        (MIERCOLES, 'MIERCOLES'),
        (JUEVES, 'JUEVES'),
        (VIERNES, 'VIERNES'),
        (SABADO, 'SABADO'),
        (DOMINGO, 'DOMINGO'),
    )
    dia_semanal = models.PositiveIntegerField(
        choices=DIA_SEMANAL_CHOICES,
    )

    hora_desde = models.TimeField()
    hora_hasta = models.TimeField()
    promocion = models.ForeignKey(Promocion, related_name='actuaciones')

    def __unicode__(self):
        return "Promocion {0} - Actuación: {1}".format(
            self.promocion,
            self.dia_semanal,
        )

    def verifica_actuacion(self, hoy_ahora):
        """
        Este método verifica que el día de la semana y la hora
        pasada en el parámetro hoy_ahora sea válida para la
        actuación actual.
        Devuelve True o False.
        """

        assert isinstance(hoy_ahora, datetime.datetime)

        dia_semanal = hoy_ahora.weekday()
        hora_actual = datetime.time(
            hoy_ahora.hour, hoy_ahora.minute, hoy_ahora.second)

        if not self.dia_semanal == dia_semanal:
            return False

        if not self.hora_desde <= hora_actual <= self.hora_hasta:
            return False

        return True

    def dia_concuerda(self, fecha_a_chequear):
        """Este método evalua si el dia de la actuacion actual `self`
        concuerda con el dia de la semana de la fecha pasada por parametro.

        :param fecha_a_chequear: fecha a chequear
        :type fecha_a_chequear: `datetime.date`
        :returns: bool - True si la actuacion es para el mismo dia de
                  la semana que el dia de la semana de `fecha_a_chequear`
        """
        # NO quiero que funcione con `datatime` ni ninguna otra
        #  subclase, más que específicamente `datetime.date`,
        #  por eso no uso `isinstance()`.
        assert type(fecha_a_chequear) == datetime.date

        return self.dia_semanal == fecha_a_chequear.weekday()

    def es_anterior_a(self, time_a_chequear):
        """Este método evalua si el rango de tiempo de la actuacion
        actual `self` es anterior a la hora pasada por parametro.
        Verifica que sea 'estrictamente' anterior, o sea, ambas horas
        de la Actuacion deben ser anteriores a la hora a chequear
        para que devuelva True.

        :param time_a_chequear: hora a chequear
        :type time_a_chequear: `datetime.time`
        :returns: bool - True si ambas horas de la actuacion son anteriores
                  a la hora pasada por parametro `time_a_chequear`.
        """
        # NO quiero que funcione con ninguna subclase, más que
        #  específicamente `datetime.time`, por eso no uso `isinstance()`.
        assert type(time_a_chequear) == datetime.time

        # Es algo redundante chequear ambos, pero bueno...
        return self.hora_desde < time_a_chequear and \
               self.hora_hasta < time_a_chequear

    def clean(self):
        """
        Valida que al crear una actuación a una promocion
        no exista ya una actuación en el rango horario
        especificado y en el día semanal seleccionado.
        """
        if self.hora_desde and self.hora_hasta:
            if self.hora_desde >= self.hora_hasta:
                raise ValidationError({
                    'hora_desde': ["La hora desde debe ser\
                        menor o igual a la hora hasta."],
                    'hora_hasta': ["La hora hasta debe ser\
                        mayor a la hora desde."],
                })

            conflicto = self.promocion.actuaciones.filter(
                dia_semanal=self.dia_semanal,
                hora_desde__lte=self.hora_hasta,
                hora_hasta__gte=self.hora_desde,
            )
            if any(conflicto):
                raise ValidationError({
                    'hora_desde': ["Ya esta cubierto el rango horario\
                        en ese día semanal."],
                    'hora_hasta': ["Ya esta cubierto el rango horario\
                        en ese día semanal."],
                })

    def obtener_actuaciones_validas(self):
        """
        Este método devuelve un lista con las actuaciones válidas de una
        campaña. Teniendo como válidas aquellas que se van a ser procesadas
        teniendo en cuenta las fechas y horas que se le setearon.

        En caso de que las fecha_iniio y fecha_fin sean nulas, como ser en un
        template, devuelve una lista vacia.
        """
        hoy_ahora = datetime.datetime.today()
        hoy = hoy_ahora.date()
        ahora = hoy_ahora.time()

        lista_actuaciones = [actuacion.dia_semanal for actuacion in
                             self.actuaciones.all()]
        lista_actuaciones_validas = []

        dias_totales = (self.fecha_fin - self.fecha_inicio).days + 1
        for numero_dia in range(dias_totales):
            dia_actual = (self.fecha_inicio + datetime.timedelta(
                days=numero_dia))
            dia_semanal_actual = dia_actual.weekday()

            if dia_semanal_actual in lista_actuaciones:
                actuaciones_diaria = self.actuaciones.filter(
                    dia_semanal=dia_semanal_actual)
                for actuacion in actuaciones_diaria:
                    actuacion_valida = True
                    if dia_actual == hoy and ahora > actuacion.hora_hasta:
                        actuacion_valida = False

                    if actuacion_valida:
                        lista_actuaciones_validas.append(actuacion)
        return lista_actuaciones_validas

    class Meta:
        verbose_name = "Programación"
        verbose_name_plural = "Programaciones"


class DetallePromocion(models.Model):
    """
    Clase DetallePromocion
    """
    PRODUCTO = 1
    """selecciona para agregar un producto"""

    MENU = 2
    """seleccion para agregar un menú."""

    DETALLE_CHOICES = (
        (PRODUCTO, 'Producto'),
        (MENU, 'Menú'),
    )
    tipo_detalle = models.PositiveIntegerField(
        choices=DETALLE_CHOICES, verbose_name='Selecccione un producto ó menú'
    )

    promocion = models.ForeignKey(Promocion, related_name='detalles')
    tipo_producto = models.ForeignKey(TipoProducto,
                                      verbose_name='Tipo de Producto',
                                      null=True, blank=True)
    producto = ChainedForeignKey(
        Producto,
        chained_field='tipo_producto',
        chained_model_field='tipo_producto',
        show_all=False,
        auto_choose=True,
        null=True, blank=True
    )
    version_producto = ChainedForeignKey(DetalleVersiones,
                                         chained_field='producto',
                                         chained_model_field='producto',
                                         show_all=False,
                                         auto_choose=True,
                                         verbose_name='Versiones de Producto',
                                         null=True, blank=True)
    menu = models.ForeignKey(Menu, null=True, blank=True)
    cantidad = models.IntegerField()

    def __unicode__(self):
        return "Promocion {0} - cantidad producto: {1}".format(
            self.promocion,
            self.cantidad,
        )

    def precio_total_producto(self):
        return self.version_producto.precio * self.cantidad

    def precio_total_menu(self):
        return self.menu.precio_venta * self.cantidad


class ClienteProfileManager(models.Manager):
    def obtener_cliente_profile_editar(self, user):
        """Devuelve el cliente_profile pasado por ID, siempre que dicha
        pedido pueda ser editar

        En caso de no encontarse, lanza SuspiciousOperation
        """
        try:
            return self.get(user=user)
        except ClienteProfile.DoesNotExist:
            return None


class ClienteProfile(models.Model):
    objects_default = models.Manager()
    # Por defecto django utiliza el primer manager instanciado. Se aplica al
    # admin de django, y no aplica las customizaciones del resto de los
    # managers que se creen.

    objects = ClienteProfileManager()

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar_url = models.CharField(max_length=256, blank=True, null=True)
    telefono = models.CharField(max_length=100, blank=True, null=True)
    direccion = models.CharField(max_length=250, verbose_name='Dirección',
                                 blank=True, null=True)
    numero_direccion = models.IntegerField(verbose_name='Número', blank=True,
                                           null=True)
    piso = models.IntegerField(blank=True, null=True)
    depto = models.CharField(max_length=50, blank=True, null=True)
    codigo_postal = models.CharField(max_length=100,
                                     verbose_name="Código Postal",
                                     blank=True, null=True)
    localidad = models.ForeignKey(Localidad, blank=True, null=True)
    barrio = ChainedForeignKey(Barrio, chained_field="localidad",
                               chained_model_field="localidad",
                               auto_choose=True, blank=True, null=True)

    def get_direccion_numero(self):
        direccion = None
        if self.direccion and self.numero_direccion:
            direccion = self.direccion + " " + str(self.numero_direccion)
        return direccion

    def get_count_entregados(self):
        estado_entregado = HistorialEstadoPedido.ENTREGADO
        includes = [pedido.id for pedido in self.pedidos.all()
                    if pedido.get_historico_estado_actual().estado ==
                    estado_entregado]
        return self.pedidos.filter(id__in=includes).count()

    def get_count_cancelados(self):
        estado_cancelado = HistorialEstadoPedido.CANCELADO
        includes = [pedido.id for pedido in self.pedidos.all()
                    if pedido.get_historico_estado_actual().estado ==
                    estado_cancelado]
        return self.pedidos.filter(id__in=includes).count()

    def get_red_social(self):
        if self.user.socialaccount_set.all().count() > 0:
            return self.user.socialaccount_set.all().first().provider
        return "web"

    def __unicode__(self):
        return self.user.get_full_name()

    class Meta:
        verbose_name_plural = "Clientes"
        verbose_name = "cliente"


@receiver(user_signed_up)
def set_initial_user_names(request, user, sociallogin=None, **kwargs):
    """
    When a social account is created successfully and this signal is received,
    django-allauth passes in the sociallogin param, giving access to metadata on the remote account, e.g.:

    sociallogin.account.provider  # e.g. 'twitter'
    sociallogin.account.get_avatar_url()
    sociallogin.account.get_profile_url()
    sociallogin.account.extra_data['screen_name']

    See the socialaccount_socialaccount table for more in the 'extra_data' field.

    From http://birdhouse.org/blog/2013/12/03/django-allauth-retrieve-firstlast-names-from-fb-twitter-google/comment-page-1/
    """
    preferred_avatar_size_pixels = 256

    picture_url = "http://www.gravatar.com/avatar/{0}?s={1}".format(
        hashlib.md5(user.email.encode('UTF-8')).hexdigest(),
        preferred_avatar_size_pixels
    )

    if sociallogin:
        # Extract first / last names from social nets and store on User record

        if sociallogin.account.provider == 'facebook':
            user.username = user.email
            user.first_name = sociallogin.account.extra_data['first_name']
            user.last_name = sociallogin.account.extra_data['last_name']
            # verified = sociallogin.account.extra_data['verified']
            picture_url = "http://graph.facebook.com/{0}/picture?width={1}&height={1}".format(
                sociallogin.account.uid, preferred_avatar_size_pixels)

        if sociallogin.account.provider == 'google':
            user.username = user.email
            user.first_name = sociallogin.account.extra_data['given_name']
            user.last_name = sociallogin.account.extra_data['family_name']
            # verified = sociallogin.account.extra_data['verified_email']
            picture_url = sociallogin.account.extra_data['picture']

    profile = ClienteProfile(user=user, avatar_url=picture_url)
    profile.save()

    # user.guess_display_name()
    user.save()


class ProductoParaArmarManager(models.Manager):
    def obtener_en_definicion_para_editar(self, producto_id):
        """Devuelve el prodcuto pasada por ID, siempre que dicha
        producto pueda ser editar (editada en el proceso de
        definirla, o sea, en el proceso de "creacion" de la
        producto).

        En caso de no encontarse, lanza Suspicious
        Operation
        """
        try:
            return self.filter(
                estado=self.model.ESTADO_EN_DEFINICION).get(
                pk=producto_id)
        except self.model.DoesNotExist:
            raise (SuspiciousOperation("No se encontro productoparamar %s en "
                                       "estado ESTADO_EN_DEFINICION"))


class ProductoParaArmar(models.Model):
    """Clase Producto para Armar""
    Atributos: tipo producto, slogan y version"""

    objects_default = models.Manager()
    # Por defecto django utiliza el primer manager instanciado. Se aplica al
    # admin de django, y no aplica las customizaciones del resto de los
    # managers que se creen.

    objects = ProductoParaArmarManager()

    ESTADO_EN_DEFINICION = 1
    """El producto para armado esta siendo definido en el wizard"""

    ESTADO_ACTIVA = 2
    """El producto para armadoes activa """

    ESTADO_FINALIZADA = 3
    """El producto para armado"""

    ESTADOS = (
        (ESTADO_EN_DEFINICION, '(en definicion)'),
        (ESTADO_ACTIVA, 'Activa'),
        (ESTADO_FINALIZADA, 'Finalizada')
    )

    estado = models.PositiveIntegerField(
        choices=ESTADOS,
        default=ESTADO_EN_DEFINICION,
    )
    tipo_producto = models.ForeignKey(TipoProducto,
                                      verbose_name="Tipo de Producto")
    slogan = models.CharField(max_length=50, verbose_name="Slogan Producto")
    version = models.ForeignKey(Version)

    def __unicode__(self):
        return self.slogan

    class Meta:
        verbose_name_plural = "Productos para armar"

    def get_detalle_versiones(self):
        return VersionProducto.objects.filter(producto=self)

    def get_secciones(self):
        return SeccionProducto.objects.filter(producto=self)

    def valida_versiones(self, clasificacion):
        valida = False
        lista_versiones = [version.clasificacion for version in
                           self.versiones.all()]
        resultado = self.versiones.filter(clasificacion=clasificacion)
        if resultado:
            valida = True
        return valida


class VersionProducto(models.Model):
    """Clase DetalleVersiones
    Atributos: Clasificacion, Imagen de producto, Precio """

    def vista_previa(self):
        return '<a href="/media/%s"><img src="/media/%s" width=50px heigth=50px/></a>' % (
            self.imagen, self.imagen)

    vista_previa.allow_tags = True
    clasificacion = models.ForeignKey(Clasificacion,
                                      related_name="%(app_label)s_%(class)s_related")
    imagen_producto = models.ImageField(upload_to='imagenes/DetalleVersiones',
                                        verbose_name='Imágen')
    precio = models.DecimalField(max_digits=5, decimal_places=2,
                                 verbose_name="Precio por clasificación($)")
    producto = models.ForeignKey(ProductoParaArmar, related_name="versiones")

    def __unicode__(self):
        return u'%s- %s' % (self.producto.slogan, self.clasificacion.nombre)

    class Meta:
        verbose_name_plural = "Detalle de versiones"


class SeccionProductoManager(models.Manager):
    def obtener_siguiente_orden(self, producto_id):
        try:
            seccion_producto = self.filter(producto=producto_id).latest('orden')
            return seccion_producto.orden + 1
        except SeccionProducto.DoesNotExist:
            return 1


class SeccionProducto(models.Model):
    """ Clase Seccion por producto
    Atributos: producto, nombre, orden, tipo ingrediente, descripcion,
    obligatoria, excluyente, cantidad de exclusiones"""

    objects = SeccionProductoManager()

    producto = models.ForeignKey(ProductoParaArmar, related_name="secciones")
    nombre = models.CharField(max_length=100, verbose_name="Sección Producto")
    orden = models.IntegerField(verbose_name="Orden Presentación")
    tipo_ingrediente = models.ForeignKey(TipoIngrediente,
                                         verbose_name="Tipo de Ingrediente")
    descripcion = models.TextField(verbose_name='Descripción')
    obligatoria = models.BooleanField(verbose_name="Sección Obligatoria")
    excluyente = models.BooleanField(verbose_name="Clasificaciones Excluyentes")
    cantidad_exclusiones = models.IntegerField(
        verbose_name="Cantidad Exclusiones", blank=True, null=True)

    def __unicode__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Secciones del Producto"

    def get_ingrediente_seccion(self):
        return IngredientesSeccion.objects.filter(seccion=self)

    def get_excluyente_display(self):
        if self.excluyente:
            return "Excluyente"
        else:
            return "No Excluyente"


class IngredientesSeccion(models.Model):
    """ Clase Ingredientes por seccion
    Atributos: producto, ingredientes"""
    seccion = models.ForeignKey(SeccionProducto, related_name="ingredientes")
    ingrediente = models.ForeignKey(Ingrediente)

    def __unicode__(self):
        return u'%s - %s - %s' % (
            self.seccion.producto.slogan, self.seccion.nombre,
            self.ingrediente.nombre)

    class Meta:
        verbose_name_plural = "Ingredientes por Seccion"


class ProductoArmado(models.Model):
    """ Producto Armado
    Atributos: producto, versiones"""

    PRODUCTO_DEFINICION = 1
    """Producto armandose"""

    PRODUCTO_ARMADO = 2
    """ Producto armado"""

    ESTADO_CHOICES = (
        (PRODUCTO_DEFINICION, 'Producto en definicion'),
        (PRODUCTO_ARMADO, 'Producto armado'),
    )
    estado = models.PositiveIntegerField(choices=ESTADO_CHOICES,
                                         default=PRODUCTO_DEFINICION)
    producto = models.ForeignKey(ProductoParaArmar)
    version = models.ForeignKey(VersionProducto)

    def __unicode__(self):
        return self.producto.slogan

    def get_detalle_producto_armado(self):
        return DetalleProductoArmado.objects.filter(producto=self)

    def validar_exclusion_ingrediente(self, ingrediente):
        cantidad = 1
        for detalle in self.get_detalle_producto_armado():
            if ingrediente.seccion.pk is detalle.ingrediente.seccion.pk:
                cantidad += 1

        if ingrediente.seccion.cantidad_exclusiones >= cantidad:
            return True
        else:
            return False

    def ver_ingredientes_producto_armado(self):
        cadena = "El prodocto {0} en version {1}. Está compuesto por ".format(
            self.version, self.version.clasificacion)

        for detalle in self.detalles_producto.all():
            cadena += detalle.ingrediente.ingrediente.nombre + ","
        return cadena


class DetalleProductoArmado(models.Model):
    """Detalle Producto Armado
    Atributos: producto, ingredientes"""
    producto = models.ForeignKey(ProductoArmado,
                                 related_name="detalles_producto")
    ingrediente = models.ForeignKey(IngredientesSeccion)

    def __unicode__(self):
        return u'%s - %s' % (
            self.producto.producto.slogan, self.ingrediente.seccion)


class PedidoManager(models.Manager):
    def obtener_en_definicion_para_editar(self, pedido_id):
        """Devuelve el pedido pasado por ID, siempre que dicha
        pedido pueda ser editar (editada en el proceso de
        definirla, o sea, en el proceso de "creacion" del
        pedido).

        En caso de no encontarse, lanza SuspiciousOperation
        """
        try:
            return self.filter(
                estado=Pedido.ESTADO_EN_DEFINICION).get(
                pk=pedido_id)
        except Pedido.DoesNotExist:
            raise (SuspiciousOperation("No se encontro pedido %s en "
                                       "estado ESTADO_EN_DEFINICION"))

    def obtener_pedido_por_cliente(self, cliente):
        return self.filter(cliente=cliente).order_by('-fechaPedido')

    def pedidos_by_filtro(self, filtro):

        if not filtro:
            return self.all().order_by('-fechaPedido')

        try:
            return self.filter(Q(email__contains=filtro) |
                               Q(celular__contains=filtro)).order_by(
                '-fechaPedido')
        except Pedido.DoesNotExist:
            raise (SuspiciousOperation("No se encontro Pedidos con este "
                                       "filtro"))

    def pedido_count_cliente(self):
        try:
            return self.values('cliente', 'cliente__user__first_name',
                               'cliente__user__last_name',
                               'cliente__user__socialaccount__provider'
                               ).annotate(cantidad_pedidos=Count('cliente')
                                          ).filter(cantidad_pedidos__gt=0)
        except Pedido.DoesNotExist:
            raise (SuspiciousOperation("No se encontraron peddo"))


class Pedido(models.Model):
    """
    Clase Pedido
    """

    objects_default = models.Manager()
    # Por defecto django utiliza el primer manager instanciado. Se aplica al
    # admin de django, y no aplica las customizaciones del resto de los
    # managers que se creen.

    objects = PedidoManager()



    SERVICIO_DELIVERY = 1
    SERVICIO_PARA_LLEVAR = 2
    SERVICIO_SUCURSALES = 3

    SERVICIO_CHOICES = (
        (SERVICIO_DELIVERY, 'Delivery'),
        (SERVICIO_PARA_LLEVAR, 'Retirar en Sucursal'),
        (SERVICIO_SUCURSALES, 'Sucursales'),
    )

    cliente = models.ForeignKey(ClienteProfile, blank=True, null=True, related_name="pedidos")
    fechaPedido = models.DateTimeField()
    hora_entrega = models.TimeField(blank=True, null=True)
    hora_solicitud = models.TimeField(blank=True, null=True)
    servicio = models.PositiveIntegerField(choices=SERVICIO_CHOICES, default=SERVICIO_DELIVERY)
    sucursal = models.ForeignKey(Sucursal, blank=True, null=True, related_name="pedidossucursal")
    precio_envio = models.DecimalField(max_digits=5, decimal_places=2,
                                       verbose_name="Costo de Envio($)",
                                       blank=True, null=True)
    tiempo_estimado_envio = models.DecimalField(max_digits=5, decimal_places=2,
                                                verbose_name="Tiempo estimado "
                                                             "de envío",
                                                blank=True, null=True)
    importe_abonado = models.DecimalField(max_digits=10, decimal_places=2,
                                          verbose_name="Importe a pagar($)",
                                          blank=True, null=True)

    # Direccion del pedido
    direccion = models.CharField(max_length=250, verbose_name='Dirección',
                                 blank=True, null=True)
    piso = models.IntegerField(blank=True, null=True)
    depto = models.CharField(max_length=50, blank=True, null=True)
    barrio = models.ForeignKey(Barrio, blank=True, null=True)
    descripcion = models.TextField(blank=True, null=True)

    # atributo para producto para armar
    producto_armar = models.ForeignKey(ProductoArmado, blank=True, null=True)

    # atributos para notificacion
    notificar_email = models.BooleanField(default=False)
    notificar_sms = models.BooleanField(default=False)
    email = models.EmailField(blank=True, null=True)
    celular = models.CharField(max_length=32, blank=True, null=True)

    favorito = models.BooleanField(default=False)
    observaciones = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return u"%s" % (self.fechaPedido)

    def sub_total(self):
        total = 0
        for d in self.get_detalle_pedido():
            total += float(d.precio) * float(d.cantidad)
        return ("%.2f" % total)

    def precio_total(self):
        total = 0
        total = float(self.sub_total()) + float(self.precio_envio)
        return ("%.2f" % total)

    def cantidad_total_productos(self):
        total = 0
        for d in self.get_detalle_pedido():
            total += d.cantidad
        return total

    def get_detalle_pedido(self):
        return DetallePedido.objects.filter(pedido=self)

    def get_vuelto(self):
        total = self.precio_total()
        vuelto = float(0 if self.importe_abonado is None else self.importe_abonado) - float(total)
        return vuelto

    def get_detalle_direccion(self):
        direccion = self.direccion
        if self.piso:
            direccion += " " + str(self.piso)
        if self.depto:
            direccion += " " + self.depto
        direccion += " " + self.barrio.nombre
        return direccion

    def get_estado_actual_display(self):
        return self.estados.last().get_estado_display()

    def get_historico_estado_actual(self):
        return self.estados.last()

    def change_status(self, new_status, observaciones=None):
        if self.get_historico_estado_actual().accepts(str(new_status)):
            HistorialEstadoPedido.objects.create(pedido=self,
                estado=new_status, fecha=fecha_hora_actual(),
                observaciones=observaciones)
        else:
            raise Exception(
                'Un pedido en estado {} no puede pasar a {}. '.format(
                    self.get_estado_actual_display(), [item[1] for item in
                        HistorialEstadoPedido.ESTADOS_CHOICES if
                        item[0] == new_status][0]))

    def puede_cancelar(self):
        if self.get_historico_estado_actual().estado in (2,3,5):
            return True
        return False

    def get_primer_detalle(self):
        detalle = self.detalles.first()
        return detalle


class DetallePedidoManager(models.Manager):
    def obtener_count_por_productos(self):
        try:
            return self.values("producto__producto").annotate(
                Count("producto__producto")). \
                order_by("-producto__producto__count")
        except DetallePedido.DoesNotExist:
            raise (SuspiciousOperation("No se encontro DetallePedido %s en "
                                       "estado ESTADO_EN_DEFINICION"))


class DetallePedido(models.Model):
    """Clase Detalle de Pedido
    Atributos: pedido, cantidad, producto,menu, estado, precio"""

    objects_default = models.Manager()
    # Por defecto django utiliza el primer manager instanciado. Se aplica al
    # admin de django, y no aplica las customizaciones del resto de los
    # managers que se creen.

    objects = DetallePedidoManager()

    pedido = models.ForeignKey(Pedido, related_name="detalles")
    cantidad = models.IntegerField()
    producto = models.ForeignKey(DetalleVersiones, blank=True, null=True)
    producto_armado = models.ForeignKey(ProductoArmado, blank=True, null=True)
    menu = models.ForeignKey(Menu, blank=True, null=True)
    promocion = models.ForeignKey(Promocion, blank=True, null=True)
    precio = models.DecimalField(max_digits=5, decimal_places=2,
                                 verbose_name="Precio($)")

    def __unicode__(self):
        total = ''
        if self.producto is not None:
            total = self.producto.producto.nombre
        elif self.menu is not None:
            total = self.menu.nombre
        elif self.promocion is not None:
            total = self.promocion.nombre
        # elif self.producto_armado is not None:
        #     total = self.producto_armado.producto.slogan
        return total

    def precio_total_unidad(self):
        total = (self.precio * self.cantidad)
        return total


class DomicilioSearchManager(models.Manager):
    def obtener_domicilio_pedido(self, pedido):
        try:
            return self.get(pedido=pedido)
        except DomicilioSearch.DoesNotExist:
            raise (SuspiciousOperation("No se encontro domicilio con"
                                       " ese pedido"))


class DomicilioSearch(models.Model):
    """Clase DomicilioSearch
    Atributos: direccion,numero_direccion,piso,depto,codigo_postal,
    localidad,barrio"""

    objects_default = models.Manager()
    objects = DomicilioSearchManager()

    pedido = models.ForeignKey(Pedido)
    direccion = models.CharField(max_length=250, verbose_name='Dirección')
    # numero_direccion = models.IntegerField(verbose_name='Número')
    piso = models.IntegerField(blank=True, null=True)
    depto = models.CharField(max_length=50, blank=True, null=True)
    # localidad = models.ForeignKey(Localidad)
    barrio = models.ForeignKey(Barrio)
    descripcion = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return u'%s, %s' % (self.direccion, self.numero_direccion)

    class Meta:
        verbose_name = "Domicilio Cliente"
        verbose_name_plural = "Domicilios"


class TiempoPromedioColaManager(models.Manager):
    def obtener_tiempo_promedio_hoy(self):
        hoy_ahora = datetime.datetime.today()
        hoy = hoy_ahora.date()
        ahora = hoy_ahora.time()
        try:
            return self.filter(dia_semanal=hoy.weekday())
        except TiempoPromedioCola.DoesNotExist:
            raise (SuspiciousOperation("No se encontro tiempo promedio de cola"))


class TiempoPromedioCola(models.Model):
    objects = TiempoPromedioColaManager()

    """Dias de la semana, compatibles con datetime.date.weekday()"""
    LUNES = 0
    MARTES = 1
    MIERCOLES = 2
    JUEVES = 3
    VIERNES = 4
    SABADO = 5
    DOMINGO = 6

    DIA_SEMANAL_CHOICES = (
        (LUNES, 'LUNES'),
        (MARTES, 'MARTES'),
        (MIERCOLES, 'MIERCOLES'),
        (JUEVES, 'JUEVES'),
        (VIERNES, 'VIERNES'),
        (SABADO, 'SABADO'),
        (DOMINGO, 'DOMINGO'),
    )
    dia_semanal = models.PositiveIntegerField(
        choices=DIA_SEMANAL_CHOICES,
    )

    hora_desde = models.TimeField()
    hora_hasta = models.TimeField()
    cantidad_cocineros = models.PositiveIntegerField()
    promedio = models.PositiveIntegerField()

    def __unicode__(self):
        return ("Tiempo promedio del pedido en cola  - Día: {0} -Hora desde: "
                "{1} - Hora hasta: {2} - Promedio: {3}".format(self.dia_semanal,
                                                               self.hora_desde,
                                                               self.hora_hasta,
                                                               self.promedio)
                )

    def valida_nuevo_tiempo_promedio_cola(self, tiempo):
        tiempos = TiempoPromedioCola.objects.all()
        tiempos_diarios = tiempos.filter(dia_semanal=tiempo.dia_semanal)
        valida = False
        for tiempo_actual in tiempos_diarios:
            if not (tiempo.hora_hasta <= tiempo_actual.hora_desde or \
                                tiempo.hora_desde >= tiempo_actual.hora_hasta):
                valida = True
        return valida

    def valida_update_tiempo_promedio_cola(self, tiempo):
        tiempos = TiempoPromedioCola.objects.all().exclude(pk=tiempo.pk)
        tiempos_diarios = tiempos.filter(dia_semanal=tiempo.dia_semanal)
        valida = False
        for tiempo_actual in tiempos_diarios:
            if not (tiempo.hora_hasta <= tiempo_actual.hora_desde or \
                                tiempo.hora_desde >= tiempo_actual.hora_hasta):
                valida = True
        return valida


class TiempoPromedioEntregaManager(models.Manager):
    def obtener_tiempo_promedio_hoy(self):
        hoy_ahora = datetime.datetime.today()
        hoy = hoy_ahora.date()
        try:
            return self.filter(dia_semanal=hoy.weekday())
        except TiempoPromedioEntrega.DoesNotExist:
            raise (SuspiciousOperation("No se encontro tiempo promedio de"
                                       " entrega"))


class TiempoPromedioEntrega(models.Model):
    objects = TiempoPromedioEntregaManager()

    """Dias de la semana, compatibles con datetime.date.weekday()"""
    LUNES = 0
    MARTES = 1
    MIERCOLES = 2
    JUEVES = 3
    VIERNES = 4
    SABADO = 5
    DOMINGO = 6

    DIA_SEMANAL_CHOICES = (
        (LUNES, 'LUNES'),
        (MARTES, 'MARTES'),
        (MIERCOLES, 'MIERCOLES'),
        (JUEVES, 'JUEVES'),
        (VIERNES, 'VIERNES'),
        (SABADO, 'SABADO'),
        (DOMINGO, 'DOMINGO'),
    )
    dia_semanal = models.PositiveIntegerField(
        choices=DIA_SEMANAL_CHOICES,
    )

    hora_desde = models.TimeField()
    hora_hasta = models.TimeField()
    delivery = models.PositiveIntegerField()
    promedio = models.PositiveIntegerField()

    def __unicode__(self):
        return ("Tiempo promedio de entrega  - Día: {0} -Hora desde: "
                "{1} - Hora hasta: {2} - Promedio: {3}".format(self.dia_semanal,
                                                               self.hora_desde,
                                                               self.hora_hasta,
                                                               self.promedio)
                )

    def valida_nuevo_tiempo_promedio_entrega(self, tiempo):
        tiempos = TiempoPromedioEntrega.objects.all()
        tiempos_diarios = tiempos.filter(dia_semanal=tiempo.dia_semanal)
        valida = False
        for tiempo_actual in tiempos_diarios:
            if not (tiempo.hora_hasta <= tiempo_actual.hora_desde or \
                                tiempo.hora_desde >= tiempo_actual.hora_hasta):
                valida = True
        return valida

    def valida_update_tiempo_promedio_entrega(self, tiempo):
        tiempos = TiempoPromedioEntrega.objects.all().exclude(pk=tiempo.pk)
        tiempos_diarios = tiempos.filter(dia_semanal=tiempo.dia_semanal)
        valida = False
        for tiempo_actual in tiempos_diarios:
            if not (tiempo.hora_hasta <= tiempo_actual.hora_desde or \
                                tiempo.hora_desde >= tiempo_actual.hora_hasta):
                valida = True
        return valida


class TiempoPromedioProductoManager(models.Manager):
    def obtener_tiempo_by_producto(self, producto):
        try:
            return self.filter(producto=producto)
        except TiempoPromedioProducto.DoesNotExist:
            raise (SuspiciousOperation("No se encontro tiempo preparacion"
                                       " para ese producto"))


class TiempoPromedioProducto(models.Model):
    objects = TiempoPromedioProductoManager()
    producto = models.ForeignKey(Producto, related_name="tiempo_preparacion")
    cantidad_minima = models.PositiveIntegerField()
    cantidad_maxima = models.PositiveIntegerField()
    promedio = models.PositiveIntegerField()

    def __unicode__(self):
        return ("Tiempo preparacion promedio producto {0}- cantidad mínima: {1}"
                "-cantidad maxima: {2} ".format(self.producto,
                                                self.cantidad_minima,
                                                self.cantidad_maxima,
                                                self.promedio)
                )

    def valida_nuevo_tiempo_promedio_producto(self, tiempo):
        tiempos = TiempoPromedioProducto.objects.all()
        tiempos_diarios = tiempos.filter(producto=tiempo.producto)
        valida = False
        for tiempo_actual in tiempos_diarios:
            if not (tiempo.cantidad_maxima < tiempo_actual.cantidad_minima or
                            tiempo.cantidad_minima >
                            tiempo_actual.cantidad_maxima):
                valida = True
        return valida

    def valida_update_tiempo_promedio_producto(self, tiempo):
        tiempos = TiempoPromedioProducto.objects.all().exclude(pk=tiempo.pk)
        tiempos_diarios = tiempos.filter(producto=tiempo.producto)
        valida = False
        for tiempo_actual in tiempos_diarios:
            if not (tiempo.cantidad_maxima < tiempo_actual.cantidad_minima or
                            tiempo.cantidad_minima >
                            tiempo_actual.cantidad_maxima):
                valida = True
        return valida


class HistorialEstadoPedido(models.Model):

    ESTADO_EN_DEFINICION = 1
    PENDIENTE = 2
    EN_PRODUCCION = 3
    LISTO_PARA_ENTREGAR = 4
    EN_ENVIO = 5
    ENTREGADO = 6
    CANCELADO = 7
    ESTADOS_CHOICES = (
        (ESTADO_EN_DEFINICION, '(en definicion)'),
        (PENDIENTE, 'Pendiente'),
        (EN_PRODUCCION, 'En Produccion'),
        (LISTO_PARA_ENTREGAR, 'Listo Para Entregar'),
        (EN_ENVIO, 'En Envio'),
        (ENTREGADO, 'Entregado'),
        (CANCELADO, 'Cancelado'),
    )
    sm_pedido = {
        ESTADO_EN_DEFINICION: {'2': PENDIENTE, '7': CANCELADO},
        PENDIENTE: {'3': EN_PRODUCCION, '7': CANCELADO},
        EN_PRODUCCION: {'4': LISTO_PARA_ENTREGAR, '7': CANCELADO},
        LISTO_PARA_ENTREGAR: {'5': EN_ENVIO, '7': CANCELADO},
        EN_ENVIO: {'6': ENTREGADO, '7': CANCELADO}
    }

    pedido = models.ForeignKey(Pedido, related_name='estados')
    estado = models.PositiveIntegerField(choices=ESTADOS_CHOICES,
                                         default=ESTADO_EN_DEFINICION)
    fecha = models.DateTimeField()
    observaciones = models.CharField(max_length=255, blank=True, null=True)

    def __unicode__(self):
        return "estado {0} fecha{1}".format(self.get_estado_display(),
                                            self.fecha)

    def accepts(self, s):
        return s in self.sm_pedido[self.estado]


