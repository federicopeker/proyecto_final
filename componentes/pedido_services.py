# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import datetime
import codecs
from componentes.models import *
from django.core.exceptions import SuspiciousOperation
from django.core.mail import EmailMessage
from django.db.models import Count
import logging as _logging
from constance import config

logger = _logging.getLogger(__name__)


class PedidoServices():

    def descontar_stock(self, pedido):
        for d in pedido.get_detalle_pedido():  # recorro los detalles del pedido
            if d.producto is not None:  # Si el detalle es un producto
                for i in d.producto.producto.get_ingredientes():  # recorre los
                    # ingrediente del productos
                    cantidad = i.cantidad * d.cantidad
                    ing = Ingrediente.objects.get(pk=i.ingrediente.id)
                    ing.stock_actual = ing.stock_actual - cantidad
                    ing.save()

            elif d.menu is not None:  # Si el detalle es un menu
                for detm in d.menu.get_detalle_menu():  # recorro los detalles
                    # de menu
                    for promenu in detm.producto.get_ingredientes():  # recorre
                        # lo ingrediente de los productos del detalle
                        cantidad = promenu.cantidad * detm.cantidad * d.cantidad
                        #cantidad = cantidad * d.cantidad
                        ing = Ingrediente.objects.get(
                            pk=promenu.ingrediente.id)
                        ing.stock_actual = ing.stock_actual - cantidad
                        ing.save()

            elif d.promocion is not None:  # si el detalle es una promocion
                for promoPro in d.promocion.get_detalle_promocion():
                    # recorre los productos en el detalle de promocion

                    if promoPro.producto is not None:  # si es un producto de la promocion
                        for promoing in promoPro.producto.get_ingredientes():
                            # recorre los ingrediente de los productos
                            cantidad = promoPro.cantidad * promoing.cantidad * d.cantidad
                            #cantidad += cantidad * d.cantidad
                            ing = Ingrediente.objects.get(
                                pk=promoing.ingrediente.id)
                            ing.stock_actual = ing.stock_actual - cantidad
                            ing.save()

                    elif promoPro.menu is not None:  # si es un menu de la promocion
                        for promoMenu in promoPro.menu.get_detalle_menu():
                            # recorre los productos del detalle de menu
                            for promoing in promoMenu.producto.get_ingredientes():
                                # recorre los ingredientes de los productos del
                                # detalle de menu
                                cantidad = promoMenu.cantidad * promoing.cantidad * d.cantidad
                                #cantidad = cantidad * d.cantidad
                                #cantidad = cantidad * promoMenu.cantidad
                                ing = Ingrediente.objects.get(
                                    pk=promoing.ingrediente.id)
                                ing.stock_actual -= cantidad
                                ing.save()

            elif d.producto_armado is not None:  # si es un producto para armar
                for det_prod in d.producto_armado.detalles_producto.all():
                    ing = Ingrediente.objects.get(pk=det_prod.ingrediente.ingrediente.pk)
                    ing.stock_actual = ing.stock_actual - d.cantidad
                    ing.save()

    def obtener_productos_popularares(self):
        count_productos = DetallePedido.objects.obtener_count_por_productos()
        productos = []
        if count_productos is not None:
            for producto in count_productos:
                if producto.get('producto__producto') is not None:
                    productos.append(Producto.objects.get(
                        pk=producto.get('producto__producto')))
        return productos

    def calcular_tiempo_pedido(self, pedido):
        tiempo_preparacion = self.calcular_tiempo_preparacion(pedido)
        tiempo_cola = self.calcular_tiempo_cola()
        tiempo_entrega = self.calcular_tiempo_entrega()
        tiempo_total = tiempo_preparacion + tiempo_cola + tiempo_entrega
        return tiempo_total

    def calcular_tiempo_preparacion(self, pedido):
        productos_cantidad = self._obtener_cantidad_pedido(pedido)
        print productos_cantidad
        tiempo_total = 0
        for producto_id in productos_cantidad:
            cantidad = productos_cantidad[producto_id]
            producto = Producto.objects.get(pk=producto_id)
            tiempos = TiempoPromedioProducto.objects.obtener_tiempo_by_producto(
                producto)
            for tiempo in tiempos:
                if tiempo.cantidad_minima <= cantidad <= tiempo.cantidad_maxima:
                    tiempo_total += tiempo.promedio
        return tiempo_total

    def calcular_tiempo_cola(self):
        hoy_ahora = datetime.datetime.today()
        hoy = hoy_ahora.date()
        ahora = hoy_ahora.time()
        tiempos = TiempoPromedioCola.objects.obtener_tiempo_promedio_hoy()
        tiempo_total = 0
        for tiempo_actual in tiempos:
            if tiempo_actual.hora_desde <= ahora and ahora <=\
                    tiempo_actual.hora_hasta:
                tiempo_total = tiempo_actual.promedio
                break
        return tiempo_total

    def calcular_tiempo_entrega(self):
        hoy_ahora = datetime.datetime.today()
        hoy = hoy_ahora.date()
        ahora = hoy_ahora.time()
        tiempos = TiempoPromedioEntrega.objects.obtener_tiempo_promedio_hoy()
        tiempo_total = 0
        for tiempo_actual in tiempos:
            if tiempo_actual.hora_desde <= ahora and ahora <=\
                    tiempo_actual.hora_hasta:
                tiempo_total = tiempo_actual.promedio
                break
        return tiempo_total

    def _obtener_cantidad_pedido(self, pedido):
        productos_dict = {}
        for detalle in pedido.get_detalle_pedido():
            if detalle.producto is not None:
                if detalle.producto.producto.pk not in productos_dict.keys():
                    productos_dict[detalle.producto.producto.pk] = detalle.cantidad
                else:
                    productos_dict[detalle.producto.producto.pk] += detalle.cantidad
            elif detalle.menu is not None:
                for det_menu in detalle.menu.get_detalle_menu():
                    cantidad = detalle.cantidad * det_menu.cantidad
                    if det_menu.producto.pk not in productos_dict.keys():
                        productos_dict[det_menu.producto.pk] = cantidad
                    else:
                        productos_dict[det_menu.producto.pk] += cantidad
            elif detalle.promocion is not None:
                for det_promo in detalle.promocion.detalles.all():
                    # recorre los productos en el detalle de promocion

                    if det_promo.producto is not None:  # si es un producto de la promocion
                        cantidad = detalle.cantidad * det_promo.cantidad
                        if det_promo.producto.pk not in productos_dict.keys():
                            productos_dict[det_promo.producto.pk] = cantidad
                        else:
                            productos_dict[det_promo.producto.pk] += cantidad
                    elif det_promo.menu is not None:  # si es un menu de la promocion
                        for promoMenu in det_promo.menu.get_detalle_menu():
                            cantidad = detalle.cantidad * promoMenu.cantidad * det_promo.cantidad
                            if promoMenu.producto.pk not in productos_dict.keys():
                                productos_dict[promoMenu.producto.pk] = cantidad
                            else:
                                productos_dict[promoMenu.producto.pk] += cantidad
            elif detalle.producto_armado is not None:
                producto_tipo =detalle.producto_armado.producto.tipo_producto.productos.all()
                print producto_tipo
                if producto_tipo[0].pk not in productos_dict.keys():
                    productos_dict[producto_tipo[0].pk] = detalle.cantidad
                else:
                    productos_dict[producto_tipo[0].pk] += detalle.cantidad

        return productos_dict

    def enviar_email(self, pedido, mensaje):
        titulo = 'Su pedido en el sistema'
        contenido = "Notificacion del pedido"
        contenido += "\n" + "Numero de pedido: " + str(pedido.id) + "\n"
        contenido += "Hora de entrega aproximada: " + str(pedido.hora_entrega) + "\n"
        contenido += "Estado: " + str(pedido.get_estado_actual_display()) + "\n"
        #contenido += "mensaje :" + str(mensaje) + "\n"
        contenido += "\n " + "\n"
        contenido += "Lo saluda atentamente el equipo de {} \n".format(config.NOMBRE_EMPRESA)
        correo = EmailMessage(titulo, contenido, to=[pedido.email])
        correo.send()

    def escribir_sms_en_send(self, numero, mensaje):
        """
        Este metodo escribir los archivos sms en el servidor
        """

        dwgconfig_income_path = '/home/federico/ProyectoFinal/send/'

        try:
            sms_partfilename = '%s%s' % (dwgconfig_income_path, numero)

            sms = codecs.open(sms_partfilename, 'w', 'utf-8')
            sms.write('%s\n' % numero)
            sms.write('%s\n' % mensaje)

            sms.close()

        except:
            logger.error("error en la escritura del sms")

    def notificar_pedido(self, pedido, mensaje):
        if pedido.notificar_email:
            self.enviar_email(pedido, mensaje)

        if pedido.notificar_sms:
            self.escribir_sms_en_send(pedido.celular, mensaje)

    def pedidos_por_estado(self, estado):
        return [pedido for pedido in Pedido.objects.all() if
                pedido.get_historico_estado_actual() is not None and
                pedido.get_historico_estado_actual().estado == estado]

    def obtener_pedidos_exclude_definicion(self, pedidos=None):
        if not pedidos:
            pedidos = Pedido.objects.all()
        excludes = []
        for pedido in pedidos:
            if pedido.get_historico_estado_actual().estado == \
                    HistorialEstadoPedido.ESTADO_EN_DEFINICION:
                excludes.append(pedido.id)
        return pedidos.exclude(id__in=excludes)

    def obtener_count_pedidos(self, pedidos):
        try:
            return pedidos.values('cliente', 'cliente__user__first_name',
                                  'cliente__user__last_name',
                                  'cliente__user__socialaccount__provider'
                                  ).annotate(cantidad_pedidos=Count('cliente')
                                             ).filter(cantidad_pedidos__gt=0)
        except Pedido.DoesNotExist:
            raise (SuspiciousOperation("No se encontraron peddo"))

    def obtener_count_pedidos_generico(self, pedidos):
        try:
            return pedidos.values('cliente__id', 'cliente__user__first_name',
                                  'cliente__user__last_name', 'cliente__barrio'
                                  ).annotate(cantidad_pedidos=Count('cliente')
                                             ).filter(cantidad_pedidos__gt=0)
        except Pedido.DoesNotExist:
            raise (SuspiciousOperation("No se encontraron peddo"))

    def obtener_pedidos_por_estado(self, pedidos, estado):
        includes = [pedido.id for pedido in pedidos
                    if pedido.get_historico_estado_actual().estado == estado]
        return pedidos.filter(id__in=includes)

    def obtener_count_pedidos_barrio(self, pedidos):
        try:
            return pedidos.values('barrio__nombre').annotate(
                cantidad_pedidos=Count('barrio')).filter(
                servicio=Pedido.SERVICIO_DELIVERY)
        except Pedido.DoesNotExist:
            raise (SuspiciousOperation("No se encontraron pedidos"))

    def obtener_pedidos_por_cliente(self, pedidos, cliente):
        try:
            return pedidos.filter(cliente=cliente)
        except Pedido.DoesNotExist:
            raise (SuspiciousOperation("No se encontraron pedido"))

    def repetir_pedido(self, pedido):
        fecha = datetime.datetime.now()
        pedido_nuevo = Pedido.objects.create(
            fechaPedido=fecha, servicio=pedido.servicio,
            precio_envio=pedido.precio_envio, direccion=pedido.direccion,
            piso=pedido.piso, depto=pedido.depto, barrio_id=pedido.barrio_id,
            sucursal=pedido.sucursal
        )
        for detalle in pedido.detalles.all():
            DetallePedido.objects.create(
                pedido=pedido_nuevo, cantidad=detalle.cantidad,
                producto=detalle.producto, menu=detalle.menu,
                producto_armado=detalle.producto_armado, precio=detalle.precio,
                promocion=detalle.promocion)

        return pedido_nuevo