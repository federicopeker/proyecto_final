"""
WSGI and Django Middlware for mobiledetect
"""


class BaseMobileDetectMiddleware(object):
    pass


class WSGIMobileDetectMiddleware(BaseMobileDetectMiddleware):
    pass


class DjangoMobileDetectMiddleware(BaseMobileDetectMiddleware):
    pass

