"""
	Developed by 
	Andrea Stagi <stagi.andrea@gmail.com>

	Tiny4py: python wrapper to use main url shortener services in your apps
	Copyright (C) 2010 Andrea Stagi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published 
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

TINY4PY_BITLY_USR="bitlyapidemo"
TINY4PY_BITLY_API="R_0da49e0a9118ff35f52f629d2d71bf07"

BITLY_OK=200
BITLY_ERR=500
BITLY_TLERR=403

BITLY_LOGERR="INVALID_LOGIN"
BITLY_URLERR="INVALID_URI"

GOOGLE_ID="goo.gl"
BITLY_ID="bit.ly"
TINYURL_ID="tinyurl"

GOOGLE_BASEURL="http://goo.gl/api/"
BITLY_BASEURL="http://api.bit.ly/v3/"
TINYURL_BASEURL="http://tinyurl.com/api-"

