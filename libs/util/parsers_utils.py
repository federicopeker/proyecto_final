'''
Created on 09/09/2011

@author: crian
'''
import re
import cgi
import json
from django.utils import datetime_safe
import datetime
import decimal
from webserv_utils import Respuesta
from django.db.models.base import ModelState
from django.db import models
from django.db.models.query import QuerySet
from django.db.models.fields.files import ImageFieldFile


def json_serial(o):
    DATE_FORMAT = "%Y-%m-%d"
    TIME_FORMAT = "%H:%M:%S"

    if isinstance(o, datetime.datetime):
        d = datetime_safe.new_datetime(o)
        return d.strftime("%s %s" % (DATE_FORMAT, TIME_FORMAT))
    elif isinstance(o, datetime.date):
        d = datetime_safe.new_date(o)
        return d.strftime(DATE_FORMAT)
    elif isinstance(o, datetime.time):
        return o.strftime(TIME_FORMAT)
    elif isinstance(o, decimal.Decimal):
        return str(o)
    elif isinstance(o, Respuesta):
        return o.__dict__
    elif isinstance(o, models.Model):
        aux = o.__dict__
        return aux
    elif isinstance(o, ModelState):
        return "x"
    elif isinstance(o, ImageFieldFile):
        return o.name
    elif isinstance(o, QuerySet):
        data = []
        for item in o:
            data.append(item.__dict__)
        return data


def toJSON(obj):
    return json.dumps(obj, default=json_serial)

def plaintext2html(text, tabstop=4):
    re_string = re.compile(r'(?P<htmlchars>[<&>])|(?P<space>^[ \t]+)|(?P<lineend>\r\n|\r|\n)|(?P<protocal>(^|\s)((http|ftp)://.*?))(\s|$)', re.S|re.M|re.I)
    def do_sub(m):
        c = m.groupdict()
        if c['htmlchars']:
            return cgi.escape(c['htmlchars'])
        if c['lineend']:
            return '<br>'
        elif c['space']:
            t = m.group().replace('\t', '&nbsp;'*tabstop)
            t = t.replace(' ', '&nbsp;')
            return t
        elif c['space'] == '\t':
            return ' '*tabstop;
        else:
            url = m.group('protocal')
            if url.startswith(' '):
                prefix = ' '
                url = url[1:]
            else:
                prefix = ''
            last = m.groups()[-1]
            if last in ['\n', '\r', '\r\n']:
                last = '<br>'
            return '%s<a href="%s">%s</a>%s' % (prefix, url, url, last)
    return re.sub(re_string, do_sub, text)

