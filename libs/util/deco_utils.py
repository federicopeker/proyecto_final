# -*- coding: utf-8 -*-
'''
Created on 09/09/2011

@author: crian
'''
from django.http import HttpResponse
from parsers_utils import toJSON


def decorador_con_parametros(d):
    def decorador(*args, **kwargs):
        def inner(func):
            return d(func, *args, **kwargs)
        return inner
    return decorador


def jsonResponse(funcion):
    def inner(*args, **kwargs):
        """
        Decorada por jsonResponse: Los datos de este servicio seran
        devueltos en formato json
        """
        objRespuesta = funcion(*args, **kwargs)
        response = HttpResponse(toJSON(objRespuesta), content_type="application/json")
        #response.status_code= objRespuesta.status_code
        return response

    inner = preserve_documentation(inner, funcion) 
     
    return inner

def onlyautodoc(funcion):
    def inner(*args, **kwargs):
        """
        Decorada por onlyautodoc para autodocumentacion
        """
        objRespuesta = funcion(*args, **kwargs)
        return objRespuesta

    inner = preserve_documentation(inner, funcion) 
     
    return inner


def preserve_documentation(inner, funcion) :    
    #verifico si es una funcion original o viene previamente decorada    

    if hasattr(funcion, "foriginal"):
        inner.foriginal = funcion.foriginal
    else:
        inner.foriginal = funcion
    
    foriginal = inner.foriginal
    #Si la funcion original tiene documentacion le contateno la documentacion
    #del decorador
    if foriginal.__doc__:
        foriginal.__doc__ += str(inner.__doc__)
    else:
        foriginal.__doc__ = "El servicio: " \
            + str(foriginal.__name__)  + " no esta documentado. " + str(inner.__doc__)
    
    return inner
