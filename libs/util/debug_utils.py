'''
Created on 09/09/2011

@author: crian
'''

from pprint import pformat
import logging.handlers
import logging


def print_r(x):
    if hasattr(x, '__dict__'):
        d = {
            '__str__':str(x),
            '__unicode__':unicode(x),
            '__repr__':repr(x),
        }
        d.update(x.__dict__)
        x = d
    output = pformat(x)+'\n'
    print output
    return output

class Logger():
    def __init__(self, file):
        """
        @param file to store logs in
        """
        self.logger = logging.getLogger("web")
        self.handler = logging.handlers.TimedRotatingFileHandler(file, when="D", interval=1)
        self.formatter = logging.Formatter('%(asctime)s %(threadName)s %(levelname)s %(message)s')
        
        self.handler.setFormatter(self.formatter)
        self.logger.addHandler(self.handler)
        self.logger.setLevel(logging.DEBUG)
        
    def log(self, msg, type = "info", exc_info = None):
        type = type.lower()
        print msg
        try :
            #versatile syntax
            getattr(self.logger, type)(msg, exc_info=exc_info)
        except AttributeError: 
            raise AttributeError("Parameter type incorrect in Logger.log()")
        
        
