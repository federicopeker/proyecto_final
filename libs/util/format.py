# coding=utf-8
class CurrencyUtil():
    CURRENCY_CODES = {
        'USD': u'$',
        'EUR': u'€',
    }

    @classmethod
    def get_symbol(cls, currency_code):
        return cls.CURRENCY_CODES[currency_code]

    @staticmethod
    def format_currency(amount, currency_code):
        symbol = CurrencyUtil.get_symbol(currency_code)
        result = u"{} {:10.2f}".format(symbol, amount)
        return result