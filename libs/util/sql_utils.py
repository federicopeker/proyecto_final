'''
Created on 09/09/2011

@author: crian
'''

from _mysql import escape_string as mysql_escape


def calcular_acotacion_temporal(fperiodo, fdesde, fhasta, campo):
    str = ""
    
    if not fperiodo and not (fdesde or fhasta ):
        fperiodo = "2 WEEK "
    #avoid SQL injection
    elif len(fperiodo.split(" ")) > 2:
        fperiodo = "2 WEEK "
    
    if fperiodo :
        str = " " + mysql_escape(campo) + " BETWEEN CURDATE() - INTERVAL "+ mysql_escape(fperiodo) +" AND CURDATE()"
    elif fdesde and fhasta :
        str = " " + mysql_escape(campo) + " BETWEEN '"+mysql_escape(fdesde)+"' AND '" + mysql_escape(fhasta) + "' "
    
    return str

def dictfetchall(cursor):
    "Returns all rows from a cursor as a dict"
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row))
        for row in cursor.fetchall()
    ]

