'''
Created on 09/09/2011

@author: crian
'''


class Respuesta(object):
    """
    Clase que estandariza el formato de las respuestas de los servicios
    """
    datos = ""
    status = "" 
    
    def __init__(self, datos ="", status_code=200):
        self.data = datos
        self.status = {"code":"0","message":"Success"}
        self.status_code = 200
        
        
class MngError():
    @staticmethod
    def getError(nroError):
        resp = Respuesta()
        if nroError == "1":
            resp.status = {'code':'1', 
                           'message':'Success'}
        else:
            resp.status = {'code':'99', 'message':'Error desconocido'}
        
        return resp
    
    @staticmethod
    def noTienePermisos(detalle=""):
        resp = Respuesta()
        resp.status = {'code':'13', 
                       'message':'No tiene permisos', 
                       'detalle':detalle}
        resp.status_code = 401   
        return resp
    @staticmethod
    def noHayRolEnCampania(detalle=""):
        resp = Respuesta()
        resp.status = {'code':'13', 
                       'message':'No se encontro rol en campania', 
                       'detalle':detalle}
        resp.status_code = 404   
        return resp
    @staticmethod
    def errorCampaniaNoEncontrada(detalle=""):
        resp = Respuesta()
        resp.status = {'code':'13', 
                       'message':'Campania no encontrada', 
                       'detalle':detalle}
        resp.status_code = 404   
        return resp
    
    @staticmethod
    def errorCampaniaNoBorrable(detalle=""):
        resp = Respuesta()
        resp.status = {'code':'153', 
                       'message':'Campania no Borrable', 
                       'detalle':detalle}
        resp.status_code = 404   
        return resp
    
    @staticmethod
    def errorReglaEtiquetadoNoEncontrada(detalle=""):
        resp = Respuesta()
        resp.status = {'code':'14', 
                       'message':'Regla de Etiquetado no encontrada', 
                       'detalle':detalle}
        resp.status_code = 404   
        return resp
    
    @staticmethod
    def errorRegladeValorizacionNoEncontrada(detalle=""):
        resp = Respuesta()
        resp.status = {'code':'15', 'message':'Regla de Valorizacion no encontrada', 'detalle':detalle}
        resp.status_code = 404   
        return resp
    
    @staticmethod
    def errorAutorNoEncontrado(detalle=""):
        resp = Respuesta()
        resp.status = {'code':'16', 'message':'Autor no encontrado', 'detalle':detalle}
        resp.status_code = 404   
        return resp
    
    @staticmethod
    def errorRedSocialNoEncontrada(detalle=""):
        resp = Respuesta()
        resp.status = {'code':'17','message':'Red Social no encontrada','detalle':detalle}
        resp.status_code = 404   
        return resp
    
    @staticmethod
    def errorNoSePuedeParsearJSON(detalle=""):
        resp = Respuesta()
        resp.status = {'code':'18','message':'No se Puede Parsear JSON','detalle':detalle}
        resp.status_code = 400   
        return resp
    
    @staticmethod
    def errorPublicacionNoEncontrada(detalle=""):
        resp = Respuesta()
        resp.status = {'code':'19','message':'Publicacion no Encontrada','detalle':detalle}
        resp.status_code = 404   
        return resp
    
    @staticmethod
    def errorEtiquetaNoEncontrada(detalle=""):
        resp = Respuesta()
        resp.status = {'code':'19','message':'Publicacion no Encontrada','detalle':detalle}
        resp.status_code = 404   
        return resp
    
    
    @staticmethod
    def errorEmailDuplicado(detalle=""):
        resp = Respuesta()
        resp.status = {'code':'112', 'message':'El email ya esta registrado', 'detalle':detalle}
        resp.status_code = 400   
        return resp
    
    @staticmethod
    def errorUsuarioNoEncontrado(detalle=""):
        resp = Respuesta()
        resp.status = {'code':'77', 'message':'El usuario no se encuentra', 'detalle':detalle}
        resp.status_code = 200   
        return resp
    
    @staticmethod
    def errorNoTipificado(detalle=""):
        resp = Respuesta()
        resp.status = {'code':'757', 'message':'Error no tipificado', 'detalle':detalle}
        resp.status_code = 400   
        return resp
    @staticmethod
    def errorInformeNoEncontrado(detalle=""):
        resp = Respuesta()
        resp.status = {'code':'757', 'message':'Error informe no encontrado' , 'detalle':detalle}
        resp.status_code = 404   
        return resp
    
    @staticmethod
    def errorNoTienePermisos(detalle=""):
        resp = Respuesta()
        resp.status = {'code':'7457', 'message':'Error no tiene permisos', 'detalle':detalle}
        resp.status_code = 401   
        return resp
    
    @staticmethod
    def errorFaltanParametros(detalle=""):
        resp = Respuesta()
        resp.status = {'code': '1677', 'message':'Faltan parametros obligatorios', 'detalle':detalle}
        resp.status_code = 400   
        return resp

    @staticmethod
    def errorNoEncontrado(detalle="") :
        resp.status = {'code': '404', 'message': 'Objeto no encontrado'}
        resp = Respuesta()
        resp.status_code = 404
        return resp

