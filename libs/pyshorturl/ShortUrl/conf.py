
USER_AGENT_STRING = 'pyShortUrl'
SUPPORTED_SERVICES = ('goo.gl', 'bit.ly', 'tinyurl.com', 'v.gd', 'is.gd')
SUPPORTED_DOMAINS = ('bit.ly', 'bitly.com', 'goo.gl', 'is.gd', 'j.mp', 'tinyurl.com', 'v.gd')
