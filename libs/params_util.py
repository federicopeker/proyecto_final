'''
Created on 09/09/2011

@author: crian
'''

import time
import re

class ParamMissing(Exception) :
    pass


class ParamError(Exception):
    pass

def getParam(parametro, request, tipo=None, obligatorio=False):
    
    """
    Obtiene los parametros del request y los valida
    """
    debug = True 
    if debug == True:
        data = request.REQUEST
    else:
        data = request.POST
        
    if data.has_key(parametro) and data[parametro]:
        if tipo == "fecha":
            try:
                time.strptime(data[parametro], '%Y-%m-%d')
            except:
                raise ParamError("El parametro " + parametro + " no es fecha")
        elif tipo == "email":
            if re.match("^[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+.[a-zA-Z]{2,6}$", data[parametro]) == None:
                raise ParamError("El parametro " + parametro + " no es email")
        elif tipo == "cadena":
            if len(data[parametro]) < 2:
                raise ParamError("El parametro " + parametro + " no es una cadena de mas de 2 caracteres")
        elif tipo == "numerico":
            try:
                float(data[parametro])
            except:
                raise ParamError("El parametro " + parametro + " no es numerico")
                                
            
        return  data[parametro]
    elif obligatorio :
        # caught by web.middleware.MissingParameterMiddleware
        raise ParamError("el parametro " + parametro + " es obligatorio")
    else :
        return ""
    
class Respuesta(object):
    """
    Clase que estandariza el formato de las respuestas de los servicios
    """
    datos = ""
    status = "" 
    
    def __init__(self, datos ="", error_code="0", error_msg="error"):
        self.data = datos
        
        if error_code != "0":
            self.status = {"code":error_code,"message":error_msg}
        else:
            self.status = {"code":"0","message":"Success"}
