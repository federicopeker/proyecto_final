'''
Created on 09/09/2011

@author: crian
'''
import re
import cgi
from django.utils import simplejson, datetime_safe
import datetime
import decimal
from params_util import Respuesta
from django.db.models.base import ModelState
from django.db import models
from django.db.models.query import QuerySet


def toJSON(obj):
    return simplejson.dumps(obj, cls=MyDjangoJSONEncoder)
    

class MyDjangoJSONEncoder(simplejson.JSONEncoder):
    """
    JSONEncoder subclass that knows how to encode date/time and decimal types.
    """

    DATE_FORMAT = "%Y-%m-%d"
    TIME_FORMAT = "%H:%M:%S"

    def default(self, o):
        if isinstance(o, datetime.datetime):
            d = datetime_safe.new_datetime(o)
            return d.strftime("%s %s" % (self.DATE_FORMAT, self.TIME_FORMAT))
        elif isinstance(o, datetime.date):
            d = datetime_safe.new_date(o)
            return d.strftime(self.DATE_FORMAT)
        elif isinstance(o, datetime.time):
            return o.strftime(self.TIME_FORMAT)
        elif isinstance(o, decimal.Decimal):
            return str(o)
        elif isinstance(o, Respuesta):
            return o.__dict__
        elif isinstance(o, models.Model):
            aux = o.__dict__
            return aux           
        elif isinstance(o, ModelState):
            return "x"
        elif isinstance(o, QuerySet):
            data = []
            for item in o:
                data.append(item.__dict__)
            return data
        else:
            return super(MyDjangoJSONEncoder, self).default(o)

def plaintext2html(text, tabstop=4):
    re_string = re.compile(r'(?P<htmlchars>[<&>])|(?P<space>^[ \t]+)|(?P<lineend>\r\n|\r|\n)|(?P<protocal>(^|\s)((http|ftp)://.*?))(\s|$)', re.S|re.M|re.I)
    def do_sub(m):
        c = m.groupdict()
        if c['htmlchars']:
            return cgi.escape(c['htmlchars'])
        if c['lineend']:
            return '<br>'
        elif c['space']:
            t = m.group().replace('\t', '&nbsp;'*tabstop)
            t = t.replace(' ', '&nbsp;')
            return t
        elif c['space'] == '\t':
            return ' '*tabstop;
        else:
            url = m.group('protocal')
            if url.startswith(' '):
                prefix = ' '
                url = url[1:]
            else:
                prefix = ''
            last = m.groups()[-1]
            if last in ['\n', '\r', '\r\n']:
                last = '<br>'
            return '%s<a href="%s">%s</a>%s' % (prefix, url, url, last)
    return re.sub(re_string, do_sub, text)

