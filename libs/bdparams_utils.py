'''
Created on 09/09/2011

@author: crian
'''
from bdresponse_utils import Respuesta

# import time
from datetime import datetime, time
import re

class ParamMissing(Exception) :
    pass


class ParamError(Exception):
    pass

def get_param(request, parametro, tipo=None, obligatorio=False):
    
    """
    Obtiene los parametros del request y los valida
    """
    debug = True 
    if debug == True:
        data = request.REQUEST
    else:
        data = request.POST
        
    if data.has_key(parametro) and data[parametro]:
        if tipo == "date":
            try:
                mydate = datetime.strptime(data[parametro], '%Y-%m-%d').date()
                return mydate
            except:
                raise ParamError("El parametro " + parametro + " no es fecha")
        elif tipo == 'time':
            try:
                mytime = datetime.strptime(data[parametro], '%H:%M').time()
                return mytime
            except:
                raise ParamError("The parameter " + parametro + " is not time")
        elif tipo == "email":
            if re.match("^[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+.[a-zA-Z]{2,6}$", data[parametro]) == None:
                raise ParamError("El parametro " + parametro + " no es email")
        elif tipo == "cadena":
            if len(data[parametro]) < 2:
                raise ParamError("El parametro " + parametro + " no es una cadena de mas de 2 caracteres")
        elif tipo == "numerico":
            try:
                float(data[parametro])
            except:
                raise ParamError("El parametro " + parametro + " no es numerico")
                                
            
        return  data[parametro]
    elif obligatorio :
        # caught by web.middleware.MissingParameterMiddleware
        raise ParamError("el parametro " + parametro + " es obligatorio")
    else :
        return ""
