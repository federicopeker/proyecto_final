from random import choice
import string
def generate_string(length=8, chars=string.letters + string.digits):
    return ''.join([choice(chars) for i in range(length)])

def base_url(request) :
    return ("http://" + request.META['HTTP_HOST'] + "/")
