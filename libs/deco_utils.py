# -*- coding: utf-8 -*-
'''
Created on 09/09/2011

@author: crian
'''
from django.http import HttpResponse
from bdparsers_utils import toJSON
from bdparams_utils import getParam
from bdresponse_utils import MngError
from django.conf import settings
from functools import wraps


def onlyautodoc(funcion):
    def inner(*args, **kwargs):
        """
        Decorada por onlyautodoc para autodocumentacion
        """
        objRespuesta = funcion(*args, **kwargs)
        return objRespuesta

    inner = preserve_documentation(inner, funcion) 
     
    return inner


def preserve_documentation(inner, funcion) :    
    #verifico si es una funcion original o viene previamente decorada    

    if hasattr(funcion, "foriginal"):
        inner.foriginal = funcion.foriginal
    else:
        inner.foriginal = funcion
    
    foriginal = inner.foriginal
    #Si la funcion original tiene documentacion le contateno la documentacion
    #del decorador
    if foriginal.__doc__:
        foriginal.__doc__ += str(inner.__doc__)
    else:
        foriginal.__doc__ = "El servicio: " \
            + str(foriginal.__name__)  + " no esta documentado. " + str(inner.__doc__)
    
    return inner


def method_cache(seconds=0):
    """
    A `seconds` value of `0` means that we will not memcache it.

    If a result is cached on instance, return that first.  If that fails, check
    memcached. If all else fails, hit the db and cache on instance and in memcache.

    ** NOTE: Methods that return None are always "recached".
    """

    from hashlib import sha224
    from django.core.cache import cache

    def inner_cache(method):

        def x(instance, *args, **kwargs):
            key = sha224(str(method.__module__) + str(method.__name__) + \
                str(instance.id) + str(args) + str(kwargs)).hexdigest()


            if hasattr(instance, key):
                # has on class cache, return that
                result = getattr(instance, key)
            else:
                result = cache.get(key)

                if result is None:
                    # all caches failed, call the actual method
                    result = method(instance, *args, **kwargs)

                    # save to memcache and class attr
                    if seconds and isinstance(seconds, int):
                        cache.set(key, result, seconds)
                    setattr(instance, key, result)

            return result

        return x

    return inner_cache


def function_cache(seconds=0):
    """
    A `seconds` value of `0` means that we will not memcache it.

    If a result is cached on instance, return that first.  If that fails, check
    memcached. If all else fails, hit the db and cache on instance and in memcache.

    ** NOTE: Methods that return None are always "recached".
    """

    from hashlib import sha224
    from django.core.cache import cache

    def inner_cache(method):

        def x(*args, **kwargs):
            key = sha224(str(method.__module__) + str(method.__name__) + \
                str(args) + str(kwargs)).hexdigest()

            result = cache.get(key)

            if result is None:
                # all caches failed, call the actual method
                result = method(*args, **kwargs)

                # save to memcache and class attr
                if seconds and isinstance(seconds, int):
                    cache.set(key, result, seconds)

            return result

        return x

    return inner_cache
